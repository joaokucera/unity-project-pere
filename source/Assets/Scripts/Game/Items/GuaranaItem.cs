using UnityEngine;
using System.Collections;

public class GuaranaItem : BaseItem
{
	#region [ FIELDS ]

	/// <summary>
	/// The extra health percentage.
	/// </summary>
	[SerializeField]
	private int extraHealthPercentage = 50;

	#endregion

    #region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();
		
		itemType = ItemType.Guarana;
	}

    /// <summary>
    /// Raises the use event.
    /// </summary>
    public override void OnUse()
    {
		int extraHealthValue = extraHealthPercentage * GameContext.PlayerHealth.InitialHealth / 100;
		
		GameContext.PlayerHealth.AddExtraHealth (extraHealthValue);

        base.OnUse();
    }

    #endregion
}
