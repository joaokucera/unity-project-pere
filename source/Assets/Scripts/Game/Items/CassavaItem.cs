using UnityEngine;
using System.Collections;
using System;

public class CassavaItem : BaseItem
{
	#region [ FIELDS ]
	
	/// <summary>
	/// The extra damage value.
	/// </summary>
	[SerializeField]
	private int extraDamageValue = 1;
	/// <summary>
	/// The extra damage effect time.
	/// </summary>
	[SerializeField]
	private float extraDamageEffectTime = 5f;
	
	#endregion
	
	#region [ METHODS ]
	
	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();
		
		itemType = ItemType.Cassava;
	}
	
	/// <summary>
	/// Raises the use event.
	/// </summary>
	public override void OnUse()
	{
		//GameObject.FindObjectOfType<TimerItemImage> ().StartClock (extraDamageEffectTime);

		GameContext.PlayerWeapon.IncreaseDamage(extraDamageValue, extraDamageEffectTime);

		base.OnUse();
	}
	
	#endregion
}