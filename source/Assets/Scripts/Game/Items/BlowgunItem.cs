﻿using UnityEngine;
using System.Collections;

public class BlowgunItem : BaseItem
{
	#region [ FIELDS ]

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		itemType = ItemType.Weapon_Blowgun;
	}

    /// <summary>
    /// Raises the use event.
    /// </summary>
    public override void OnUse()
    {
        base.OnUse();
    }

    #endregion
}
