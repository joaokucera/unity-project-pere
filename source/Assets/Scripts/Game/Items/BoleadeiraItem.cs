using UnityEngine;
using System.Collections;

public class BoleadeiraItem : BaseItem
{
	#region [ FIELDS ]

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		itemType = ItemType.Weapon_Boleadeira;
	}

    /// <summary>
    /// Raises the use event.
    /// </summary>
    public override void OnUse()
    {
        base.OnUse();
    }

    #endregion
}
