using UnityEngine;
using System.Collections;

public class CoffeeItem : BaseItem
{
	#region [ FIELDS ]
	
	/// <summary>
	/// The extra speed percentage.
	/// </summary>
	[SerializeField]
	private int extraSpeedPercentage = 50;
	/// <summary>
	/// The extra speed effect time.
	/// </summary>
	[SerializeField]
	private float extraSpeedEffectTime = 5f;
	
	#endregion
	
	#region [ METHODS ]
	
	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();
		
		itemType = ItemType.Coffee;
	}
	
	/// <summary>
	/// Raises the use event.
	/// </summary>
	public override void OnUse()
	{
		float extraSpeedValue = extraSpeedPercentage * GameContext.PlayerMovement.InitialWalkSpeed / 100f;
		
		GameContext.PlayerMovement.IncreaseSpeed (extraSpeedValue, extraSpeedEffectTime);
		
		base.OnUse();
	}

	#endregion
}