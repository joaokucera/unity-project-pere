﻿using UnityEngine;
using System.Collections;

public class MuiraquitaItem : BaseItem
{
	#region [ METHODS ]
	
	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();
		
		itemType = ItemType.Muiraquita;
	}

	#endregion
}