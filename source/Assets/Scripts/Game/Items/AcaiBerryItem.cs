using UnityEngine;
using System.Collections;

public class AcaiBerryItem : BaseItem
{
	#region [ FIELDS ]
	
	/// <summary>
	/// The heal value.
	/// </summary>
	[SerializeField]
	private int healPercentage = 50;

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		itemType = ItemType.AcaiBerry;
	}

    /// <summary>
    /// Raises the use event.
    /// </summary>
    public override void OnUse()
    {
		int healValue = healPercentage * GameContext.PlayerHealth.InitialHealth / 100;

		GameContext.PlayerHealth.Heal (healValue);

        base.OnUse();
    }

    #endregion
}
