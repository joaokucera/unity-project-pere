﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GoldMotherItem : BaseItem
{
	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();
		
		itemType = ItemType.GoldMother;
	}

    /// <summary>
    /// When will use the gold item.
    /// </summary>
    public override void OnUse()
    {
        GameObject[] golds = GameObject.FindGameObjectsWithTag(ShortcutWords.GoldTag);

        print("Existem " + golds.Length + " golds no cenário!");

		for (int i = 0; i < golds.Length; i++)
		{
			GameObject g = golds[i];

			g.renderer.enabled = true;
			g.collider2D.enabled = true;
		}

        base.OnUse();
    }

    #endregion
}
