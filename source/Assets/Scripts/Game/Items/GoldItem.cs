﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldItem : BaseItem
{
	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();
		
		itemType = ItemType.Gold;
	}

    #endregion
}