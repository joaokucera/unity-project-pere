using System;
using System.Collections;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(CircleCollider2D))]
public class CapeloboTongueBehavior : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The current damage.
	/// </summary>
	[SerializeField]
	private int currentDamage = 2;
	/// <summary>
	/// The line renderer.
	/// </summary>
	private LineRenderer line;
	/// <summary>
	/// The parent behavior.
	/// </summary>
	private CapeloboBehavior parentBehavior;

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		line = GetComponent<LineRenderer>();
		line.enabled = false;

		parentBehavior = GetComponentInParent<CapeloboBehavior> ();
	}

	/// <summary>
	/// Dos the attack.
	/// </summary>
	public void OnLick (Vector2 parentPosition, float tongueSpeed, float timeToAttack)
	{
		float distanceFromPlayer = Vector2.Distance (GameContext.PlayerMovement.Pivot, parentPosition);

		Vector2 direction = GameContext.PlayerMovement.Pivot - parentPosition;
		Vector3 normalizeDirection = new Vector3(direction.normalized.x, direction.normalized.y, direction.normalized.y) / 2f;

		StartCoroutine(Lick(normalizeDirection, parentPosition, distanceFromPlayer, tongueSpeed, timeToAttack));
	}

	/// <summary>
	/// Raises the lick event.
	/// </summary>
	/// <param name="direction">Direction.</param>
	/// <param name="distance">Distance.</param>
	/// <param name="tongueSpeed">Tongue speed.</param>
	private IEnumerator Lick(Vector3 direction, Vector2 parentPosition, float distanceFromPlayer, float tongueSpeed, float timeToAttack)
	{
		StartCoroutine (parentBehavior.DoAttackFeedback ());
		yield return new WaitForSeconds (timeToAttack);

		line.enabled = true;

		for (float i = 0; i < distanceFromPlayer; i += tongueSpeed * Time.deltaTime)
		{
			line.SetPosition (1, direction * i);
			
			yield return null;
		}
		Vector3 targetPosition = direction * distanceFromPlayer;

		RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, targetPosition, distanceFromPlayer);

		bool takenPlayer = hits.Any (h => h.collider.CompareTag (ShortcutWords.PlayerTag));
		if (takenPlayer)
		{
			GameContext.PlayerHealth.Damage(currentDamage);
		}

		float distanceFromCapelobo = Vector2.Distance (targetPosition, transform.localPosition);
		while (distanceFromCapelobo > 0.5f)
		{
			targetPosition = Vector3.MoveTowards(targetPosition, Vector2.zero, tongueSpeed * Time.deltaTime);

			line.SetPosition (1, targetPosition);

			distanceFromCapelobo = Vector2.Distance (targetPosition, transform.localPosition);

			yield return null;
		}

		line.enabled = false;

		parentBehavior.EvaluateNextStatus ();
	}

	#endregion
}