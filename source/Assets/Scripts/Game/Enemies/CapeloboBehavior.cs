using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapeloboBehavior : BaseEnemy 
{
	#region [ FIELDS ]

	/// <summary>
	/// The name of the evaluate next status method.
	/// </summary>
	private const string EvaluateNextStatusMethodName = "EvaluateNextStatus";

	/// <summary>
	/// The attack distance.
	/// </summary>
	[SerializeField]
	private float attackDistance = 10f;
	/// <summary>
	/// The sprint force.
	/// </summary>
	[SerializeField]
	private float sprintForce = 500f;
	/// <summary>
	/// The tongue speed.
	/// </summary>
	[SerializeField]
	private float tongueSpeed = 10f;
	/// <summary>
	/// The probability percentage to sprint.
	/// </summary>
	[SerializeField]
	private int probabilityPercentageToSprint = 70;
	/// <summary>
	/// The probability percentage to lick.
	/// </summary>
	[SerializeField]
	private int probabilityPercentageToLick = 30;
	/// <summary>
	/// The integer probability pool.
	/// </summary>
	private List<EnemyStatus> statusProbabilityPool = new List<EnemyStatus>();
	/// <summary>
	/// The sprint target.
	/// </summary>
	private Vector2 sprintTarget;
	/// <summary>
	/// The tongue.
	/// </summary>
	private CapeloboTongueBehavior tongue;
	/// <summary>
	/// The trail.
	/// </summary>
	private TrailRenderer trailFeedback;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets the draws action status.
	/// </summary>
	/// <value>The draws action status.</value>
	public EnemyStatus DrawsAttackStatus
	{
		get 
		{
			int randomProbability =  Random.Range (0, statusProbabilityPool.Count);

			return statusProbabilityPool[randomProbability];
		}
	}
	
	#endregion

	#region [ METHODS ]
	
	#region implemented abstract members of BaseEnemy
	
	/// <summary>
	/// Executes the status.
	/// </summary>
	protected override void ExecuteCurrentStatus ()
	{
		StopCoroutine(FollowPathMethodName);

		HideSprintFeedback();

		switch (CurrentStatus)
		{
			case EnemyStatus.Idle: // if IDLE, can go to ATTACK or CHASE.
			{
				StartCoroutine(VerifyDistanceFromPlayerToAttackOrChase());
				
				break;
			}
			case EnemyStatus.Chase: // if CHASE, can go to ATTACK (SPRINT or SHOOT) or IDLE.
			{
				speed = chaseSpeed;

				StartCoroutine(VerifyDistanceFromPlayerToAttackOrIdle());
				
				break;
			}
			case EnemyStatus.Sprint: // if SPRINT, can go to MELEE or CHASE or IDLE.
			{
				StartCoroutine(OnSprint());
				
				break;
			}
			case EnemyStatus.Shoot: // if SHOOT (i.e. LICK), can go to other CHASE or IDLE.
			{
				tongue.OnLick(Pivot, tongueSpeed, timeToAttack);

				break;
			}
			case EnemyStatus.Melee: // if AWAY, can go to CHASE or IDLE.
			{
				StartCoroutine(VerifyDistanceFromPlayerToChaseOrIdle());
				
				break;
			}
		}
	}

	#endregion
	
	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		trailFeedback = GetComponentInChildren<TrailRenderer> ();

		tongue = GetComponentInChildren<CapeloboTongueBehavior> ();
		tongue.transform.position = Pivot;

		InitializeStatusProbabilityPool ();

		CurrentStatus = EnemyStatus.Idle;
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	protected override void Update ()
	{
		transform.SetFakeDepth ();
	}

	/// <summary>
	/// Sent when an incoming collider makes contact with this object's collider (2D physics only).
	/// </summary>
	/// <param name="collision">Collision.</param>
	protected override void OnCollisionEnter2D (Collision2D collision)
	{
		base.OnCollisionEnter2D (collision);

		if (collision.gameObject.CompareTag(ShortcutWords.ObstacleTag) || collision.gameObject.CompareTag(ShortcutWords.DestructibleTag))
		{
			rigidbody2D.velocity = Vector2.zero;
		}
	}

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this object (2D physics only).
	/// </summary>
	/// <param name="collider">Collider.</param>
	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		base.OnTriggerEnter2D (collider);

		if (collider.CompareTag(ShortcutWords.ObstacleTag) || collider.CompareTag(ShortcutWords.DestructibleTag))
		{
			rigidbody2D.velocity = Vector2.zero;
		}
	}

	/// <summary>
	/// Verifies the distance from player to chase.
	/// </summary>
	/// <returns>The distance from player to chase.</returns>
	private IEnumerator VerifyDistanceFromPlayerToAttackOrChase()
	{
		while (true) 
		{
			float distanceFromPlayer = DistanceFromPlayer;

			if  (distanceFromPlayer <= attackDistance)
			{
				OnAttack();
				
				break;
			}
			else if (wasHit || distanceFromPlayer <= chaseDistance)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Chase));

				break;
			}

			yield return null;
		}
	}

	/// <summary>
	/// Verifies the distance from player to attack or idle.
	/// </summary>
	/// <returns>The distance from player to attack or idle.</returns>
	private IEnumerator VerifyDistanceFromPlayerToAttackOrIdle()
	{
		while (true)
		{
			RequestPath (transform.position, GameContext.PlayerObject.transform.position, OnPathFound);

			float distanceFromPlayer = DistanceFromPlayer;

			if  (distanceFromPlayer <= attackDistance)
			{
				OnAttack();

				break;
			}
			else if (distanceFromPlayer > rangeDistance)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Idle));

				break;
			}

			yield return null;
		}
	}

	/// <summary>
	/// Verifies the distance from player to idle.
	/// </summary>
	/// <returns>The distance from player to idle.</returns>
	private IEnumerator VerifyDistanceFromPlayerToChaseOrIdle()
	{
		while (isTouching)
		{
			yield return null;
		}

		EvaluateNextStatus ();
	}

	/// <summary>
	/// Evaluates to idle or chase.
	/// </summary>
	public void EvaluateNextStatus()
	{
		if (isTouching)
		{
			StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Melee));
		}
		else if (DistanceFromPlayer > rangeDistance)
		{
			StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Idle));
		}
		else
		{
			StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Chase));
		}
	}

	/// <summary>
	/// Raises the attack event.
	/// </summary>
	private void OnAttack ()
	{
		EnemyStatus actionStatus = DrawsAttackStatus;

		if (actionStatus == EnemyStatus.Sprint)
		{
			if (Physics2D.Linecast(Pivot, GameContext.PlayerMovement.Pivot, Grid.UnwalkableMask))
			{
				actionStatus = EnemyStatus.Shoot;
			}
		}

		StartCoroutine (WaitTimeToChangeStatus (actionStatus));
	}

	/// <summary>
	/// Dos the sprint.
	/// </summary>
	private IEnumerator OnSprint ()
	{
		StartCoroutine (DoAttackFeedback ());
		ShowSprintFeedback();
		
		yield return new WaitForSeconds (timeToAttack);
		
		Vector2 direction = GameContext.PlayerMovement.Pivot - Pivot;
		sprintTarget = direction.normalized * sprintForce;

		rigidbody2D.AddForce(sprintTarget);

		Invoke (EvaluateNextStatusMethodName, timeToAttack);
	}

	/// <summary>
	/// Shows the sprint feedback.
	/// </summary>
	private void ShowSprintFeedback ()
	{
		trailFeedback.enabled = true;
		rigidbody2D.isKinematic = false;
	}

	/// <summary>
	/// Hides the sprint feedback.
	/// </summary>
	private void HideSprintFeedback ()
	{
		trailFeedback.enabled = false;
		rigidbody2D.isKinematic = true;
	}

	/// <summary>
	/// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
	/// </summary>
	protected override void OnDrawGizmos ()
	{
		base.OnDrawGizmos ();

		if (!debug) return;

		Gizmos.color = DistanceFromPlayer <= attackDistance ? Color.red : Color.white;
		Gizmos.DrawWireSphere (Pivot, attackDistance);
		Gizmos.DrawLine (Pivot, sprintTarget);
		Gizmos.DrawSphere (sprintTarget, 0.25f);
	}
	
	/// <summary>
	/// Initializes the status probability pool.
	/// </summary>
	private void InitializeStatusProbabilityPool ()
	{
		for (int i = 0; i < probabilityPercentageToSprint; i++)
		{
			statusProbabilityPool.Add(EnemyStatus.Sprint);
		}
		for (int i = 0; i < probabilityPercentageToLick; i++) 
		{
			statusProbabilityPool.Add(EnemyStatus.Shoot);
		}
	}

	#endregion
}