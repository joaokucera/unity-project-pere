﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapinguariHealth : EnemyHealth 
{
	#region [ FIELDS ]

	/// <summary>
	/// The main behavior.
	/// </summary>
	private MapinguariBehavior mainBehavior;

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();
		
		mainBehavior = GetComponent<MapinguariBehavior> ();
	}

	/// <summary>
	/// Hits the verification.
	/// </summary>
	/// <param name="collider">Collider.</param>
	public override void HitVerification (Collider2D collider)
	{
		if (collider.CompareTag(hitAgainstOwnerAttackTag.ToString()))
		{
			BaseAmmo ammo = collider.GetComponent<BaseAmmo> ();

			if (ammo.GetType() == typeof(MeleeAttack))
			{
				Damage (ammo.AmmoSettings.DamageSettings.Total);
			}
			else if (mainBehavior.CurrentStatus != EnemyStatus.Blocker)
			{
				mainBehavior.UnderAttack = true;
			}

			ammo.DoActive(false);
		}
	}

	#endregion
}