using UnityEngine;
using System.Collections;

public class BoitataEyeBehavior : BaseEnemy 
{
	#region [ FIELDS ]
	
	/// <summary>
	/// The melee distance.
	/// </summary>
	[SerializeField]
	private float meleeDistance;
	
	#endregion

	#region [ METHODS ]
	
	#region implemented abstract members of BaseEnemy
	
	/// <summary>
	/// Executes the status.
	/// </summary>
	protected override void ExecuteCurrentStatus ()
	{
		StopCoroutine(FollowPathMethodName);
		
		switch (CurrentStatus)
		{
			case EnemyStatus.Chase: // if CHASE, can go to MELEE.
			{
				speed = chaseSpeed;
				
				StartCoroutine(VerifyDistanceFromPlayerToMelle());
				
				break;
			}
			case EnemyStatus.Melee: // if MELEE, can go to CHASE.
			{
				StartCoroutine(VerifyDistanceFromPlayerToChase());
				
				break;
			}
		}
	}
	
	#endregion
	
	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		collider2D.enabled = false;
	}

	/// <summary>
	/// Dos the release.
	/// </summary>
	public void DoRelease()
	{
		transform.SetParent (GameContext.DynamicObjects);
		collider2D.enabled = true;

		StartCoroutine (Rescale ());

		CurrentStatus = EnemyStatus.Chase;
	}

	/// <summary>
	/// Rescale this instance.
	/// </summary>
	private IEnumerator Rescale()
	{
		Vector2 currentScale = transform.localScale;
		while (currentScale != Vector2.one)
		{
			transform.SetScale (Vector2.one * Time.deltaTime);
			currentScale = transform.localScale;

			yield return null;
		}
	}

	/// <summary>
	/// Raises the chase event.
	/// </summary>
	protected IEnumerator VerifyDistanceFromPlayerToMelle()
	{
		while (true)
		{
			RequestPath (transform.position, GameContext.PlayerObject.transform.position, OnPathFound);
			
			if (DistanceFromPlayer <= meleeDistance && isTouching)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Melee));
				
				break;
			}
			
			yield return null;
		}
	}
	
	/// <summary>
	/// Verifies the distance from player to chase.
	/// </summary>
	/// <returns>The distance from player to chase.</returns>
	private IEnumerator VerifyDistanceFromPlayerToChase()
	{
		while (DistanceFromPlayer <= meleeDistance)
		{
			yield return null;
		}
		
		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Chase));
	}
	
	/// <summary>
	/// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
	/// </summary>
	protected override void OnDrawGizmos ()
	{
		base.OnDrawGizmos ();
		
		if (!debug) return;
		
		Gizmos.color = DistanceFromPlayer <= meleeDistance ? Color.red : Color.white;
		Gizmos.DrawWireSphere (Pivot, meleeDistance);
	}
	
	#endregion
}