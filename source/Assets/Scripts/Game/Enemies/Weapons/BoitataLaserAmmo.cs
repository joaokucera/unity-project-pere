﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class BoitataLaserAmmo : BaseAmmo 
{
	#region [ PROPERTIES ]

	/// <summary>
	/// Set if game object is active.
	/// </summary>
	/// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
	public override bool Active 
	{
		get 
		{
			return base.Active;
		}
		set 
		{
			if (!value)
			{
				return;
			}

			base.Active = value;
		}
	}

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();
		
		gameObject.tag = ShortcutWords.EnemyAttackTag;
	}

	/// <summary>
	/// Raises the move event.
	/// </summary>
	/// <param name="settings">Settings.</param>
	protected override IEnumerator OnMove (WeaponSettings settings)
	{
		ammoSettings = settings.AmmoSettings;

		int sort = Random.Range (-1, 1);
		sort = sort > 0 ? 1 : -1;

		for (float i = 0; i < ammoSettings.DamageSettings.Time; i += Time.deltaTime)
		{
			transform.Rotate(sort * Vector3.forward * ammoSettings.Speed * Time.deltaTime);

			yield return null;
		}

		base.Active = false;
	}

	#endregion
}