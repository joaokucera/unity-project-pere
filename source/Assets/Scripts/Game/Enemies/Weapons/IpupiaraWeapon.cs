using UnityEngine;
using System.Collections;

public class IpupiaraWeapon : EnemyWeapon
{
	#region [ FIELDS ]

	#endregion
	
	#region [ METHODS ]

	/// <summary>
	/// Shoot this instance.
	/// </summary>
	protected override IEnumerator Shoot (Vector2 parentPosition)
	{
		yield return new WaitForSeconds (weaponSettings.CooldownSettings.Rate);

		Quaternion angle = Quaternion.FromToRotation(Vector3.up, GameContext.PlayerMovement.Pivot - parentPosition);
		ReleaseShot (angle);

		angle = Quaternion.FromToRotation(Vector3.up, GameContext.PlayerMovement.Pivot - new Vector2(1.5f, 1.5f) - parentPosition);
		ReleaseShot (angle);

		angle = Quaternion.FromToRotation(Vector3.up, GameContext.PlayerMovement.Pivot + new Vector2(1.5f, 1.5f) - parentPosition);
		ReleaseShot (angle);

		shotReleased = true;
	}

	/// <summary>
	/// Releases the shot.
	/// </summary>
	/// <param name="angle">Angle.</param>
	private void ReleaseShot(Quaternion angle)
	{
		GameObject ammo = GetObjectFromPool (transform.position, angle);

		if (ammo != null)
		{
			var direction = ammo.transform.up;

			weaponSettings.AmmoSettings.Direction = direction;
			weaponSettings.AmmoSettings.DamageSettings.Force = weaponSettings.AmmoSettings.Speed;
			weaponSettings.AmmoSettings.DamageSettings.Time = weaponSettings.TimeSettings.Value;

			ammo.SendMessage(ExecuteMethodName, weaponSettings);
		}
	}
	
	#endregion
}