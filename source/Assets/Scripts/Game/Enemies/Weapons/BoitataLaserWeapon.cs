using UnityEngine;
using System.Collections;

public class BoitataLaserWeapon : EnemyWeapon 
{
	#region [ FIELDS ]

	/// <summary>
	/// The scale divider.
	/// </summary>
	[SerializeField]
	private float scaleDivider = 100f;

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Shoot this instance.
	/// </summary>
	/// <param name="parentPosition">Parent position.</param>
	protected override IEnumerator Shoot (Vector2 parentPosition)
	{
		Vector3 viewportSize = new Vector3 (GameContext.MainCamera.orthographicSize * GameContext.MainCamera.aspect, GameContext.MainCamera.orthographicSize, 0);
		Vector3 worldSize = GameContext.MainCamera.ViewportToWorldPoint(viewportSize);

		ReleaseFire (new Vector2(worldSize.x, 1));
		ReleaseFire (new Vector2(1, worldSize.y));

		for (float i = 0; i < weaponSettings.CooldownSettings.Rate; i+= Time.deltaTime)
		{
			yield return null;
		}

		shotReleased = true;
	}

	/// <summary>
	/// Releases the fire.
	/// </summary>
	/// <returns>The fire.</returns>
	private void ReleaseFire(Vector2 newScale)
	{
		GameObject ammo = GetObjectFromPool (transform.position, Quaternion.identity);

		if (ammo != null)
		{
			StartCoroutine(GrowScale(ammo, newScale));

			weaponSettings.AmmoSettings.DamageSettings.Time = weaponSettings.TimeSettings.Value;
			
			ammo.SendMessage(ExecuteMethodName, weaponSettings);
		}
	}

	/// <summary>
	/// Grows the scale.
	/// </summary>
	/// <returns>The scale.</returns>
	/// <param name="ammo">Ammo.</param>
	/// <param name="newScale">New scale.</param>
	private IEnumerator GrowScale(GameObject ammo, Vector2 newScale)
	{
		Vector2 growScale = newScale / scaleDivider;
		Vector2 currentScale = ammo.transform.localScale;

		while (currentScale != newScale)
		{
			ammo.transform.SetScale(growScale, 0, 0, 0, newScale.x, newScale.y, 1); 
			currentScale = ammo.transform.localScale;

			yield return null;
		}

		ammo.transform.localScale = newScale;
	}

	#endregion
}