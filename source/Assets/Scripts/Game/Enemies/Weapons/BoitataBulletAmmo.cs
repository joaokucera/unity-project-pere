﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CircleCollider2D), typeof(Rigidbody2D))]
public class BoitataBulletAmmo : BaseAmmo 
{
	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();
		
		gameObject.tag = ShortcutWords.EnemyAttackTag;
	}

	/// <summary>
	/// Raises the move event.
	/// </summary>
	/// <param name="settings">Settings.</param>
	protected override IEnumerator OnMove (WeaponSettings settings)
	{
		ammoSettings = settings.AmmoSettings;
		rigidbody2D.AddForce(ammoSettings.Direction * settings.AmmoSettings.DamageSettings.Force);

		for (float i = 0; i < ammoSettings.DamageSettings.Time; i += Time.deltaTime)
		{
			yield return null;
		}

		Active = false;
	}

	#endregion
}