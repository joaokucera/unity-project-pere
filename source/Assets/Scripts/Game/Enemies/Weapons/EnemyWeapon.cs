using UnityEngine;
using System.Collections;

public abstract class EnemyWeapon : GenericPooling
{
	#region [ FIELDS ]

	/// <summary>
	/// The name of the execute method.
	/// </summary>
	protected const string ExecuteMethodName = "Execute";

	/// <summary>
	/// The weapon settings.
	/// </summary>
	[SerializeField]
	protected WeaponSettings weaponSettings;
	/// <summary>
	/// The shot released.
	/// </summary>
	protected bool shotReleased;

	#endregion
	
	#region [ PROPERTIES ]

	/// <summary>
	/// Gets the weapon cooldown.
	/// </summary>
	/// <value>The weapon cooldown.</value>
	public bool ShotReleased
	{
		get { return shotReleased; }
	}

	#endregion
	
	#region [ METHODS ]

	/// <summary>
	/// Raises the shoot event.
	/// </summary>
	public void OnShoot (Vector2 parentPosition)
	{
		shotReleased = false;

		StartCoroutine(Shoot(parentPosition));
	}

	/// <summary>
	/// Shoot this instance.
	/// </summary>
	protected abstract IEnumerator Shoot (Vector2 parentPosition);
	
	#endregion
}