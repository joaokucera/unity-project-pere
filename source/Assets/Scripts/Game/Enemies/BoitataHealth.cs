using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BoitataHealth : EnemyHealth 
{
	#region [ FIELDS ]

	/// <summary>
	/// The parts.
	/// </summary>
	private List<BoitataPart> parts;
	/// <summary>
	/// The parts parent.
	/// </summary>
	private Transform partsParent;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets the boitata parts.
	/// </summary>
	/// <value>The boitata parts.</value>
	public List<BoitataPart> Parts { get { return parts; } }
	/// <summary>
	/// Gets the parts parent.
	/// </summary>
	/// <value>The parts parent.</value>
	public Transform PartsParent { get { return partsParent; } }

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		parts = GetComponentsInChildren<BoitataPart> ().ToList();
		partsParent = parts [0].transform.parent;

		Vector3 scale = transform.localScale;
		float xFactor = scale.x / parts.Count / 2f;
		float yFactor = scale.y / parts.Count / 2f;

		for (int i = 0; i < parts.Count; i++)
		{
			scale.x -= xFactor;
			scale.y -= yFactor;

			parts [i].Initialize(rigidbody2D, i + 1, scale);
		}
	}

	/// <summary>
	/// Raises the death event.
	/// </summary>
	protected override void OnDeath ()
	{
		for (int i = 0; i < parts.Count; i++)
		{
			var part = parts[i];

			GameContext.ParticlesRepository.SpawnParticle (ParticleType.FlashLight, part.Pivot, part.transform.rotation);
			
			part.Active = false;
		}

		base.OnDeath ();
	}

	/// <summary>
	/// Raises the blink event.
	/// </summary>
	protected override void OnBlink ()
	{
		base.OnBlink ();

		for (int i = 0; i < parts.Count; i++)
		{
			parts[i].OnBlink (originalColor, hitColor);
		}
	}

	/// <summary>
	/// Raises the recover event.
	/// </summary>
	protected override void OnRecover ()
	{
		base.OnRecover ();

		for (int i = 0; i < parts.Count; i++)
		{
			parts[i].renderer.material.color = originalColor;
		}
	}

	#endregion
}