using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BoitataBehavior : BaseEnemy 
{
	#region [ FIELDS ]

	/// <summary>
	/// The name of the hole object.
	/// </summary>
	private const string HoleObjectName = "Hole"; 
	/// <summary>
	/// The name of the main eye object.
	/// </summary>
	private const string MainEyeObjectName = "Main Eye";

	/// <summary>
	/// The hole.
	/// </summary>
	private Transform hole;
	/// <summary>
	/// The health.
	/// </summary>
	private BoitataHealth health;
	/// <summary>
	/// The laer weapon.
	/// </summary>
	private BoitataLaserWeapon laserWeapon;
	/// <summary>
	/// The bullet weapon.
	/// </summary>
	private BoitataBulletWeapon bulletWeapon;
	/// <summary>
	/// The probability percentage to shoot.
	/// </summary>
	[SerializeField]
	private int probabilityPercentageToShoot = 50;
	/// <summary>
	/// The boolean probability pool.
	/// </summary>
	private List<bool> booleanProbabilityPool = new List<bool>();
	/// <summary>
	/// The boitata eyes.
	/// </summary>
	private Queue<BoitataEyeBehavior> boitataEyes = new Queue<BoitataEyeBehavior>();
	/// <summary>
	/// The sprint multiplier.
	/// </summary>
	[SerializeField]
	private float sprintMultiplier;// = 10f;
	/// <summary>
	/// The inside hole.
	/// </summary>
	private bool insideHole = false;
	/// <summary>
	/// The index of the bullet weapon.
	/// </summary>
	[SerializeField]
	private int bulletWeaponIndex = 3;
	/// <summary>
	/// The index of the current weapon.
	/// </summary>
	private int currentWeaponIndex = 0;
	/// <summary>
	/// The main eye.
	/// </summary>
	private Transform mainEye;
	/// <summary>
	/// The last move towards.
	/// </summary>
	private Vector2 lastMoveTowards;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets a value indicating whether this <see cref="BoitataBehavior"/> draws boolean probability.
	/// </summary>
	/// <value><c>true</c> if draws boolean probability; otherwise, <c>false</c>.</value>
	protected bool DrawsBooleanProbability
	{
		get 
		{
			int randomProbability = Random.Range (0, booleanProbabilityPool.Count);
			
			return booleanProbabilityPool[randomProbability];
		}
	}
	/// <summary>
	/// Gets or sets the draw position around player.
	/// </summary>
	/// <value>The draw position around player.</value>
	private Vector2 DrawPositionAroundPlayer 
	{
		get
		{
			Vector2 playerPosition = GameContext.PlayerMovement.Pivot;

			float extraRange = 5f;
			float x  = Random.Range(playerPosition.x - extraRange, playerPosition.x + extraRange);
			float y  = Random.Range(playerPosition.y - extraRange, playerPosition.y + extraRange);

			return new Vector2(x, y);
		}
	}
	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="BoitataBehavior"/> is active.
	/// </summary>
	/// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
	public override bool Active 
	{
		get 
		{
			return base.Active;
		}
		set 
		{
			base.Active = value;

			if (!value)
			{
				RedefineHole (Pivot, false, ShortcutWords.Untagged);
			}
		}
	}

	#endregion

	#region [ METHODS ]

	#region implemented abstract members of BaseEnemy

	/// <summary>
	/// Executes the status.
	/// </summary>
	protected override void ExecuteCurrentStatus ()
	{
		StopCoroutine(FollowPathMethodName);

		switch (CurrentStatus)
		{
			case EnemyStatus.Idle: // if IDLE, can go to SPRINT.
			{
				StartCoroutine(VerifyDistanceFromPlayerToSprint());

				break;
			}
			case EnemyStatus.Sprint: // if SPRINT, can go to BURROWER.
			{
				speed = chaseSpeed;
				
				StartCoroutine(PrepareToSprintAndGoToBurrower());
				
				break;
			}
			case EnemyStatus.Shoot: // if SHOOT, can go to BURROWER.
			{
				StartCoroutine(PrepareToShootAndGoToBurrower());

				break;
			}
			case EnemyStatus.Burrower: // if BURROWER, can go to SPRINT, SHOOT or IDLE.
			{
				StartCoroutine(PrepareToBurrowerAndGoToSprintOrShootOrIdle());

				break;
			}
		}
	}

	#endregion

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		health = GetComponent<BoitataHealth> ();

		laserWeapon = GetComponentInChildren<BoitataLaserWeapon> ();
		bulletWeapon = GetComponentInChildren<BoitataBulletWeapon> ();

		var eyes = GetComponentsInChildren<BoitataEyeBehavior> ();
		for (int i = 0; i < eyes.Length; i++)
		{
			boitataEyes.Enqueue(eyes[i]);
		}

		InitializeHole ();

		mainEye = transform.FindChild (MainEyeObjectName);
		booleanProbabilityPool = MethodService.CreateBooleanProbabilityPool (probabilityPercentageToShoot);

		CurrentStatus = EnemyStatus.Idle;
	}

	/// <summary>
	/// Dos the activation.
	/// </summary>
	/// <param name="enable">If set to <c>true</c> enable.</param>
	private void DoActivation(bool enable)
	{
		collider2D.enabled = enable;
		renderer.enabled = enable;

		mainEye.renderer.enabled = enable;

		BoitataEyeBehavior[] eyes = boitataEyes.ToArray ();
		for (int i = 0; i < eyes.Length; i++)
		{
			eyes[i].renderer.enabled = enable;
		}
	}
	
	/// <summary>
	/// Initializes the hole.
	/// </summary>
	private void InitializeHole ()
	{
		hole = transform.FindChild (HoleObjectName);
		
		hole.gameObject.SetActive (false);
		hole.collider2D.isTrigger = true;

		hole.SetParent (GameContext.DynamicObjects);
	}

	/// <summary>
	/// Redefines the hole.
	/// </summary>
	/// <param name="newPosition">New position.</param>
	/// <param name="active">If set to <c>true</c> active.</param>
	/// <param name="newTag">New tag.</param>
	private void RedefineHole (Vector2 newPosition, bool active, string newTag)
	{
		hole.tag = newTag;

		hole.SetPositionTo(newPosition);

		hole.gameObject.SetActive (active);
	}

	/// <summary>
	/// Verifies the distance from player to sprint.
	/// </summary>
	/// <returns>The distance from player to sprint.</returns>
	private IEnumerator VerifyDistanceFromPlayerToSprint ()
	{
		while (!wasHit && DistanceFromPlayer > chaseDistance)
		{
			yield return null;
		}

		StartCoroutine (DoAttackFeedback ());
		yield return new WaitForSeconds (timeToAttack);

		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Sprint));
	}

	/// <summary>
	/// Prepares to sprint and go to idle.
	/// </summary>
	/// <returns>The to sprint and go to idle.</returns>
	private IEnumerator PrepareToSprintAndGoToBurrower()
	{
		Vector2 direction = (GameContext.PlayerMovement.Pivot - Pivot).normalized;
		lastMoveTowards = GameContext.PlayerMovement.Pivot + (direction * sprintMultiplier);
		Vector2 currentPosition = transform.position;

		RedefineHole (GameContext.PlayerMovement.Pivot + (direction * sprintMultiplier / 2f), true, ShortcutWords.HoleTag);
		while (!insideHole && currentPosition != lastMoveTowards)
		{
			if (renderer.enabled)
			{
				Vector2 nextTile = currentPosition + (direction * Grid.NodeDiameter * 2f);
				if (!GameContext.Grid.GetNodeFromWorldPoint(nextTile).walkable)
				{
					break;
				}
			}

			transform.MoveTowardsTo(lastMoveTowards, speed * Time.deltaTime);
			currentPosition = transform.position;
			
			yield return null;
		}
		RedefineHole (Pivot, false, ShortcutWords.Untagged);

		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Burrower));
	}

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this object (2D physics only).
	/// </summary>
	/// <param name="collider">Collider.</param>
	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		base.OnTriggerEnter2D (collider);

		if (collider.CompareTag(ShortcutWords.HoleTag))
		{
			DoActivation(false);
		}
	}

	/// <summary>
	/// Tries the hole.
	/// </summary>
	/// <param name="otherCollider">Other collider.</param>
	/// <param name="otherPart">Other part.</param>
	public void TryHole (Collider2D otherCollider, BoitataPart otherPart)
	{
		if (otherCollider.CompareTag(ShortcutWords.HoleTag))
		{
			otherPart.DoActivation(false);

			if (health.Parts.All(p => !p.renderer.enabled))
			{
				insideHole = true;
			}
		}
	}

	/// <summary>
	/// Takes the current weapon.
	/// </summary>
	/// <returns>The current weapon.</returns>
	private EnemyWeapon TakeCurrentWeapon()
	{
		EnemyWeapon currentWeapon = laserWeapon;

		currentWeaponIndex++;

		if (currentWeaponIndex == bulletWeaponIndex || health.Parts.Count == 0)
		{
			currentWeaponIndex = 0;

			currentWeapon = bulletWeapon;
		}

		return currentWeapon;
	}

	/// <summary>
	/// Prepares to shoot and go to idle.
	/// </summary>
	/// <returns>The to shoot and go to idle.</returns>
	private IEnumerator PrepareToShootAndGoToBurrower()
	{
		StartCoroutine (DoAttackFeedback ());
		yield return new WaitForSeconds (timeToAttack);

		var currentWeapon = TakeCurrentWeapon();

		currentWeapon.OnShoot (Pivot);
		while (!currentWeapon.ShotReleased)
		{
			transform.ShakeAtLocalPosition();
			yield return null;
		}

		yield return new WaitForSeconds (timeToAttack);
		HideAll ();

		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Burrower));
	}

	/// <summary>
	/// Hides all.
	/// </summary>
	private void HideAll()
	{
		DoActivation(false);

		for (int i = 0; i < health.Parts.Count; i++) 
		{
			health.Parts[i].DoActivation(false);
		}
	}

	/// <summary>
	/// Shows all.
	/// </summary>
	private void ShowAll()
	{
		DoActivation(true);

		for (int i = 0; i < health.Parts.Count; i++)
		{
			health.Parts[i].DoRetract(health.PartsParent);
		}
	}

	/// <summary>
	/// Prepares to burrower and go to idle.
	/// </summary>
	/// <returns>The to burrower and go to idle.</returns>
	private IEnumerator PrepareToBurrowerAndGoToSprintOrShootOrIdle()
	{
		if (insideHole)
		{
			Vector2 newPositionAroundPlayer = DrawPositionAroundPlayer;
			transform.SetPositionTo (newPositionAroundPlayer);

			RedefineHole (newPositionAroundPlayer, true, ShortcutWords.Untagged);
			yield return new WaitForSeconds (timeToAttack);
			
			insideHole = false;
		}

		ShowAll ();

		EvaluateNextStatus ();
	}

	/// <summary>
	/// Evaluates the next status.
	/// </summary>
	private void EvaluateNextStatus ()
	{
		if (DistanceFromPlayer > rangeDistance)
		{
			RedefineHole (Pivot, false, ShortcutWords.Untagged);

			StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Idle));
		}
		else if (DrawsBooleanProbability) 
		{
			StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Shoot));
		}
		else 
		{
			RedefineHole (Pivot, false, ShortcutWords.Untagged);

			StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Sprint));
		}
	}

	/// <summary>
	/// Its the was hit out of action.
	/// </summary>
	public override void ItWasHitOutOfAction ()
	{
		base.ItWasHitOutOfAction ();

		ReleaseEye ();
	}

	/// <summary>
	/// Releases the eye.
	/// </summary>
	private void ReleaseEye()
	{
		if (boitataEyes.Count > 0)
		{
			var eye = boitataEyes.Dequeue ();

			eye.DoRelease();
		}
	}

	/// <summary>
	/// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
	/// </summary>
	protected override void OnDrawGizmos ()
	{
		base.OnDrawGizmos ();
		
		if (!debug) return;
		
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (lastMoveTowards, 0.25f);
	}

	#endregion
}