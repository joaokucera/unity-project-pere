using UnityEngine;
using System.Collections;

public class ArrancaLinguasBehavior : BaseEnemy 
{
	#region [ FIELDS ]

	/// <summary>
	/// The melee distance.
	/// </summary>
	[SerializeField]
	private float meleeDistance;// = 1.75f;
	/// <summary>
	/// The speed.
	/// </summary>
	[SerializeField]
	private float returnGuardSpeed;// = 1.5f;
	/// <summary>
	/// The start position.
	/// </summary>
	private Vector2 startPosition;

	#endregion

	#region [ PROPERTIES ]
	
	/// <summary>
	/// Gets the distance from start position.
	/// </summary>
	/// <value>The distance from start position.</value>
	protected float DistanceFromStartPosition 
	{
		get { return Vector2.Distance (Pivot, startPosition); }
	}

	#endregion

	#region [ METHODS ]

	#region implemented abstract members of BaseEnemy

	/// <summary>
	/// Executes the status.
	/// </summary>
	protected override void ExecuteCurrentStatus ()
	{
		StopCoroutine(FollowPathMethodName);

		switch (CurrentStatus)
		{
			case EnemyStatus.Guard: // if GUARD, can go to CHASE.
			{
				StartCoroutine(VerifyDistanceFromPlayerToChase());

				break;
			}
			case EnemyStatus.ReturnToGuard: // if RETURN TO GUARD, can go to CHASE or GUARD.
			{
				speed = returnGuardSpeed;

				StartCoroutine(VerifyDistanceFromPlayerToChaseOrGuard());

				break;
			}
			case EnemyStatus.Chase: // if CHASE, can go to MELEE or RETURN TO GUARD.
			{
				StartCoroutine (DoAttackFeedback ());

				speed = chaseSpeed;
				
				StartCoroutine(VerifyDistanceFromPlayerToMelleOrReturnToGuard());
				
				break;
			}
			case EnemyStatus.Melee: // if MELEE, can go to CHASE
			{
				StartCoroutine(VerifyDistanceFromPlayerToChase());
				
				break;
			}
		}
	}
	
	#endregion

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		startPosition = transform.position;

		CurrentStatus = EnemyStatus.Guard;
	}

	/// <summary>
	/// Verifies the distance from player to chase.
	/// </summary>
	/// <returns>The distance from player to chase.</returns>
	private IEnumerator VerifyDistanceFromPlayerToChase()
	{
		while (!wasHit && (DistanceFromPlayer <= meleeDistance || DistanceFromPlayer > chaseDistance))
		{
			yield return null;
		}

		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Chase));
	}

	/// <summary>
	/// Verifies the distance from player to melle or return to guard.
	/// </summary>
	/// <returns>The distance from player to melle or return to guard.</returns>
	private IEnumerator VerifyDistanceFromPlayerToMelleOrReturnToGuard()
	{
		while (true)
		{
			RequestPath (transform.position, GameContext.PlayerObject.transform.position, OnPathFound);

			if (DistanceFromPlayer <= meleeDistance && isTouching)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Melee));

				break;
			}
			else if (DistanceFromPlayer > rangeDistance)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.ReturnToGuard));

				break;
			}

			yield return null;
		}
	}

	/// <summary>
	/// Verifies the distance from player to guard or chase.
	/// </summary>
	/// <returns>The distance from player to guard or chase.</returns>
	private IEnumerator VerifyDistanceFromPlayerToChaseOrGuard()
	{
		while (true)
		{
			RequestPath (transform.position, startPosition, OnPathFound);

			if (DistanceFromPlayer <= chaseDistance)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Chase));
				
				break;
			}
			else if (DistanceFromStartPosition <= 1f)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Guard));
				
				break;
			}
			
			yield return null;
		}
	}

	/// <summary>
	/// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
	/// </summary>
	protected override void OnDrawGizmos ()
	{
		base.OnDrawGizmos ();

		if (!debug) return;

		Gizmos.color = DistanceFromPlayer <= meleeDistance ? Color.red : Color.white;
		Gizmos.DrawWireSphere (Pivot, meleeDistance);

		Gizmos.color = Color.white;
		Gizmos.DrawLine (Pivot, startPosition);
	}

	#endregion
}