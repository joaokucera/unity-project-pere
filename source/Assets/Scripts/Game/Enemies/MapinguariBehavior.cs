using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class MapinguariBehavior : BaseEnemy 
{
	#region [ FIELDS ]

	/// <summary>
	/// The melee distance.
	/// </summary>
	[SerializeField]
	private float meleeDistance;
	/// <summary>
	/// The wait time when blocking.
	/// </summary>
	[SerializeField]
	private float waitTimeWhenBlocking = 2f;
	/// <summary>
	/// The sprite when blocking.
	/// </summary>
	[SerializeField]
	private Sprite spriteWhenBlocking;
	/// <summary>
	/// The sprite original.
	/// </summary>
	private Sprite spriteOriginal;
	/// <summary>
	/// The sprite renderer.
	/// </summary>
	private SpriteRenderer spriteRenderer;
	/// <summary>
	/// The under attack.
	/// </summary>
	private bool underAttack;

	#endregion

	#region [ METHODS ]

	#region [ PROPERTIES ]

	/// <summary>
	/// Sets a value indicating whether this <see cref="MapinguariBehavior"/> under attack.
	/// </summary>
	/// <value><c>true</c> if under attack; otherwise, <c>false</c>.</value>
	public bool UnderAttack { set { underAttack = value; } }

	#endregion

	#region implemented abstract members of BaseEnemy

	/// <summary>
	/// Executes the status.
	/// </summary>
	protected override void ExecuteCurrentStatus ()
	{
		StopCoroutine(FollowPathMethodName);

		switch (CurrentStatus)
		{
			case EnemyStatus.Idle: // if IDLE, can go to BLOCKER or CHASE.
			{
				StartCoroutine(VerifyDistanceFromPlayerToBlockerOrChase());

				break;
			}
			case EnemyStatus.Blocker: // if BLOCKER, can go to CHASE.
			{
				StartCoroutine(VerifyDistanceFromPlayerToChase());

				break;
			}
			case EnemyStatus.Chase: // if CHASE, can go to BLOCK or MELEE or IDLE.
			{
				speed = chaseSpeed;
				
				StartCoroutine(VerifyDistanceFromPlayerToBlockOrMelleOrIdle());
				
				break;
			}
			case EnemyStatus.Melee: // if MELEE, can go to BLOCK or CHASE
			{
				StartCoroutine(VerifyDistanceFromPlayerToBlockerOrChase());
				
				break;
			}
		}
	}
	
	#endregion

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		spriteRenderer = renderer as SpriteRenderer;
		spriteOriginal = spriteRenderer.sprite;

		CurrentStatus = EnemyStatus.Idle;
	}

	/// <summary>
	/// Verifies the distance from player to blocker or chase.
	/// </summary>
	/// <returns>The distance from player to blocker or chase.</returns>
	private IEnumerator VerifyDistanceFromPlayerToBlockerOrChase()
	{
		while (!wasHit && (DistanceFromPlayer <= meleeDistance || DistanceFromPlayer > chaseDistance))
		{
			if (underAttack)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Blocker));

				yield break;
			}

			yield return null;
		}

		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Chase));
	}

	/// <summary>
	/// Verifies the distance from player to block or melle or idle.
	/// </summary>
	/// <returns>The distance from player to block or melle or idle.</returns>
	private IEnumerator VerifyDistanceFromPlayerToBlockOrMelleOrIdle()
	{
		while (true)
		{
			RequestPath (transform.position, GameContext.PlayerObject.transform.position, OnPathFound);

			if (underAttack)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Blocker));
				
				break;
			}
			else if (DistanceFromPlayer <= meleeDistance && isTouching)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Melee));

				break;
			}
			else if (DistanceFromPlayer > rangeDistance)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Idle));

				break;
			}

			yield return null;
		}
	}

	/// <summary>
	/// Verifies the distance from player to chase.
	/// </summary>
	/// <returns>The distance from player to chase.</returns>
	private IEnumerator VerifyDistanceFromPlayerToChase()
	{
		underAttack = false;

		spriteRenderer.sprite = spriteWhenBlocking;
		yield return new WaitForSeconds (waitTimeWhenBlocking);
		spriteRenderer.sprite = spriteOriginal;

		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Chase));
	}

	/// <summary>
	/// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
	/// </summary>
	protected override void OnDrawGizmos ()
	{
		base.OnDrawGizmos ();

		if (!debug) return;

		Gizmos.color = DistanceFromPlayer <= meleeDistance ? Color.red : Color.white;
		Gizmos.DrawWireSphere (Pivot, meleeDistance);
	}

	#endregion
}