using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorpoSecoMasterBehavior : BaseEnemy
{
	#region [ FIELDS ]

	private const string SpawnSlaveMethodName = "SpawnSlave";
	/// <summary>
	/// The corpo seco slave resource file.
	/// </summary>
	private const string CorpoSecoSlaveResourceFile = "Corpo Seco Slave";
	/// <summary>
	/// The melee distance.
	/// </summary>
	[SerializeField]
	private float meleeDistance;
	/// <summary>
	/// The size of the flock.
	/// </summary>
	[SerializeField]
	private int flockSize = 10;
	/// <summary>
	/// The flock.
	/// </summary>
	private List<CorpoSecoSlaveBehavior> flock = new List<CorpoSecoSlaveBehavior>();
	/// <summary>
	/// The corpo seco prefab.
	/// </summary>
	private CorpoSecoSlaveBehavior corpoSecoPrefab;
	/// <summary>
	/// The time to spawn.
	/// </summary>
	[SerializeField]
	private float timeToSpawn = 2f;

	#endregion

	#region [ METHODS ]
	
	#region implemented abstract members of BaseEnemy
	
	/// <summary>
	/// Executes the status.
	/// </summary>
	protected override void ExecuteCurrentStatus ()
	{
		StopCoroutine(FollowPathMethodName);

		CancelInvoke (SpawnSlaveMethodName);

		switch (CurrentStatus)
		{
			case EnemyStatus.Idle: // if IDLE, can go to CHASE.
			{
				StartCoroutine(VerifyDistanceFromPlayerToChase());
				
				break;
			}
			case EnemyStatus.Chase: // if CHASE, can go to MELEE or IDLE.
			{
				StartCoroutine (DoAttackFeedback ());

				speed = chaseSpeed;

				StartCoroutine(VerifyDistanceFromPlayerToMelleOrIdle());
				
				InvokeRepeating(SpawnSlaveMethodName, 0f, timeToSpawn);

				break;
			}
			case EnemyStatus.Melee: // if MELEE, can go to CHASE.
			{
				StartCoroutine(VerifyDistanceFromPlayerToChase());
				
				break;
			}
		}
	}
	
	#endregion

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		corpoSecoPrefab = Resources.Load <CorpoSecoSlaveBehavior>(string.Format("{0}/{1}", EnemiesResourceFolder, CorpoSecoSlaveResourceFile));

		CurrentStatus = EnemyStatus.Idle;
	}

	/// <summary>
	/// Spawns the slaves.
	/// </summary>
	private void SpawnSlave()
	{
		if (flock.Count < flockSize)
		{
			Vector2 separation = transform.position;
			for (int i = 0; i < flock.Count; i++)
			{
				var other = flock[i];
				if (other)
				{
					Vector2 relativePosition = new Vector2(transform.position.x, transform.position.y) + new Vector2(other.transform.position.x, other.transform.position.y);	
					separation += relativePosition / relativePosition.sqrMagnitude;
				}
			}
			Vector2 randomize = RandomizePosition ();

			var newSlave = Instantiate(corpoSecoPrefab, separation + randomize, Quaternion.identity) as CorpoSecoSlaveBehavior;
			newSlave.transform.SetParent(GameContext.DynamicObjects);
			
			flock.Add (newSlave);
		}
	}

	/// <summary>
	/// Verifies the distance from player to chase.
	/// </summary>
	/// <returns>The distance from player to chase.</returns>
	private IEnumerator VerifyDistanceFromPlayerToChase()
	{
		while (!wasHit && (DistanceFromPlayer <= meleeDistance || DistanceFromPlayer > chaseDistance))
		{
			yield return null;
		}
		
		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Chase));
	}

	/// <summary>
	/// Verifies the distance from player to melle or idle.
	/// </summary>
	/// <returns>The distance from player to melle or idle.</returns>
	private IEnumerator VerifyDistanceFromPlayerToMelleOrIdle()
	{
		while (true)
		{
			RequestPath (transform.position, GameContext.PlayerObject.transform.position, OnPathFound);
			
			if (DistanceFromPlayer <= meleeDistance && isTouching)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Melee));
				
				break;
			}
			else if (DistanceFromPlayer > rangeDistance)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Idle));
				
				break;
			}
			
			yield return null;
		}
	}

	/// <summary>
	/// This function is called when the MonoBehaviour will be destroyed.
	/// </summary>
	protected override void OnDestroy ()
	{
		for (int i = 0; i < flock.Count; i++) 
		{
			if (flock[i])
			{
				flock[i].AutoDestruction ();
			}
		}
	}

	/// <summary>
	/// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
	/// </summary>
	protected override void OnDrawGizmos ()
	{
		base.OnDrawGizmos ();
		
		if (!debug) return;
		
		Gizmos.color = DistanceFromPlayer <= meleeDistance ? Color.red : Color.white;
		Gizmos.DrawWireSphere (Pivot, meleeDistance);
	}

	#endregion
}