using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer), typeof(BoxCollider2D), typeof(Rigidbody2D))]
[RequireComponent(typeof(DistanceJoint2D))]
public class BoitataPart : BaseScript
{
	#region [ FIELDS ]

	/// <summary>
	/// The behavior.
	/// </summary>
	private BoitataBehavior mainBehavior;
	/// <summary>
	/// The health.
	/// </summary>
	private BoitataHealth mainHealth;
	/// <summary>
	/// The joint.
	/// </summary>
	private DistanceJoint2D joint;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="BaseEnemy"/> is active.
	/// </summary>
	/// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
	public bool Active 
	{
		get 
		{ 
			return gameObject.activeInHierarchy; 
		}
		set 
		{ 
			gameObject.SetActive(value);
		}
	}
	/// <summary>
	/// Gets the pivot.
	/// </summary>
	/// <value>The pivot.</value>
	public virtual Vector2 Pivot
	{
		get 
		{
			return new Vector2(transform.position.x, transform.position.y + renderer.bounds.size.y / 2); 
		}
	}

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		collider2D.isTrigger = true;

		rigidbody2D.fixedAngle = true;
		rigidbody2D.isKinematic = false;

		joint = GetComponent<DistanceJoint2D> ();

		mainBehavior = GetComponentInParent<BoitataBehavior> ();
		mainHealth = GetComponentInParent<BoitataHealth> ();
	}

	/// <summary>
	/// Dos the retract.
	/// </summary>
	/// <param name="originalParent">Original parent.</param>
	public void DoRetract(Transform originalParent)
	{
		transform.SetParent (originalParent);
		transform.SetLocalPositionTo(Vector2.zero);

		transform.SetParent (GameContext.DynamicObjects);

		DoActivation (true);
	}

	/// <summary>
	/// Dos the activation.
	/// </summary>
	/// <param name="enable">If set to <c>true</c> enable.</param>
	public void DoActivation(bool enable)
	{
		renderer.enabled = enable;
		collider2D.enabled = enable;
	}

	/// <summary>
	/// Initialize the specified connected, distance and scale.
	/// </summary>
	/// <param name="connected">Connected.</param>
	/// <param name="distance">Distance.</param>
	/// <param name="scale">Scale.</param>
	public void Initialize(Rigidbody2D connected, float distance, Vector3 scale)
	{
		joint.connectedBody = connected;
		joint.distance = distance;
		joint.maxDistanceOnly = true;

		transform.SetParent (GameContext.DynamicObjects);
		transform.localScale = scale;
	}

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this object (2D physics only).
	/// </summary>
	/// <param name="collider">Collider.</param>
	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		mainHealth.HitVerification(collider);

		mainBehavior.TryHole (collider, this);
	}

	/// <summary>
	/// Raises the blink event.
	/// </summary>
	public void OnBlink (Color originalColor, Color hitColor)
	{
		Color color = renderer.material.color;
		
		if (color == originalColor)
		{
			color.g = hitColor.g;
			color.b = hitColor.b;
		}
		else if (color == hitColor)
		{
			color.g = originalColor.g;
			color.b = originalColor.b;
		}
		
		renderer.material.color = color;
	}

	#endregion
}