using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IpupiaraBehavior : BaseEnemy 
{
	#region [ FIELDS ]

	/// <summary>
	/// The weapon.
	/// </summary>
	private EnemyWeapon weapon;
	/// <summary>
	/// The probability percentage to burrower.
	/// </summary>
	[SerializeField]
	private int probabilityPercentageToBurrower = 50;
	/// <summary>
	/// The boolean probability pool.
	/// </summary>
	private List<bool> booleanProbabilityPool = new List<bool>();
	/// <summary>
	/// The burrower places.
	/// </summary>
	[SerializeField]
	private Transform[] burrowerPlaces;
	/// <summary>
	/// The allowed to burrower.
	/// </summary>
	private bool allowedToBurrower = true;

	#endregion
	
	#region [ PROPERTIES ]
	
	/// <summary>
	/// Gets a value indicating whether this <see cref="IpupiaraBehavior"/> draws boolean probability.
	/// </summary>
	/// <value><c>true</c> if draws boolean probability; otherwise, <c>false</c>.</value>
	protected bool DrawsBooleanProbability
	{
		get 
		{
			int randomProbability = Random.Range (0, booleanProbabilityPool.Count);
			
			return booleanProbabilityPool[randomProbability];
		}
	}

	/// <summary>
	/// Gets the draws burrower place position.
	/// </summary>
	/// <value>The draws burrower place position.</value>
	protected Vector2 DrawsBurrowerTilePlacePosition
	{
		get 
		{
			int randomProbability = Random.Range (0, burrowerPlaces.Length);

			return burrowerPlaces[randomProbability].transform.position;
		}
	}
	
	#endregion
	
	#region [ METHODS ]
	
	#region implemented abstract members of BaseEnemy
	
	/// <summary>
	/// Executes the status.
	/// </summary>
	protected override void ExecuteCurrentStatus ()
	{
		switch (CurrentStatus)
		{
			case EnemyStatus.Idle: // if IDLE, can go to CHASE.
			{
				StartCoroutine(VerifyDistanceFromPlayerToChase());

				break;
			}
			case EnemyStatus.Chase: // if CHASE, can go to SHOOT, BURROWER or IDLE.
			{
				VerifyDistanceFromPlayerToShootOrBurrowerOrIdle();

				break;
			}
			case EnemyStatus.Shoot: // if SHOOT, can go to IDLE.
			{
				StartCoroutine(PrepareToShootAndGoToIdle());

				break;
			}
			case EnemyStatus.Burrower: // if BURROWER, can go to IDLE.
			{
				StartCoroutine(PrepareToBurrowerAndGoToIdle());

				break;
			}
		}
	}
	
	#endregion
	
	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		if (burrowerPlaces.Length == 0)
		{
			Debug.LogError("Has not been defined a list of burrower places!");
		}
		for (int i = 0; i < burrowerPlaces.Length; i++)
		{
			burrowerPlaces[i].transform.SetPositionZ(transform.position.z + 1f);
		}

		rigidbody2D.isKinematic = true;

		weapon = GetComponentInChildren<EnemyWeapon> ();
		booleanProbabilityPool = MethodService.CreateBooleanProbabilityPool (probabilityPercentageToBurrower);

		CurrentStatus = EnemyStatus.Idle;
	}
	
	/// <summary>
	/// Verifies the distance from player to chase.
	/// </summary>
	/// <returns>The distance from player to chase.</returns>
	private IEnumerator VerifyDistanceFromPlayerToChase()
	{
		while (!wasHit && DistanceFromPlayer > chaseDistance)
		{
			yield return null;
		}
		
		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Chase));
	}

	/// <summary>
	/// Verifies the distance from player to shoot or burrower or idle.
	/// </summary>
	/// <returns>The distance from player to shoot or burrower or idle.</returns>
	private void VerifyDistanceFromPlayerToShootOrBurrowerOrIdle()
	{
		if (DistanceFromPlayer > rangeDistance)
		{
			StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Idle));
		}
		else if (allowedToBurrower && DrawsBooleanProbability)
		{
			allowedToBurrower = false;

			StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Burrower));
		}
		else
		{
			allowedToBurrower = true;

			StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Shoot));
		}
	}

	/// <summary>
	/// Prepares to shoot and go to idle.
	/// </summary>
	/// <returns>The to shoot and go to idle.</returns>
	private IEnumerator PrepareToShootAndGoToIdle()
	{
		weapon.OnShoot (Pivot);

		while (!weapon.ShotReleased)
		{
			transform.ShakeAtLocalPosition();

			yield return null;
		}

		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Idle));
	}

	/// <summary>
	/// Prepares to burrower and go to idle.
	/// </summary>
	/// <returns>The to burrower and go to idle.</returns>
	private IEnumerator PrepareToBurrowerAndGoToIdle()
	{
		Vector3 originalScale = transform.localScale;
		Vector3 multiplier = Vector3.one * 5f;

		while (transform.localScale != Vector3.zero) 
		{
			transform.SetScale(-multiplier * Time.deltaTime, 0, 0, 0, originalScale.x, originalScale.y, originalScale.z); 
			
			yield return null;
		}

		transform.SetPositionTo (DrawsBurrowerTilePlacePosition);

		while (transform.localScale != originalScale) 
		{
			transform.SetScale(multiplier * Time.deltaTime, 0, 0, 0, originalScale.x, originalScale.y, originalScale.z); 
			
			yield return null;
		}

		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Idle));
	}

	#endregion
}