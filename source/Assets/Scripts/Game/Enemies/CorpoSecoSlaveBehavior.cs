using UnityEngine;
using System.Collections;
using System;

public class CorpoSecoSlaveBehavior : BaseEnemy
{
	#region [ FIELDS ]

	/// <summary>
	/// The melee distance.
	/// </summary>
	[SerializeField]
	private float meleeDistance;
	/// <summary>
	/// The animator.
	/// </summary>
	private Animator animator;
	/// <summary>
	/// The death parameter.
	/// </summary>
	private int deathParameter = Animator.StringToHash("Death");

	#endregion

	#region [ METHODS ]

	#region implemented abstract members of BaseEnemy

	/// <summary>
	/// Executes the status.
	/// </summary>
	protected override void ExecuteCurrentStatus ()
	{
		StopCoroutine(FollowPathMethodName);

		switch (CurrentStatus)
		{
			case EnemyStatus.Chase: // if CHASE, can go to MELEE.
			{
				speed = chaseSpeed;

				StartCoroutine(VerifyDistanceFromPlayerToMelle());

				break;
			}
			case EnemyStatus.Melee: // if MELEE, can go to CHASE.
			{
				StartCoroutine(VerifyDistanceFromPlayerToChase());
				
				break;
			}
		}
	}
	
	#endregion

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		animator = GetComponent<Animator> ();

		CurrentStatus = EnemyStatus.Chase;
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	protected override void Update ()
	{
		transform.ShakeAtLocalPosition();
	}

	/// <summary>
	/// Raises the chase event.
	/// </summary>
	protected IEnumerator VerifyDistanceFromPlayerToMelle()
	{
		while (true)
		{
			RequestPath (transform.position, GameContext.PlayerObject.transform.position, OnPathFound);

			if (DistanceFromPlayer <= meleeDistance && isTouching)
			{
				StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Melee));
				
				break;
			}
			
			yield return null;
		}
	}

	/// <summary>
	/// Verifies the distance from player to chase.
	/// </summary>
	/// <returns>The distance from player to chase.</returns>
	private IEnumerator VerifyDistanceFromPlayerToChase()
	{
		while (DistanceFromPlayer <= meleeDistance)
		{
			yield return null;
		}
		
		StartCoroutine (WaitTimeToChangeStatus (EnemyStatus.Chase));
	}

	/// <summary>
	/// Autos the destruction.
	/// </summary>
	public void AutoDestruction()
	{
		animator.SetTrigger (deathParameter);
	}

	/// <summary>
	/// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
	/// </summary>
	protected override void OnDrawGizmos ()
	{
		base.OnDrawGizmos ();
		
		if (!debug) return;
		
		Gizmos.color = DistanceFromPlayer <= meleeDistance ? Color.red : Color.white;
		Gizmos.DrawWireSphere (Pivot, meleeDistance);
	}

	#endregion
}