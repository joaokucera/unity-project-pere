using UnityEngine;
using System.Collections;
using System;

public class EnemyHealth : BaseHealth 
{
	#region [ FIELDS ]

	/// <summary>
	/// The color of the original.
	/// </summary>
	protected Color originalColor;
	/// <summary>
	/// The color of the hit.
	/// </summary>
	protected Color hitColor;
	/// <summary>
	/// The base enemy.
	/// </summary>
	protected BaseEnemy baseEnemy;

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Raises the death event.
	/// </summary>
	protected override void OnDeath ()
	{
		print(string.Format("The {0} is dead!", gameObject.name));

		GameContext.ParticlesRepository.SpawnParticle (ParticleType.FlashLight, baseEnemy.Pivot, transform.rotation);

		baseEnemy.Active = false;
	}

	/// <summary>
	/// Raises the blink event.
	/// </summary>
	protected override void OnBlink ()
	{
		Color color = renderer.material.color;

		if (color == originalColor)
		{
			color.g = hitColor.g;
			color.b = hitColor.b;
		}
		else if (color == hitColor)
		{
			color.g = originalColor.g;
			color.b = originalColor.b;
		}

		renderer.material.color = color;
	}

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		hitAgainstOwnerAttackTag = OwnerAttackTag.PlayerAttack;

		originalColor = renderer.material.color;
		hitColor = Color.red;

		baseEnemy = GetComponent<BaseEnemy> ();
	}

	/// <summary>
	/// Raises the recover event.
	/// </summary>
	protected override void OnRecover()
	{
		base.OnRecover();

		renderer.material.color = originalColor;
	}

	/// <summary>
	/// Raises the hit event.
	/// </summary>
	protected override void OnHit ()
	{
		base.OnHit ();

		if (baseEnemy)
		{
			baseEnemy.ItWasHitOutOfAction();
		}
	}

	#endregion
}