﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class AmmoSettings 
{
	public float Speed;
	public AmmoSettingsDamage DamageSettings;

	public Vector3 Direction { get; set; }
}

[Serializable]
public class AmmoSettingsDamage
{
	public int Current;

	public int Extra { get; set; }
	public float Force { get; set; }
	public float Time { get; set; }

	public int Total { get { return Current + Extra; } }
}