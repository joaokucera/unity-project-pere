using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public abstract class BaseHealth : BaseScript
{
    #region [ FIELDS ]

	/// <summary>
	/// The owner tag.
	/// </summary>
	protected OwnerAttackTag hitAgainstOwnerAttackTag;
	/// <summary>
	/// The was hit.
	/// </summary>
	protected bool wasHit = false;
	/// <summary>
	/// The initial health.
	/// </summary>
	[SerializeField]
	protected int initialHealth;
	/// <summary>
	/// The current heath.
	/// </summary>
	protected int currentHeath;
	/// <summary>
	/// The initial blink effect time.
	/// </summary>
	[SerializeField]
	protected float initialBlinkEffectTime;
	/// <summary>
	/// The blink effect time.
	/// </summary>
	protected float blinkEffectTime;
	/// <summary>
	/// The recovering time.
	/// </summary>
	[SerializeField]
	protected float recoveryTime;
	/// <summary>
	/// The extra health.
	/// </summary>
	protected int extraHealth;
    
    #endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets the initial health.
	/// </summary>
	/// <value>The initial health.</value>
	public int InitialHealth { get { return initialHealth; } }
	/// <summary>
	/// Gets the current heath.
	/// </summary>
	/// <value>The current heath.</value>
	public int CurrentHealth { get { return currentHeath; } }
	/// <summary>
	/// Gets the extra health.
	/// </summary>
	/// <value>The extra health.</value>
	public int ExtraHealth { get { return extraHealth; } }

	#endregion

    #region [ METHODS ]

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    /// </summary>
	protected override void Start ()
    {
		currentHeath = initialHealth;

		blinkEffectTime = initialBlinkEffectTime;
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
	protected override void Update ()
    {
        UpdateBlink ();
    }

	/// <summary>
	/// Updates the blink.
	/// </summary>
	private void UpdateBlink ()
	{
		if (wasHit)
		{
			blinkEffectTime -= Time.deltaTime;

			if (blinkEffectTime < 0) 
			{
				blinkEffectTime = initialBlinkEffectTime;

				OnBlink ();
			}
		}
	}

    /// <summary>
    /// When trigger is enabled detect other collider against this one.
    /// </summary>
	protected override void OnTriggerEnter2D (Collider2D collider)
    {
		HitVerification (collider);
    }

	/// <summary>
	/// Hits the verification.
	/// </summary>
	/// <param name="collider">Collider.</param>
	public virtual void HitVerification (Collider2D collider)
	{
		if (collider.CompareTag(hitAgainstOwnerAttackTag.ToString()))
		{
			BaseAmmo ammo = collider.GetComponent<BaseAmmo> ();

			Damage (ammo.AmmoSettings.DamageSettings.Total);

			ammo.DoActive(false);
		}
	}

	/// <summary>
	/// Heal the specified heal.
	/// </summary>
	/// <param name="heal">Heal.</param>
	public void Heal(int heal)
	{
		currentHeath += heal;

		currentHeath = Mathf.Clamp (currentHeath, 0, initialHealth);
	}

    /// <summary>
    /// Damage the specified loss.
    /// </summary>
    /// <param name="loss">Loss.</param>
    public virtual void Damage(int loss)
    {
		int difference = loss - extraHealth;

		if (extraHealth > 0)
		{
			extraHealth -= loss;
		}

		if (difference > 0)
		{
			currentHeath -= difference;
		}
		
		OnHit ();
		
		if (currentHeath <= 0)
		{	
			OnDeath();
		}
    }

	/// <summary>
	/// Instants the death.
	/// </summary>
	public void FullDamage()
	{
		var totalLoss = currentHeath;

		Damage (totalLoss);
	}

	/// <summary>
	/// Raises the hit event.
	/// </summary>
	protected virtual void OnHit()
	{
		wasHit = true;

		Invoke("OnRecover", recoveryTime);
	}

	/// <summary>
	/// Raises the recover event.
	/// </summary>
	protected virtual void OnRecover()
	{
		wasHit = false;
	}

	/// <summary>
	/// Raises the death event.
	/// </summary>
	protected virtual void OnDeath() { }

    /// <summary>
    /// Raises the blink event.
    /// </summary>
	protected virtual void OnBlink() { }

    #endregion
}