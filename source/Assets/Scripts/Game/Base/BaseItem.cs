﻿using System;
using System.Collections;
using UnityEngine;
using System.Text;

[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public abstract class BaseItem : BaseScript
{
    #region [ FIELDS ]

	/// <summary>
	/// The color.
	/// </summary>
	[SerializeField]
	protected Color color;
	/// <summary>
	/// The description.
	/// </summary>
	[SerializeField]
	protected string description;
	/// <summary>
	/// The sprite renderer.
	/// </summary>
	private SpriteRenderer spriteRenderer;
    /// <summary>
    /// The type of item.
    /// </summary>
    protected ItemType itemType;
	/// <summary>
	/// The origin parent.
	/// </summary>
	private Transform originalParent;

    #endregion

    #region [ PROPERTIES ]

	/// <summary>
	/// Gets the pivot.
	/// </summary>
	/// <value>The pivot.</value>
	public Vector2 Pivot 
	{
		get 
		{ 
			return new Vector2 (transform.position.x, transform.position.y + renderer.bounds.size.y / 2);
		}
	}
	/// <summary>
	/// Gets the item sprite.
	/// </summary>
	/// <value>The item sprite.</value>
	public Sprite ItemSprite { get { return spriteRenderer.sprite; } }
    /// <summary>
    /// Get the item type.
    /// </summary>
    public ItemType ItemType { get { return itemType; } }
    /// <summary>
    /// Set if game object is active.
    /// </summary>
    private bool Active { set { gameObject.SetActive(value); } }
	/// <summary>
	/// Gets a value indicating whether this instance is weapon.
	/// </summary>
	/// <value><c>true</c> if this instance is weapon; otherwise, <c>false</c>.</value>
	public bool IsWeapon { get { return itemType.ToString().Contains("Weapon"); } }

    #endregion

    #region [ METHODS ]

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    /// </summary>
	protected override void Start ()
    {
		transform.SetFakeDepth ();
		collider2D.isTrigger = true;

		spriteRenderer = renderer as SpriteRenderer;

		originalParent = transform.parent;
    }

    /// <summary>
    /// Raises the collected event.
    /// </summary>
    /// <param name="parent">Parent.</param>
    public void OnCollected(Transform parent)
    {
		GameContext.ParticlesRepository.SpawnParticle (ParticleType.Shockwave, Pivot, transform.rotation);
        transform.parent = parent;

        Active = false;
    }

    /// <summary>
    /// Raises the use event.
    /// </summary>
    public virtual void OnUse()
    {
		print ("Utilizou um " + gameObject.name);

        transform.parent = originalParent;
    }

	/// <summary>
	/// Raises the tooltip event.
	/// </summary>
	public string OnTooltip()
	{
		string name = TooltipName();
		string hexColor = HexColor ();
		string tootip = description;
		
		return string.Format ("<color={0}><size=24>{1}</size></color>{2}{3}", hexColor, name, Environment.NewLine, tootip);
	}

	/// <summary>
	/// Hexs the color.
	/// </summary>
	/// <returns>The color.</returns>
	private string HexColor()
	{
		return string.Format("#{0:X2}{1:X2}{2:X2}", (int)(color.r * 255), (int)(color.g * 255), (int)(color.b * 255));
	}

	/// <summary>
	/// Tooltips the name.
	/// </summary>
	/// <returns>The name.</returns>
	private string TooltipName()
	{
		string name = itemType.ToString ().Replace ("Item", string.Empty).Replace("Weapon_", string.Empty);

		StringBuilder builder = new StringBuilder ();
		builder.Append (name [0]);

		for (int i = 1; i < name.Length; i++) 
		{
			char letter = name[i];

			if (char.IsUpper(letter))
			{
				builder.Append(" ");
			}

			builder.Append(letter);
		}

		return builder.ToString ();
	}

    #endregion
}