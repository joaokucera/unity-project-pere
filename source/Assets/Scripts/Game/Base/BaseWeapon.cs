using UnityEngine;
using System.Collections;

public class BaseWeapon : GenericPooling
{
	#region [ FIELDS ]
	
	/// <summary>
	/// The name of the do shoot method.
	/// </summary>
	protected string ExecuteMethodName = "Execute";
	/// <summary>
	/// The name of the stop extra damage method.
	/// </summary>
	protected string StopExtraDamageMethodName = "StopExtraDamage";

	/// <summary>
	/// The weapon settings.
	/// </summary>
	[SerializeField]
	protected WeaponSettings weaponSettings;
	
	#endregion

	#region [ METHODS ]
	
	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();
		
		var parentRenderer = GetComponentInParent<Renderer> ();
		transform.position = new Vector2 (transform.position.x, transform.parent.position.y + parentRenderer.bounds.size.y / 2);

		if (weaponSettings.LoadingSettings.Enabled)
		{
			weaponSettings.LoadingSettings.Current = weaponSettings.LoadingSettings.Minimum;
		}
	}
	
	/// <summary>
	/// Raises the update event.
	/// </summary>
	protected override void Update ()
	{
		if (!InputControl.IsShotLoading && weaponSettings.CooldownSettings.Value > 0)
		{
			weaponSettings.CooldownSettings.Value -= Time.deltaTime;
			weaponSettings.CooldownSettings.Value = Mathf.Clamp (weaponSettings.CooldownSettings.Value, 0, weaponSettings.CooldownSettings.Rate);
		}
	}

	/// <summary>
	/// Executes the ranged attack.
	/// </summary>
	/// <param name="lastAimPosition">Last aim position.</param>
	/// <param name="angle">Angle.</param>
	/// <param name="useAmmoAsDirection">If set to <c>true</c> use ammo as direction.</param>
	protected void ExecuteRangedAttack(Vector3 lastAimPosition, Quaternion angle, bool useAmmoAsDirection)
	{
		// Get shot from pool.
		GameObject ammo = GetObjectFromPool (transform.position, angle);
		
		if (ammo != null)
		{
			weaponSettings.CooldownSettings.Reset();

			Vector3 direction = useAmmoAsDirection ? ammo.transform.up : lastAimPosition;

			weaponSettings.AmmoSettings.Direction = direction;
			weaponSettings.AmmoSettings.DamageSettings.Force = weaponSettings.TimeSettings.Enabled ? weaponSettings.AmmoSettings.Speed : weaponSettings.AmmoSettings.Speed * weaponSettings.LoadingSettings.Current;
			weaponSettings.AmmoSettings.DamageSettings.Time = weaponSettings.TimeSettings.Enabled ? weaponSettings.TimeSettings.Value : weaponSettings.LoadingSettings.Current;

			ammo.SendMessage(ExecuteMethodName, weaponSettings);

			EvaluateKickbackShot(-direction);
		}
	}

	/// <summary>
	/// Executes the melee attack.
	/// </summary>
	protected void ExecuteMeleeAttack(MeleeAttack melee)
	{
		melee.Execute (weaponSettings);
	}

	/// <summary>
	/// Evaluates the kickback shot.
	/// </summary>
	private void EvaluateKickbackShot(Vector3 direction)
	{
		// Kickback shot.
		if (weaponSettings.KickbackSettings.Enabled && weaponSettings.LoadingSettings.Current == weaponSettings.LoadingSettings.Maximum)
		{
			GameContext.PlayerObject.rigidbody2D.AddForce(direction * weaponSettings.KickbackSettings.Force);
			
			GameContext.CameraShake.Shake(0.005f); 
		}
		else
		{
			GameContext.CameraShake.Shake(0.001f);
		}
	}

	/// <summary>
	/// Increases the damage.
	/// </summary>
	/// <param name="extraDamageValue">Extra damage value.</param>
	/// <param name="extraDamageEffectTime">Extra damage effect time.</param>
	public void IncreaseDamage (int extraDamageValue, float extraDamageEffectTime)
	{
		weaponSettings.AmmoSettings.DamageSettings.Extra = extraDamageValue;
		
		Invoke (StopExtraDamageMethodName, extraDamageEffectTime);
	}
	
	/// <summary>
	/// Stops the extra damage.
	/// </summary>
	private void StopExtraDamage()
	{
		weaponSettings.AmmoSettings.DamageSettings.Extra = 0;
	}
	
	#endregion
}