using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class GenericPooling : BaseScript
{
    #region [ FIELDS ]

    /// <summary>
    /// The prefab.
    /// </summary>
    [SerializeField]
    protected GameObject prefab;
    /// <summary>
    /// The initial size of pool.
    /// </summary>
    [SerializeField]
    protected int poolSize = 1;
    /// <summary>
    /// If pool can grow or not.
    /// </summary>
    [SerializeField]
	protected bool poolCanGrow = false;
    /// <summary>
    /// A pool of prefab object.
    /// </summary>
    private List<GameObject> pool = new List<GameObject>();

    #endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Sets the prefab.
	/// </summary>
	/// <value>The prefab.</value>
	public GameObject Prefab { set { prefab = value; } }
	/// <summary>
	/// Sets the size of the pool.
	/// </summary>
	/// <value>The size of the pool.</value>
	public int PoolSize { set { poolSize = value; } }
	/// <summary>
	/// Sets a value indicating whether this <see cref="GenericPooling"/> pool can grow.
	/// </summary>
	/// <value><c>true</c> if pool can grow; otherwise, <c>false</c>.</value>
	public bool PoolCanGrow { set { poolCanGrow = value; } }
	
	#endregion

    #region [ METHODS ]

    /// <summary>
    /// Called when script start.
    /// </summary>
    protected override void Start ()
    {
        StartPool();
    }

    /// <summary>
    /// Initializes pool.
    /// </summary>
    private void StartPool()
    {
        if (prefab == null)
        {
            Debug.LogError("Has not been defined a prefab!");
        }

        for (int i = 0; i < poolSize; i++)
        {
            CreateNewObject();
        }
    }

    /// <summary>
    /// Get a game object from pool.
    /// </summary>
    /// <param name="position">The positoin.</param>
    /// <param name="active">Is if active or not.</param>
    /// <returns>Returns a game object from pool.</returns>
    public GameObject GetObjectFromPool(Vector2 position, Quaternion rotation, bool active = true)
    {
		for (int i = 0; i < pool.Count; i++)
        {
			GameObject obj = pool[i];

            if (!obj.activeInHierarchy)
            {
                return PrepareObjectToResponse(obj, position, rotation, active);
            }
        }

        if (poolCanGrow)
        {
            GameObject obj = CreateNewObject();

            return PrepareObjectToResponse(obj, position, rotation, active);
        }

        return null;
    }

    /// <summary>
    /// Creates a new game object.
    /// </summary>
    /// <returns>Returns a new game object.</returns>
    private GameObject CreateNewObject()
    {
        GameObject newObject = Instantiate(prefab) as GameObject;
        newObject.SetActive(false);
		newObject.transform.SetParent (GameContext.DynamicObjects);

        pool.Add(newObject);

        return newObject;
    }

    /// <summary>
    /// Prepare a game object to response.
    /// </summary>
    /// <param name="obj">The game object.</param>
    /// <param name="position">The position.</param>
    /// <param name="active">Is is active or not.</param>
    /// <returns>Returns the game object.</returns>
    private GameObject PrepareObjectToResponse(GameObject obj, Vector2 position, Quaternion rotation, bool active)
    {
        obj.transform.position = position;
        obj.transform.rotation = rotation;
        obj.SetActive(active);

        return obj;
    }

    #endregion
}