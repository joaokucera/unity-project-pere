using UnityEngine;
using System.Collections;

public static class GameContext
{
	#region [ FIELDS ]

	/// <summary>
	/// The main camera.
	/// </summary>
	private static Camera mainCamera;
	/// <summary>
	/// The camera settings.
	/// </summary>
	private static CameraSettings cameraSettings;
	/// <summary>
	/// The camera shake.
	/// </summary>
	private static CameraShake cameraShake;
	/// <summary>
	/// The camera shader.
	/// </summary>
	private static CameraShader cameraShader;
	/// <summary>
	/// The player object.
	/// </summary>
	private static GameObject playerObject;
	/// <summary>
	/// The player movement script.
	/// </summary>
	private static PlayerMovement playerMovementScript;
	/// <summary>
	/// The player health script.
	/// </summary>
	private static PlayerHealth playerHealthScript;
	/// <summary>
	/// The player weapon control.
	/// </summary>
	private static PlayerWeaponControl playerWeaponControl;
	/// <summary>
	/// The item repository.
	/// </summary>
	private static ItemsRepository itemRepository;
	/// <summary>
	/// The generic repository.
	/// </summary>
	private static Transform dynamicObjects;
	/// <summary>
	/// The inventory system.
	/// </summary>
	private static InventorySystem inventorySystem;
	/// <summary>
	/// The status bar system.
	/// </summary>
	private static StatusBarSystem statusBarSystem;
	/// <summary>
	/// The pause menu.
	/// </summary>
	private static PauseMenu pauseMenu;
	/// <summary>
	/// The particles repository.
	/// </summary>
	private static ParticlesRepository particlesRepository;
	/// <summary>
	/// The grid.
	/// </summary>
	private static Grid grid;
	/// <summary>
	/// The game over.
	/// </summary>
	private static GameOver gameOver;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="GlobalVariables"/> is paused.
	/// </summary>
	/// <value><c>true</c> if paused; otherwise, <c>false</c>.</value>
	public static bool Paused
	{
		get
		{
			if (Time.timeScale == 0f)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		set
		{
			if (value)
			{
				Time.timeScale = 0f;
			}
			else
			{
				Time.timeScale = 1f;
			}
		}
	}
	/// <summary>
	/// Gets the main camera.
	/// </summary>
	/// <value>The main camera.</value>
	public static Camera MainCamera
	{
		get
		{
			if (mainCamera == null)
			{
				mainCamera = Camera.main;
			}
			
			return mainCamera;
		}
	}
	/// <summary>
	/// Gets the camera settings.
	/// </summary>
	/// <value>The camera settings.</value>
	public static CameraSettings CameraSettings
	{
		get
		{
			if (cameraSettings == null)
			{
				cameraSettings = GameObject.FindObjectOfType<CameraSettings>();
			}
			
			return cameraSettings;
		}
	}
	/// <summary>
	/// Gets the camera shake.
	/// </summary>
	/// <value>The camera shake.</value>
	public static CameraShake CameraShake 
	{
		get
		{
			if (cameraShake == null)
			{
				cameraShake = GameObject.FindObjectOfType<CameraShake>();
			}
			
			return cameraShake;
		}
	}
	/// <summary>
	/// Gets the camera shader.
	/// </summary>
	/// <value>The camera shader.</value>
	public static CameraShader CameraShader 
	{
		get
		{
			if (cameraShader == null)
			{
				cameraShader = GameObject.FindObjectOfType<CameraShader>();
			}
			
			return cameraShader;
		}
	}
	/// <summary>
	/// Gets the player object.
	/// </summary>
	/// <value>The player object.</value>
	public static GameObject PlayerObject 
	{
		get
		{
			if (playerObject == null)
			{
				playerObject = GameObject.FindGameObjectWithTag(ShortcutWords.PlayerTag);
			}

			return playerObject;
		}
	}
	/// <summary>
	/// Gets the player movement.
	/// </summary>
	/// <value>The player movement.</value>
	public static PlayerMovement PlayerMovement
	{
		get
		{
			if (playerMovementScript == null)
			{
				playerMovementScript = PlayerObject.GetComponent<PlayerMovement>();
			}
			
			return playerMovementScript;
		}
	}
	/// <summary>
	/// Gets the player movement.
	/// </summary>
	/// <value>The player movement.</value>
	public static PlayerHealth PlayerHealth
	{
		get
		{
			if (playerHealthScript == null)
			{
				playerHealthScript = PlayerObject.GetComponent<PlayerHealth>();
			}
			
			return playerHealthScript;
		}
	}
	/// <summary>
	/// Gets the player weapon.
	/// </summary>
	/// <value>The player weapon.</value>
	public static PlayerWeaponControl PlayerWeapon
	{
		get
		{
			if (playerWeaponControl == null)
			{
				playerWeaponControl = PlayerObject.GetComponentInChildren<PlayerWeaponControl>();
			}
			
			return playerWeaponControl;
		}
	}
	/// <summary>
	/// Gets the items pooling.
	/// </summary>
	/// <value>The items pooling.</value>
	public static ItemsRepository ItemRepository
	{
		get
		{
			if (itemRepository == null)
			{
				itemRepository = GameObject.FindObjectOfType<ItemsRepository>();
			}
			
			return itemRepository;
		}
	}
	/// <summary>
	/// Gets the particles repository.
	/// </summary>
	/// <value>The particles repository.</value>
	public static ParticlesRepository ParticlesRepository
	{
		get
		{
			if (particlesRepository == null)
			{
				particlesRepository = GameObject.FindObjectOfType<ParticlesRepository>();
			}
			
			return particlesRepository;
		}
	}
	/// <summary>
	/// Gets the dynamic objects.
	/// </summary>
	/// <value>The dynamic objects.</value>
	public static Transform DynamicObjects
	{
		get
		{
			if (dynamicObjects == null)
			{
				dynamicObjects = GameObject.Find(ShortcutWords.DynamicObjectsName).transform;
			}
			
			return dynamicObjects;
		}
	}
	/// <summary>
	/// Gets the inventory system.
	/// </summary>
	/// <value>The inventory system.</value>
	public static InventorySystem InventorySystem
	{
		get
		{
			if (inventorySystem == null)
			{
				inventorySystem = GameObject.FindObjectOfType<InventorySystem>();
			}
			
			return inventorySystem;
		}
	}
	/// <summary>
	/// Gets the status bar system.
	/// </summary>
	/// <value>The status bar system.</value>
	public static StatusBarSystem StatusBarSystem
	{
		get
		{
			if (statusBarSystem == null)
			{
				statusBarSystem = GameObject.FindObjectOfType<StatusBarSystem>();
			}
			
			return statusBarSystem;
		}
	}
	/// <summary>
	/// Gets or sets the pause menu.
	/// </summary>
	/// <value>The pause menu.</value>
	public static PauseMenu PauseMenu 
	{
		get
		{
			if (pauseMenu == null)
			{
				pauseMenu = GameObject.FindObjectOfType<PauseMenu>();
			}
			
			return pauseMenu;
		}
	}
	/// <summary>
	/// Gets or sets the grid.
	/// </summary>
	/// <value>The grid.</value>
	public static Grid Grid 
	{
		get
		{
			if (grid == null)
			{
				grid = GameObject.FindObjectOfType<Grid>();
			}
			
			return grid;
		}
	}
	
	/// <summary>
	/// Gets the game over.
	/// </summary>
	/// <value>The game over.</value>
	public static GameOver GameOver 
	{
		get
		{
			if (gameOver == null)
			{
				gameOver = GameObject.FindObjectOfType<GameOver>();
			}
			
			return gameOver;
		}
	}

	#endregion
}