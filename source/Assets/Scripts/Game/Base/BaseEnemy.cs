using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public abstract class BaseEnemy : EnemyPathfinding
{
	#region [ FIELDS ]

	/// <summary>
	/// The enemies resource folder.
	/// </summary>
	protected const string EnemiesResourceFolder = "Enemies";
	/// <summary>
	/// The name of the follow path method.
	/// </summary>
	protected const string FollowPathMethodName = "FollowPath";
	/// <summary>
	/// The name of the attack feedback object.
	/// </summary>
	protected const string AttackFeedbackObjectName = "Attack Feedback";

	/// <summary>
	/// The debug.
	/// </summary>
	[SerializeField]
	protected bool debug = true;
	/// <summary>
	/// The wait time to change status.
	/// </summary>
	[SerializeField]
	protected float waitTimeToChangeStatus = 0.25f;
	/// <summary>
	/// The out of range distance.
	/// </summary>
	[SerializeField]
	protected float rangeDistance = 7.5f;
	/// <summary>
	/// The chase distance.
	/// </summary>
	[SerializeField]
	protected float chaseDistance = 5f;
	/// <summary>
	/// The chase speed.
	/// </summary>
	[SerializeField]
	protected float chaseSpeed = 2.5f;
	/// <summary>
	/// The damage by touching the players.
	/// </summary>
	[SerializeField]
	private int damageByTouchingThePlayer = 1;
	/// <summary>
	/// The current status.
	/// </summary>
	private EnemyStatus currentStatus;
	/// <summary>
	/// The movement.
	/// </summary>
	protected Vector2 movement;
	/// <summary>
	/// The was hit.
	/// </summary>
	protected bool wasHit;
	/// <summary>
	/// The is touching.
	/// </summary>
	protected bool isTouching;
	/// <summary>
	/// The ranged distance increaser.
	/// </summary>
	[SerializeField]
	private float rangedDistanceIncreaser = 1.5f;
	/// <summary>
	/// The ranged distance increaser time.
	/// </summary>
	[SerializeField]
	private float rangedDistanceIncreaserTime = 2.5f;
	/// <summary>
	/// The path.
	/// </summary>
	private Vector2[] path;
	/// <summary>
	/// The index of the target.
	/// </summary>
	private int targetIndex;
	/// <summary>
	/// The speed.
	/// </summary>
	protected float speed;
	/// <summary>
	/// The attack feedback.
	/// </summary>
	protected Transform attackFeedback;
	/// <summary>
	/// The time to sprint.
	/// </summary>
	[SerializeField]
	protected float timeToAttack;// = 1f;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets or sets the current status.
	/// </summary>
	/// <value>The current status.</value>
	public EnemyStatus CurrentStatus 
	{
		get 
		{
			return currentStatus;
		}
		set 
		{ 
			if (currentStatus == value)
			{
				return;
			}

			currentStatus = value;

			// Each enemy will execute your specific behavior.
			ExecuteCurrentStatus();
		}
	}
	/// <summary>
	/// Gets the pivot.
	/// </summary>
	/// <value>The pivot.</value>
	public virtual Vector2 Pivot
	{
		get 
		{
			return new Vector2(transform.position.x, transform.position.y + renderer.bounds.size.y / 2); 
		}
	}
	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="BaseEnemy"/> is active.
	/// </summary>
	/// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
	public virtual bool Active 
	{
		get 
		{ 
			return gameObject.activeInHierarchy; 
		}
		set 
		{ 
			gameObject.SetActive(value);

			if (!value)
			{
				StopAllCoroutines();
				CancelInvoke();
			}
		}
	}
	/// <summary>
	/// Gets the distance from player.
	/// </summary>
	/// <value>The distance from player.</value>
	public float DistanceFromPlayer 
	{
		get { return Vector2.Distance (Pivot, GameContext.PlayerMovement.Pivot); }
	}
	/// <summary>
	/// Gets the damage by touching the player.
	/// </summary>
	/// <value>The damage by touching the player.</value>
	public int DamageByTouchingThePlayer { get { return damageByTouchingThePlayer; } }

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Executes the status.
	/// </summary>
	protected abstract void ExecuteCurrentStatus ();

	/// <summary>
	/// Increases the ranged distance.
	/// </summary>
	/// <returns>The ranged distance.</returns>
	protected IEnumerator IncreaseRangedDistance()
	{
		rangeDistance *= rangedDistanceIncreaser;

		yield return new WaitForSeconds (rangedDistanceIncreaserTime);
		wasHit = false;

		rangeDistance /= rangedDistanceIncreaser;
	}

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		transform.SetFakeDepth ();

		collider2D.isTrigger = true;

		rigidbody2D.fixedAngle = true;
		rigidbody2D.isKinematic = true;

		attackFeedback = transform.FindChild (AttackFeedbackObjectName);
		attackFeedback.gameObject.SetActive (false);
	}

	/// <summary>
	/// Sent when an incoming collider makes contact with this object's collider (2D physics only).
	/// </summary>
	/// <param name="collision">Collision.</param>
	protected override void OnCollisionEnter2D (Collision2D collision)
	{
		if (collision.gameObject.CompareTag(ShortcutWords.PlayerTag))
		{
			isTouching = true;
		}
	}

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this object (2D physics only).
	/// </summary>
	/// <param name="collider">Collider.</param>
	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		if (collider.CompareTag(ShortcutWords.PlayerTag)) 
		{
			isTouching = true;
		}
	}

	/// <summary>
	/// Sent when a collider on another object stops touching this object's collider (2D physics only).
	/// </summary>
	/// <param name="collision">Collision.</param>
	protected override void OnCollisionExit2D (Collision2D collision)
	{
		if (collision.gameObject.CompareTag(ShortcutWords.PlayerTag))
		{
			isTouching = false;
		}
	}

	/// <summary>
	/// Sent when another object leaves a trigger collider attached to this object (2D physics only).
	/// </summary>
	/// <param name="collider">Collider.</param>
	protected override void OnTriggerExit2D (Collider2D collider)
	{
		if (collider.CompareTag(ShortcutWords.PlayerTag)) 
		{
			isTouching = false;
		}
	}

	/// <summary>
	/// Waits the time to change status.
	/// </summary>
	/// <returns>The time to change status.</returns>
	/// <param name="nextStatus">Next status.</param>
	protected IEnumerator WaitTimeToChangeStatus(EnemyStatus nextStatus)
	{
		yield return new WaitForSeconds (waitTimeToChangeStatus);

		CurrentStatus = nextStatus;
	}

	/// <summary>
	/// Randomizes the position.
	/// </summary>
	/// <returns>The position.</returns>
	protected Vector2 RandomizePosition ()
	{
		Vector2 randomize = new Vector2 ((Random.value * 2f) - 1, (Random.value * 2f) - 1);
		
		return randomize.normalized;
	}

	/// <summary>
	/// Its the was hit out of action.
	/// </summary>
	public virtual void ItWasHitOutOfAction ()
	{
		wasHit = true;

		StartCoroutine (IncreaseRangedDistance ());
	}

	/// <summary>
	/// Raises the path found event.
	/// </summary>
	/// <param name="newPath">New path.</param>
	/// <param name="pathSuccessful">If set to <c>true</c> path successful.</param>
	public void OnPathFound(Vector2[] newPath, bool pathSuccessful)
	{
		if (Active && pathSuccessful)
		{
			path = newPath;

			StopCoroutine(FollowPathMethodName);
			StartCoroutine(FollowPathMethodName);
		}
	}

	/// <summary>
	/// Follows the path.
	/// </summary>
	/// <returns>The path.</returns>
	private IEnumerator FollowPath()
	{
		if (path.Length == 0)
		{
			yield break;
		}

		Vector2 currentWaypoint = path [0];
		
		while (true)
		{
			Vector2 pos = new Vector2(Pivot.x, Pivot.y);
			if (pos == currentWaypoint)
			{
				targetIndex++;
				if (targetIndex >= path.Length)
				{
					yield break;
				}
				currentWaypoint = path[targetIndex];
			}

			transform.MoveTowardsTo(currentWaypoint, speed * Time.deltaTime);

			yield return null;
		}
	}

	/// <summary>
	/// Dos the attack feedback.
	/// </summary>
	/// <returns>The attack feedback.</returns>
	public IEnumerator DoAttackFeedback ()
	{
		attackFeedback.gameObject.SetActive (true);

		yield return new WaitForSeconds (timeToAttack);

		attackFeedback.gameObject.SetActive (false);
	}

	/// <summary>
	/// OnGUI is called for rendering and handling GUI events.
	/// </summary>
	protected override void OnGUI ()
	{
		if (!debug) return;

		GUI.Label(new Rect(20, Screen.height - 40, 120, 40), gameObject.name + ": " + CurrentStatus);
	}
	
	/// <summary>
	/// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
	/// </summary>
	protected override void OnDrawGizmos ()
	{
		if (!debug) return;

		Gizmos.color = DistanceFromPlayer <= chaseDistance ? Color.yellow : Color.white;
		Gizmos.DrawWireSphere (Pivot, chaseDistance);

		Gizmos.color = DistanceFromPlayer <= rangeDistance ? Color.green : Color.white;
		Gizmos.DrawWireSphere (Pivot, rangeDistance);

		Gizmos.color = Color.red;
		Gizmos.DrawSphere (Pivot, 0.25f);

		OnPathDrawGizmos ();
	}

	/// <summary>
	/// Raises the path draw gizmos event.
	/// </summary>
	private void OnPathDrawGizmos ()
	{
		if (path != null)
		{
			for (int i = targetIndex; i < path.Length; i++) 
			{
				Gizmos.color = Color.white;
				Gizmos.DrawCube(path[i], Vector2.one);
				
				if (i == targetIndex)
				{
					Gizmos.DrawLine(transform.position, path[i]);
				}
				else
				{
					Gizmos.DrawLine(path[i-1], path[i]);
				}
			}
		}
	}

	#endregion
}