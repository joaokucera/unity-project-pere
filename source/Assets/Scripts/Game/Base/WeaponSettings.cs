﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class WeaponSettings 
{
	public AmmoSettings AmmoSettings;
	public WeaponSettingsLoading LoadingSettings;
	public WeaponSettingsTime TimeSettings;
	public WeaponSettingsKickback KickbackSettings;
	public WeaponSettingsCooldown CooldownSettings;
}

[Serializable]
public class WeaponSettingsCooldown
{
	public float Rate;

	public float Value { get; set; }

	public void Reset()
	{
		Value = Rate;
	}
}

[Serializable]
public class WeaponSettingsLoading
{
	public bool Enabled;
	public float Current;
	public float Minimum;
	public float Maximum;

	public bool IsFull { get { return Current == Maximum; } }
}

[Serializable]
public class WeaponSettingsTime
{
	public bool Enabled;
	public float Value;
}

[Serializable]
public class WeaponSettingsKickback
{
	public bool Enabled;
	public float Force;
}