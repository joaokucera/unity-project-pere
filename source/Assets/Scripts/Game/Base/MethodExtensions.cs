﻿using UnityEngine;
using System.Collections;

public static class MethodExtensions
{
    /// <summary>
    /// Enforces the bounds.
    /// </summary>
    /// <param name="transform">Transform.</param>
    /// <param name="mainCamera">Main camera.</param>
    /// <param name="renderer">Renderer.</param>
    public static void EnforceBounds(this Transform transform, Camera mainCamera, Renderer renderer)
    {
        // Current positions.
        Vector3 currentPosition = transform.position;
        Vector2 currentCameraPosition = mainCamera.transform.position;
        Vector2 boundSize = renderer.bounds.size / 2;

        // Get X distances.
        float xDistance = mainCamera.aspect * mainCamera.orthographicSize;
        float xMax = currentCameraPosition.x + xDistance - boundSize.x;
        float xMin = currentCameraPosition.x - xDistance + boundSize.x;

        // Fix vertical bounds
        if (currentPosition.x < xMin || currentPosition.x > xMax)
        {
            currentPosition.x = Mathf.Clamp(currentPosition.x, xMin, xMax);
        }

        // Get Y distances.
        float yDistance = mainCamera.orthographicSize;
        float yMax = currentCameraPosition.y + yDistance - boundSize.y * 2;
        float yMin = currentCameraPosition.y - yDistance + boundSize.y / 2;

        // Fix vertical bounds
        if (currentPosition.y < yMin || currentPosition.y > yMax)
        {
            currentPosition.y = Mathf.Clamp(currentPosition.y, yMin, yMax);
        }

        // Set position.
        transform.position = currentPosition;
    }

	/// <summary>
	/// Sets the scale.
	/// </summary>
	/// <param name="transform">Transform.</param>
	/// <param name="newScale">New scale.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static void SetScale(this Transform transform, Vector3 newScale, float xMin = 0f, float yMin = 0f, float zMin = 0f, float xMax = 1f, float yMax = 1f, float zMax = 1f)
	{
		Vector3 scale = transform.localScale;
		scale += newScale;

		scale.x = Mathf.Clamp (scale.x, xMin, xMax);
		scale.y = Mathf.Clamp (scale.y, yMin, yMax);
		scale.z = Mathf.Clamp (scale.z, zMin, zMax);

		transform.localScale = scale;
	}

	/// <summary>
	/// Move the specified target.
	/// </summary>
	/// <param name="target">Target.</param>
	public static void MoveTowardsTo(this Transform transform, Vector2 target, float speed)
	{
		transform.position = Vector2.MoveTowards (transform.position, target, speed);

		transform.SetFakeDepth ();
	}

	/// <summary>
	/// Velocities to.
	/// </summary>
	/// <param name="rigidbody2D">Rigidbody2 d.</param>
	/// <param name="direction">Direction.</param>
	/// <param name="speed">Speed.</param>
	public static void VelocityTo(this Rigidbody2D rigidbody2D, Vector2 direction, float speed)
	{
		rigidbody2D.velocity = direction * speed;
		
		rigidbody2D.transform.SetFakeDepth ();
	}

	/// <summary>
	/// Sets the position to.
	/// </summary>
	/// <param name="transform">Transform.</param>
	/// <param name="newPosition">New position.</param>
	public static void SetPositionTo(this Transform transform, Vector2 newPosition)
	{
		transform.position = newPosition;

		transform.SetFakeDepth ();
	}

	/// <summary>
	/// Sets the local position to.
	/// </summary>
	/// <param name="transform">Transform.</param>
	/// <param name="newLocalPosition">New local position.</param>
	public static void SetLocalPositionTo(this Transform transform, Vector2 newLocalPosition)
	{
		transform.localPosition = newLocalPosition;
		
		transform.SetFakeDepth ();
	}

	/// <summary>
	/// Forces the perspective z.
	/// </summary>
	/// <param name="transform">Transform.</param>
	public static void SetFakeDepth(this Transform transform)
	{
		// TODO: Verificar!!!

		//transform.position = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.y);
	}

	/// <summary>
	/// Sets the position z.
	/// </summary>
	/// <param name="transform">Transform.</param>
	/// <param name="zPosition">Z position.</param>
	public static void SetPositionZ(this Transform transform, float zPosition)
	{
//		Vector3 newPosition = transform.position;
//		newPosition.z = zPosition;
//
//		transform.position = newPosition;
	}

	/// <summary>
	/// Sets the rotation z.
	/// </summary>
	/// <param name="transform">Transform.</param>
	/// <param name="zRotation">Z rotation.</param>
	public static void SetRotationZ(this Transform transform, float zRotation)
	{
		Quaternion newRotation = transform.rotation;
		newRotation.z = zRotation;
		
		transform.rotation = newRotation;
	}

	/// <summary>
	/// Shakes at local position.
	/// </summary>
	/// <param name="transform">Transform.</param>
	/// <param name="shakeValue">Shake value.</param>
	public static void ShakeAtLocalPosition(this Transform transform, float shakeValue = 2f)
	{
		Vector2 randomize = new Vector2 ((Random.value * shakeValue) - 1, (Random.value * shakeValue) - 1).normalized;

		transform.localPosition += new Vector3(randomize.x * Time.deltaTime, randomize.y * Time.deltaTime, 0);
	}
}