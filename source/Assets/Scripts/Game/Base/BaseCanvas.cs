using UnityEngine;
using System.Collections;

public abstract class BaseCanvas : BaseScript 
{
	#region [ FIELDS ]
	
	/// <summary>
	/// The canvas.
	/// </summary>
	private Canvas canvas;
	/// <summary>
	/// The canvas group.
	/// </summary>
	private CanvasGroup canvasGroup;
	/// <summary>
	/// The fading in.
	/// </summary>
	private bool fadingIn;
	/// <summary>
	/// The fading out.
	/// </summary>
	private bool fadingOut;
	/// <summary>
	/// The fade time.
	/// </summary>
	private float fadeTime = 0.5f;
	
	#endregion

	#region [ PROPERTIES ]
	
	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="InventorySystem"/> is active.
	/// </summary>
	/// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
	protected bool Active
	{
		get 
		{ 
			return CanvasGroup.alpha == 1;
		}
		set 
		{
			CanvasGroup.blocksRaycasts = value;

			if (value)
			{
				CanvasGroup.alpha = 1;
			}
			else
			{
				CanvasGroup.alpha = 0;
			}
		}
	}
	/// <summary>
	/// Gets the image U.
	/// </summary>
	/// <value>The image U.</value>
	private Canvas Canvas
	{
		get
		{
			if (canvas == null)
			{
				canvas = GetComponentInParent<Canvas> ();
			}
			
			return canvas;
		}
	}
	/// <summary>
	/// Gets a value indicating whether this instance canvas group.
	/// </summary>
	/// <value><c>true</c> if this instance canvas group; otherwise, <c>false</c>.</value>
	protected CanvasGroup CanvasGroup
	{
		get
		{
			if (canvasGroup == null)
			{
				canvasGroup = Canvas.GetComponent<CanvasGroup>();
			}
			
			return canvasGroup;
		}
	}
	
	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		Active = false;
	}

	/// <summary>
	/// Opens the immediately.
	/// </summary>
	public virtual void OpenImmediately()
	{
		Active = true;
	}
	
	/// <summary>
	/// Instants the close.
	/// </summary>
	public virtual void CloseImmediately()
	{
		Active = false;
	}

	/// <summary>
	/// Open this instance.
	/// </summary>
	protected virtual void Open()
	{
		GameContext.Paused = true;
		
		StartCoroutine ("FadeIn");
	}
	
	/// <summary>
	/// Close this instance.
	/// </summary>
	protected virtual void Close()
	{
		GameContext.Paused = false;
		
		StartCoroutine ("FadeOut");
	}

	/// <summary>
	/// Fades the out.
	/// </summary>
	/// <returns>The out.</returns>
	private IEnumerator FadeOut()
	{
		if (!fadingOut)
		{
			fadingOut = true;
			fadingIn = false;
			StopCoroutine("FadeIn");
			
			float startAlpha = CanvasGroup.alpha;
			float rate = 1f / fadeTime;
			float progress = 0f;
			
			while (progress < 1f) 
			{
				CanvasGroup.alpha = Mathf.Lerp(startAlpha, 0, progress);
				progress += rate * Time.unscaledDeltaTime;
				
				yield return null;
			}

			fadingOut = false;
			Active = false;
		}
	}
	
	/// <summary>
	/// Fades the in.
	/// </summary>
	/// <returns>The in.</returns>
	private IEnumerator FadeIn()
	{
		if (!fadingIn)
		{
			fadingIn = true;
			fadingOut = false;
			StopCoroutine("FadeOut");
			
			float startAlpha = CanvasGroup.alpha;
			float rate = 1f / fadeTime;
			float progress = 0f;
			
			while (progress < 1f) 
			{
				CanvasGroup.alpha = Mathf.Lerp(startAlpha, 1, progress);
				progress += rate * Time.unscaledDeltaTime;
				
				yield return null;
			}

			fadingIn = false;
			Active = true;
		}
	}

	#endregion
}
