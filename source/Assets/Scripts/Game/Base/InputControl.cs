﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public static class InputControl
{
	#region [ FIELDS ]

	/// <summary>
	/// The last melee axis.
	/// </summary>
	private static float lastMeleeAxis;

	#endregion

	#region [ PROPERTIES ]
	
	/// <summary>
	/// Gets a value indicating is pause pressed.
	/// </summary>
	/// <value><c>true</c> if is pause pressed; otherwise, <c>false</c>.</value>
	public static bool IsPausePressed
	{
		get
		{
			return Input.GetButtonDown(ShortcutWords.PauseInput);
		}
	}

	/// <summary>
	/// Gets a value indicating is hot key down pressed.
	/// </summary>
	/// <value><c>true</c> if is hot key down pressed; otherwise, <c>false</c>.</value>
	public static bool IsHotKeyDownPressed 
	{
		get
		{
			return Input.GetButtonDown(ShortcutWords.HotKey_Down_Input);
		}
	}

	/// <summary>
	/// Gets a value indicating is hot key right pressed.
	/// </summary>
	/// <value><c>true</c> if is hot key right pressed; otherwise, <c>false</c>.</value>
	public static bool IsHotKeyRightPressed 
	{
		get
		{
			return Input.GetButtonDown(ShortcutWords.HotKey_Right_Input);
		}
	}

	/// <summary>
	/// Gets a value indicating is hot key left pressed.
	/// </summary>
	/// <value><c>true</c> if is hot key left pressed; otherwise, <c>false</c>.</value>
	public static bool IsHotKeyLeftPressed 
	{
		get
		{
			return Input.GetButtonDown(ShortcutWords.HotKey_Left_Input);
		}
	}

	/// <summary>
	/// Gets a value indicating is hot key up pressed.
	/// </summary>
	/// <value><c>true</c> if is hot key up pressed; otherwise, <c>false</c>.</value>
	public static bool IsHotKeyUpPressed 
	{
		get
		{
			return Input.GetButtonDown(ShortcutWords.HotKey_Up_Input);
		}
	}

	/// <summary>
	/// Gets a value indicating is inventory pressed.
	/// </summary>
	/// <value><c>true</c> if is inventory pressed; otherwise, <c>false</c>.</value>
	public static bool IsInventoryPressed
	{
		get
		{
			return Input.GetButtonDown(ShortcutWords.InventoryInput);
		}
	}

    /// <summary>
    /// Gets a value indicating is dodge pressed.
    /// </summary>
    /// <value><c>true</c> if is dodge pressed; otherwise, <c>false</c>.</value>
    public static bool IsDodgePressed
    {
        get
        {
            return Input.GetButtonDown(ShortcutWords.DodgeInput);
        }
    }

    /// <summary>
    /// Gets a value indicating is shot pressed.
    /// </summary>
    /// <value><c>true</c> if is shot pressed; otherwise, <c>false</c>.</value>
    public static bool IsShotPressed
    {
        get
        {
			return Input.GetButtonDown(ShortcutWords.ShotInput) && !EventSystem.current.IsPointerOverGameObject ();
        }
    }

	/// <summary>
	/// Gets a value indicating is shot loading.
	/// </summary>
	/// <value><c>true</c> if is shot loading; otherwise, <c>false</c>.</value>
	public static bool IsShotLoading
	{
		get
		{
			return Input.GetButton(ShortcutWords.ShotInput) && !EventSystem.current.IsPointerOverGameObject ();
		}
	}

	/// <summary>
	/// Gets a value indicating is shot released.
	/// </summary>
	/// <value><c>true</c> if is shot released; otherwise, <c>false</c>.</value>
	public static bool IsShotReleased
	{
		get
		{
			return Input.GetButtonUp(ShortcutWords.ShotInput) && !EventSystem.current.IsPointerOverGameObject ();
		}
	}

	/// <summary>
	/// Gets a value indicating is melee pressed.
	/// </summary>
	/// <value><c>true</c> if is melee pressed; otherwise, <c>false</c>.</value>
	public static bool IsMeleePressed
	{
		get
		{
			return Input.GetButtonDown(ShortcutWords.MeleeInput) || EvaluateJoystickMeleeAxis();
		}
	}

	/// <summary>
	/// Gets a value indicating is melle loading.
	/// </summary>
	/// <value><c>true</c> if is melle loading; otherwise, <c>false</c>.</value>
	public static bool IsMeleeLoading
	{
		get
		{
			return Input.GetButton(ShortcutWords.MeleeInput) || EvaluateJoystickMeleeAxis();
		}
	}

	/// <summary>
	/// Gets a value indicating is melle released.
	/// </summary>
	/// <value><c>true</c> if is melle released; otherwise, <c>false</c>.</value>
	public static bool IsMeleeReleased
	{
		get
		{
			return Input.GetButtonUp(ShortcutWords.MeleeInput) || EvaluateJoystickMeleeAxis();
		}
	}

	#endregion

	#region [ METHODS ]

    /// <summary>
    /// Gets the movement.
    /// </summary>
    /// <returns>The movement.</returns>
    public static Vector3 GetMovement()
    {
        var movement = new Vector3
        (
            Input.GetAxis(ShortcutWords.HorizontalInput),
            Input.GetAxis(ShortcutWords.VerticalInput),
			0
        );

		return movement.normalized;
    }

	/// <summary>
	/// Gets the mouse position.
	/// </summary>
	/// <returns>The mouse position.</returns>
	/// <param name="mainCamera">Main camera.</param>
	/// <param name="position">Position.</param>
	public static Vector3 GetMousePosition(Camera mainCamera, Vector3 position)
	{
		var mousePosition = Input.mousePosition;
		mousePosition.z = position.z - mainCamera.transform.position.z;
		mousePosition = mainCamera.ScreenToWorldPoint(mousePosition);

		return mousePosition;
	}

    /// <summary>
    /// Gets the aim position.
    /// </summary>
    /// <returns>The aim position.</returns>
    /// <param name="mainCamera">Main camera.</param>
    /// <param name="position">Position.</param>
	public static Vector3 GetAimPosition(Camera mainCamera, Vector3 position)
    {
		#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
		var aimPosition = new Vector3
		(
			Input.GetAxis(ShortcutWords.AimX_JoystickWin_Input), 
			Input.GetAxis(ShortcutWords.AimY_JoystickWin_Input), 
			0
		);
		#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		var aimPosition = new Vector3
		(
			Input.GetAxis(ShortcutWords.AimX_JoystickMac), 
			Input.GetAxis(ShortcutWords.AimY_JoystickMac), 
			0
		);
		#endif

		return aimPosition.normalized;
    }

	/// <summary>
	/// News the method.
	/// </summary>
	/// <returns><c>true</c>, if method was newed, <c>false</c> otherwise.</returns>
	private static bool EvaluateJoystickMeleeAxis ()
	{
		float newMeleeAxis = Input.GetAxisRaw (ShortcutWords.MeleeInput);

		bool different = lastMeleeAxis != newMeleeAxis;

		lastMeleeAxis = newMeleeAxis;

		return different;
	}

	#endregion
}