﻿/// <summary>
/// Facing side.
/// </summary>
public enum FacingSide
{
	Up = 2,
	Down = 0,
	Right = 1,
	Left = -1
}

/// <summary>
/// Input type.
/// </summary>
public enum InputType
{
	Keyboard,
	Joystick
}

/// <summary>
/// Owner attack tag.
/// </summary>
public enum OwnerAttackTag
{
	PlayerAttack,
	EnemyAttack
}

/// <summary>
/// Item type.
/// </summary>
public enum ItemType
{
	AcaiBerry,
	Cassava,
	Coffee,
	Guarana,
	Gold,
	GoldMother,
	Muiraquita,
	Weapon_Sling,
	Weapon_KoiereAxe,
	Weapon_Boleadeira,
	Weapon_Blowgun
}

/// <summary>
/// Inventory tab.
/// </summary>
public enum InventoryTab
{
	All = 0,
	Weapons = 1,
	Items = 2
}

/// <summary>
/// Weapon side.
/// </summary>
public enum HotkeyType
{
	ItemDown = 0,
	ItemRight = 1,
	ItemLeft = 2,
	ItemUp = 3,
	WeaponMain,
	WeaponSecondary
}

/// <summary>
/// Particle type.
/// </summary>
public enum ParticleType
{
	None,
	GreySplash,
	ExplosionEffect,
	ToonSplat,
	FlashLight,
	Shockwave
}

/// <summary>
/// Enemy status.
/// </summary>
public enum EnemyStatus
{
	None, // do not have behavior.
	Idle, // do not move.
	Patrol, // move back and forth, maybe around waypoints.
	Chase, // like patrol, but will come after player if he is closer.
	Shoot, // fires a projectil ans tries to keep distance from player.
	Guard, // the priority is staying ins an area to stop player.
	ReturnToGuard, // returning to guard position.
	Burrower, // an enemy that has invulnerable state which allows it to change position around the play field.
	Blocker, // an enemy that can block one or more player's attack, but could be 'disarmed'.
	Doppleganger, // an enemy who reproduce what player will do.
	Melee, // when enemy is attacking player with touch.
	Sprint // when enemy is attack running against player.
}