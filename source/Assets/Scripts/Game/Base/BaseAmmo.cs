﻿using UnityEngine;
using System.Collections;
using System;

public abstract class BaseAmmo : BaseScript
{
    #region [ FIELDS ]

	/// <summary>
	/// The ammo settings.
	/// </summary>
	protected AmmoSettings ammoSettings;
	/// <summary>
	/// The shadow.
	/// </summary>
	[SerializeField]
	private Transform shadow;

    #endregion

    #region [ PROPERTIES ]

	/// <summary>
	/// Gets the pivot.
	/// </summary>
	/// <value>The pivot.</value>
	public Vector2 Pivot 
	{
		get 
		{ 
			return new Vector2 (transform.position.x, transform.position.y + renderer.bounds.size.y / 2);
		}
	}
    /// <summary>
    /// Get the shot damage.
    /// </summary>
	public AmmoSettings AmmoSettings
    {
		get { return ammoSettings; }
		set { ammoSettings = value; }
    }
    /// <summary>
    /// Set if game object is active.
    /// </summary>
    public virtual bool Active
    {
        get 
		{
			return gameObject.activeInHierarchy; 
		}
        set 
		{ 
			gameObject.SetActive(value); 
		}
    }

    #endregion

    #region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		collider2D.isTrigger = true;
	}

    /// <summary>
    /// Update game logic.
    /// </summary>
    protected override void Update ()
    {
		transform.SetFakeDepth ();

        // If shot is not visible (outside of camera view).
        if (!renderer.isVisible)
        {
            // Return to pool.  
            Active = false;
        }
    }
	
	/// <summary>
	/// When trigger is enabled detect other collider against this one.
	/// </summary>
	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		if (collider.CompareTag(ShortcutWords.ObstacleTag))
		{
			GameContext.ParticlesRepository.SpawnParticle (ParticleType.GreySplash, Pivot, transform.rotation);

			Active = false;
		}
	}

	/// <summary>
	/// Dos the shoot.
	/// </summary>
	/// <param name="settings">Settings.</param>
	public virtual void Execute(WeaponSettings settings)
	{
		StartCoroutine (OnMove (settings));
	}

	/// <summary>
	/// Raises the move event.
	/// </summary>
	/// <param name="settings">Settings.</param>
	protected abstract IEnumerator OnMove (WeaponSettings settings);

	/// <summary>
	/// Dos the active.
	/// </summary>
	/// <param name="active">If set to <c>true</c> active.</param>
	/// <remarks>
	/// OBS: This method is called as a message function.
	/// </remarks>
	public virtual void DoActive(bool active)
	{
		Active = active;
	}

    #endregion
}