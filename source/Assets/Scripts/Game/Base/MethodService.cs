﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MethodService 
{
	/// <summary>
	/// Initializes the boolean probability pool.
	/// </summary>
	/// <returns>The boolean probability pool.</returns>
	/// <param name="probabilityPercentage">Probability percentage.</param>
	public static List<bool> CreateBooleanProbabilityPool (int probabilityPercentage)
	{
		List<bool> booleanProbabilityPool = new List<bool> ();
		
		int burrower = probabilityPercentage;
		int nothing = 100 - burrower;
		
		for (int i = 0; i < burrower; i++)
		{
			booleanProbabilityPool.Add(true);
		}
		for (int i = 0; i < nothing; i++) 
		{
			booleanProbabilityPool.Add(false);
		}
		
		return booleanProbabilityPool;
	}
}