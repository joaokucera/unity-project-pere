﻿using UnityEngine;
using System.Collections;

public static class ShortcutWords
{
	#region NAMES

	/// <summary>
	/// The name of the dynamic objects.
	/// </summary>
	public const string DynamicObjectsName = "Dynamic Objects";

	#endregion

    #region TAGS

	/// <summary>
	/// The untagged.
	/// </summary>
	public const string Untagged = "Untagged";
    /// <summary>
    /// Player tag.
    /// </summary>
    public const string PlayerTag = "Player";
    /// <summary>
    /// Enemy shot tag.
    /// </summary>
    public const string PlayerAttackTag = "PlayerAttack";
    /// <summary>
    /// Enemy tag.
    /// </summary>
    public const string EnemyTag = "Enemy";
    /// <summary>
    /// Enemy shot tag.
    /// </summary>
    public const string EnemyAttackTag = "EnemyAttack";
    /// <summary>
    /// Item tag.
    /// </summary>
    public const string ItemTag = "Item";
    /// <summary>
    /// Gold tag.
    /// </summary>
	public const string GoldTag = "Gold";
	/// <summary>
	/// The obstacle tag.
	/// </summary>
	public const string ObstacleTag = "Obstacle";
	/// <summary>
	/// The destructible tag.
	/// </summary>
	public const string DestructibleTag = "Destructible";
	/// <summary>
	/// The checkpoint tag.
	/// </summary>
	public const string CheckpointTag = "Checkpoint";
	/// <summary>
	/// The hole tag.
	/// </summary>
	public const string HoleTag = "Hole";
	/// <summary>
	/// The un hole tag.
	/// </summary>
	public const string UnHoleTag = "UnHole";

    #endregion

    #region INPUTS

    /// <summary>
    /// The horizontal movement input.
    /// </summary>
    public const string HorizontalInput = "Horizontal";
    /// <summary>
    /// The vertical movement input.
    /// </summary>
    public const string VerticalInput = "Vertical";
	/// <summary>
	/// The inventory input.
	/// </summary>
	public const string InventoryInput = "Inventory";
    /// <summary>
    /// The dodge input.
    /// </summary>
    public const string DodgeInput = "Dodge";
    /// <summary>
    /// The shot input.
    /// </summary>
    public const string ShotInput = "Shot";
	/// <summary>
	/// The melee input.
	/// </summary>
	public const string MeleeInput = "Melee";
	/// <summary>
	/// The aim x_ keyboard_ input.
	/// </summary>
	public const string AimX_Keyboard_Input = "Aim X (Keyboard)";
	/// <summary>
	/// The aim y_ keyboard_ input.
	/// </summary>
	public const string AimY_Keyboard_Input = "Aim Y (Keyboard)";
	/// <summary>
	/// The aim x_ joystick mac_ input.
	/// </summary>
	public const string AimX_JoystickMac_Input = "Aim X (Joystick Mac)";
	/// <summary>
	/// The aim y_ joystick mac_ input.
	/// </summary>
	public const string AimY_JoystickMac_Input = "Aim Y (Joystick Mac)";
	/// <summary>
	/// The aim x_ joystick win_ input.
	/// </summary>
	public const string AimX_JoystickWin_Input = "Aim X (Joystick Win)";
	/// <summary>
	/// The aim y_ joystick win_ input.
	/// </summary>
	public const string AimY_JoystickWin_Input = "Aim Y (Joystick Win)";
	/// <summary>
	/// The hot key_ down_ input.
	/// </summary>
	public const string HotKey_Down_Input = "Hot Key (Down)";
	/// <summary>
	/// The hot key_ right_ input.
	/// </summary>
	public const string HotKey_Right_Input = "Hot Key (Right)";
	/// <summary>
	/// The hot key_ left_ input.
	/// </summary>
	public const string HotKey_Left_Input = "Hot Key (Left)";
	/// <summary>
	/// The hot key_ up_ input.
	/// </summary>
	public const string HotKey_Up_Input = "Hot Key (Up)";
	/// <summary>
	/// The pause input.
	/// </summary>
	public static string PauseInput = "Pause";

    #endregion

    #region LAYERS

    /// <summary>
    /// Player layer mask.
    /// </summary>
    public const string PlayerLayerMask = "Player";
	/// <summary>
	/// The obstacle layer mask.
	/// </summary>
	public const string ObstacleLayerMask = "Obstacle";

    #endregion
}