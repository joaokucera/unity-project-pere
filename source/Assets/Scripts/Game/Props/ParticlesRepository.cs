﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticlesRepository : BaseScript 
{
	#region [ FIELDS ]
	
	/// <summary>
	/// The path.
	/// </summary>
	private const string ResourceFolder = "Particles";
	/// <summary>
	/// The poolings.
	/// </summary>
	private Dictionary<ParticleType, ParticlesPooling> pools = new Dictionary<ParticleType, ParticlesPooling> ();
	
	#endregion
	
	#region [ METHODS ]
	
	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		CreatePools ();
	}
	
	/// <summary>
	/// Creates the pools.
	/// </summary>
	private void CreatePools ()
	{
		GameObject[] particles = Resources.LoadAll<GameObject> (ResourceFolder);

		for (int i = 0; i < particles.Length; i++)
		{
			var particle = particles[i];

			string name = particle.name.Replace (" Particle", string.Empty);
			
			var newPooling = new GameObject (string.Format("{0} Pooling", name), typeof(ParticlesPooling));
			newPooling.transform.SetParent (transform);
			
			var pooling = newPooling.GetComponent<ParticlesPooling> ();
			pooling.Prefab = particle.gameObject;
			pooling.PoolSize = 5;
			pooling.PoolCanGrow = true;
			
			ParticleType type = (ParticleType)System.Enum.Parse(typeof(ParticleType), name);
			pools.Add(type, pooling);
		}
	}
	
	/// <summary>
	/// Spawns the item.
	/// </summary>
	/// <param name="type">Type.</param>
	/// <param name="position">Position.</param>
	/// <param name="rotation">Rotation.</param>
	public void SpawnParticle(ParticleType type, Vector3 position, Quaternion rotation)
	{
		ParticlesPooling pooling;
		if (pools.TryGetValue(type, out pooling))
		{
			pooling.GetObjectFromPool(position, rotation);
		}
	}
	
	#endregion
}