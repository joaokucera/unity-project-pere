﻿using UnityEngine;
using System.Collections;

public class ParticleEffect : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The name of the hide method.
	/// </summary>
	private const string HideMethodName = "Hide";

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// OnBecameVisible is called when the renderer became visible by any camera.
	/// </summary>
	protected override void OnBecameVisible ()
	{
		if (particleSystem)
		{
			Invoke(HideMethodName, particleSystem.duration);
		}
	}

	/// <summary>
	/// Hide this instance.
	/// </summary>
	public void Hide()
	{
		gameObject.SetActive (false);
	}

	#endregion
}