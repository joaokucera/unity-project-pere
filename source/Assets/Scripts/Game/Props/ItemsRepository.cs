﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ItemsRepository : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The path.
	/// </summary>
	private const string ItemsResourceFolder = "Items";
	/// <summary>
	/// The poolings.
	/// </summary>
	private Dictionary<ItemType, ItemPooling> pools = new Dictionary<ItemType, ItemPooling> ();

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		CreatePools ();
	}

	/// <summary>
	/// Creates the pools.
	/// </summary>
	private void CreatePools ()
	{
		BaseItem[] items = Resources.LoadAll<BaseItem> (ItemsResourceFolder);

		for (int i = 0; i < items.Length; i++)
		{
			var item = items[i];

			string name = item.name.Replace (" Item", string.Empty);

			var newPooling = new GameObject (string.Format("{0} Pooling", name), typeof(ItemPooling));
			newPooling.transform.SetParent (transform);

			var pooling = newPooling.GetComponent<ItemPooling> ();
			pooling.Prefab = item.gameObject;
			pooling.PoolSize = 5;
			pooling.PoolCanGrow = true;

			ItemType type = (ItemType)System.Enum.Parse(typeof(ItemType), name);
			pools.Add(type, pooling);
		}
	}

	/// <summary>
	/// Spawns the item.
	/// </summary>
	/// <param name="type">Type.</param>
	/// <param name="position">Position.</param>
	/// <param name="rotation">Rotation.</param>
	public void SpawnItem(ItemType type, Vector3 position, Quaternion rotation)
	{
		ItemPooling itemPooling;
		if (pools.TryGetValue(type, out itemPooling))
		{
			itemPooling.GetObjectFromPool(position, rotation);
		}
	}

	#endregion
}