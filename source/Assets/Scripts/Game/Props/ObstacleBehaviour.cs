﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(BoxCollider2D))]
public class ObstacleBehaviour : BaseScript
{
	#region [ FIELDS ]

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets the pivot.
	/// </summary>
	/// <value>The pivot.</value>
	public Vector2 Pivot
	{
		get 
		{ 
			return new Vector2(transform.position.x, transform.position.y + renderer.bounds.size.y / 2); 
		}
	}
	
	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
    protected override void Start ()
	{
//		BoxCollider2D boxCollider = ((BoxCollider2D)collider2D);
//		boxCollider.size = new Vector2 (boxCollider.size.x, boxCollider.size.y / 2);
//		boxCollider.center = new Vector2 (boxCollider.center.x, boxCollider.size.y);

		transform.SetFakeDepth ();
    }

	#endregion
}