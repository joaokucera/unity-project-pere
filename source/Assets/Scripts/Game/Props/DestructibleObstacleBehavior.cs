using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer), typeof(BoxCollider2D))]
public class DestructibleObstacleBehavior : BaseScript
{
	#region [ FIELDS ]

	/// <summary>
	/// The name of the do active method.
	/// </summary>
	private const string DoActiveMethodName = "DoActive";

	/// <summary>
	/// The health steps sprites.
	/// </summary>
	[SerializeField]
	private Sprite[] healthStepsSprites;
	/// <summary>
	/// The spawn item probability.
	/// </summary>
	[SerializeField]
	private int spawnItemProbability;// = 50;
	/// <summary>
	/// The boolean probability pool.
	/// </summary>
	private List<bool> booleanProbabilityPool = new List<bool>();
	/// <summary>
	/// The spawn items.
	/// </summary>
	[SerializeField]
	private List<ItemType> spawnItems = new List<ItemType>();
	/// <summary>
	/// The index.
	/// </summary>
	private int destroyedSpritesIndex = -1;
	/// <summary>
	/// The sprite renderer.
	/// </summary>
	private SpriteRenderer spriteRenderer;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets a value indicating whether this <see cref="DestructibleBehavior"/> draws boolean probability.
	/// </summary>
	/// <value><c>true</c> if draws boolean probability; otherwise, <c>false</c>.</value>
	protected bool DrawsBooleanProbability
	{
		get 
		{
			int randomProbability =  Random.Range (0, booleanProbabilityPool.Count);
			
			return booleanProbabilityPool[randomProbability];
		}
	}
	/// <summary>
	/// Gets the draws item.
	/// </summary>
	/// <value>The draws item.</value>
	public ItemType DrawsItem
	{
		get 
		{
			int randomProbability =  Random.Range (0, spawnItems.Count);
			
			return spawnItems[randomProbability];
		}
	}
	/// <summary>
	/// Gets the pivot.
	/// </summary>
	/// <value>The pivot.</value>
	public Vector2 Pivot
	{
		get 
		{ 
			return new Vector2(transform.position.x, transform.position.y + renderer.bounds.size.y / 2); 
		}
	}

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		if (healthStepsSprites.Length <= 0)
		{
			Debug.LogError("Has not been defined a list of destroyed sprites!");
		}

		spriteRenderer = (SpriteRenderer)renderer;

//		BoxCollider2D boxCollider = ((BoxCollider2D)collider2D);
//		boxCollider.size = new Vector2 (boxCollider.size.x, boxCollider.size.y / 2);
//		boxCollider.center = new Vector2 (boxCollider.center.x, boxCollider.size.y);

		transform.SetFakeDepth ();

		InitializeBooleanProbabilityPool (spawnItemProbability);
	}

	/// <summary>
	/// When trigger is enabled detect other collider against this one.
	/// </summary>
	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		if (collider.CompareTag(ShortcutWords.PlayerAttackTag))
		{
			ChangeHealthStepSprite ();

			collider.SendMessage(DoActiveMethodName, false);
		}
	}

	/// <summary>
	/// Initializes the boolean probability pool.
	/// </summary>
	/// <param name="probabilityPercentage">Probability percentage.</param>
	protected void InitializeBooleanProbabilityPool (int probabilityPercentage)
	{
		int spawn = probabilityPercentage;
		int nothing = 100 - spawn;
		
		for (int i = 0; i < spawn; i++) 
		{
			booleanProbabilityPool.Add(true);
		}
		for (int i = 0; i < nothing; i++) 
		{
			booleanProbabilityPool.Add(false);
		}
	}

	/// <summary>
	/// Changes the destroyed sprite.
	/// </summary>
	private void ChangeHealthStepSprite()
	{
		destroyedSpritesIndex++;
		destroyedSpritesIndex = Mathf.Clamp (destroyedSpritesIndex, 0, healthStepsSprites.Length - 1);

		spriteRenderer.sprite = healthStepsSprites[destroyedSpritesIndex];

		if (destroyedSpritesIndex == healthStepsSprites.Length - 1)
		{
			transform.position += Vector3.forward;
			collider2D.enabled = false;
			
			if (DrawsBooleanProbability)
			{
				GameContext.ItemRepository.SpawnItem (DrawsItem, Pivot, Quaternion.identity);
			}
		}
	}

	#endregion
}