using UnityEngine;
using System.Collections;
using System;

public class CameraSmoothFollow : BaseScript 
{
	#region [ FIELDS ]
	
	/// <summary>
	/// The interpolation.
	/// </summary>
	[SerializeField]
	private float interpolation = 7.5f;
	/// <summary>
	/// The lerp speed.
	/// </summary>
	[SerializeField]
	private float lerpSpeed = 0.75f;
	/// <summary>
	/// The offset.
	/// </summary>
	[SerializeField]
	private Vector3 offset = new Vector3(0f, 0.15f, 0f);

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
	/// </summary>
	protected override void FixedUpdate ()
	{
		if (GameContext.PlayerMovement.EnforceBounds) return;

		Transform target = GameContext.PlayerObject.transform;
		
		Vector3 position = transform.position;
		position.z = target.transform.position.z;
		
		Vector3 targetDirection = target.transform.position - position;
		float interpVelocity = targetDirection.magnitude * interpolation;
		Vector3 targetPosition = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime); 
		
		transform.position = Vector3.Lerp(transform.position, targetPosition + offset, lerpSpeed);
	}

	#endregion
}