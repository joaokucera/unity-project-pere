﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HotkeyStatusBar : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The item image U.
	/// </summary>
	[SerializeField]
	private Image itemImageUI;
	/// <summary>
	/// The item texts U.
	/// </summary>
	private Text[] itemTextsUI;

	#endregion
	
	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		itemTextsUI = GetComponentsInChildren<Text>();
	}

	/// <summary>
	/// Changes the item.
	/// </summary>
	/// <param name="newSprite">New sprite.</param>
	/// <param name="newText">New text.</param>
	/// <param name="enable">If set to <c>true</c> enable.</param>
	public void ChangeItem (Sprite newSprite, string newText, bool enable)
	{
		itemImageUI.sprite = newSprite;
		itemImageUI.enabled = enable;

		for (int i = 0; i < itemTextsUI.Length; i++)
		{
			itemTextsUI[i].text = newText;
		}
	}

	/// <summary>
	/// Changes the item text.
	/// </summary>
	/// <param name="newText">New text.</param>
	public void ChangeItemText (string newText)
	{
		for (int i = 0; i < itemTextsUI.Length; i++)
		{
			itemTextsUI[i].text = newText;
		}
	}

	#endregion
}