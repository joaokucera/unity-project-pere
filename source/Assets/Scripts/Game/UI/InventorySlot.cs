﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class InventorySlot : BaseScript, IPointerClickHandler
{
	#region [ FIELDS ]

	/// <summary>
	/// The equiped label.
	/// </summary>
	private const string EquipedLabel = "E";
	
	/// <summary>
	/// The items.
	/// </summary>
	private Stack<BaseItem> items = new Stack<BaseItem>();
	/// <summary>
	/// The slot item texts U.
	/// </summary>
	private Text[] slotItemTextsUI;
	/// <summary>
	/// The image U.
	/// </summary>
	private Image imageUI;
	/// <summary>
	/// The slot item image U.
	/// </summary>
	[SerializeField]
	private Image slotItemImageUI;
	/// <summary>
	/// The button U.
	/// </summary>
	private Button buttonUI;
	/// <summary>
	/// The rect transform.
	/// </summary>
	private RectTransform rectTransform;
	/// <summary>
	/// The is equiped.
	/// </summary>
	private bool isEquiped;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets or sets the items.
	/// </summary>
	/// <value>The items.</value>
	public Stack<BaseItem> Items { get { return items; } }
	/// <summary>
	/// Gets a value indicating whether this instance is empty.
	/// </summary>
	/// <value><c>true</c> if this instance is empty; otherwise, <c>false</c>.</value>
	public bool IsEmpty { get { return items.Count == 0; } }
	/// <summary>
	/// Gets the current item.
	/// </summary>
	/// <value>The current item.</value>
	public BaseItem CurrentItem { get { return items.Peek(); } }
	/// <summary>
	/// Gets the slot item text.
	/// </summary>
	/// <value>The slot item text.</value>
	public string SlotItemText { get { return items.Count > 1 ? string.Format("{0}", items.Count) : string.Empty; } }
	/// <summary>
	/// Gets the image U.
	/// </summary>
	/// <value>The image U.</value>
	public Image ImageUI 
	{ 
		get 
		{ 
			if (imageUI == null)
			{
				imageUI = GetComponent<Image> ();
			}
			
			return imageUI; 
		} 
	}
	/// <summary>
	/// Gets the button U.
	/// </summary>
	/// <value>The button U.</value>
	public Button ButtonUI 
	{ 
		get 
		{ 
			if (buttonUI == null)
			{
				buttonUI = GetComponent<Button> ();
			}
			
			return buttonUI; 
		} 
	}
	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="InventorySlot"/> is active.
	/// </summary>
	/// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
	public bool Active
	{
		get 
		{ 
			return ImageUI.enabled; 
		}
		set 
		{
			ImageUI.enabled = value;
		}
	}
	/// <summary>
	/// Gets the rect transform.
	/// </summary>
	/// <value>The rect transform.</value>
	public RectTransform RectTransform 
	{ 
		get 
		{ 
			if (rectTransform == null)
			{
				rectTransform = GetComponent<RectTransform> ();
			}
			
			return rectTransform; 
		} 
	}
	/// <summary>
	/// Sets a value indicating whether this <see cref="InventorySlot"/> is equiped.
	/// </summary>
	/// <value><c>true</c> if equiped; otherwise, <c>false</c>.</value>
	public bool Equiped 
	{
		get
		{
			return isEquiped;
		}
		set
		{
			isEquiped = value;
			// the oppositive... (if equiped, not interactable).
			ButtonUI.interactable = !value;

			UpdateTextUI();
		}
	}

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		slotItemTextsUI = GetComponentsInChildren<Text>();
	}

	/// <summary>
	/// Adds the item.
	/// </summary>
	/// <param name="item">Item.</param>
	public void AddItem(BaseItem newItem)
	{
		items.Push (newItem);

		UpdateTextUI();

		ChangeSlotItem (newItem.ItemSprite, true);
	}

	/// <summary>
	/// Adds the items.
	/// </summary>
	/// <param name="items">Items.</param>
	public void AddItems(Stack<BaseItem> newItems)
	{
		items = new Stack<BaseItem>(newItems);

		UpdateTextUI();

		ChangeSlotItem (CurrentItem.ItemSprite, true);
	}

	/// <summary>
	/// Changes the slot item.
	/// </summary>
	/// <param name="slotItem">Slot item.</param>
	/// <param name="enable">If set to <c>true</c> enable.</param>
	private void ChangeSlotItem(Sprite slotItem, bool enable)
	{
		slotItemImageUI.sprite = slotItem;

		slotItemImageUI.enabled = enable;
	}

	/// <summary>
	/// Raises the use item event.
	/// </summary>
	public bool OnUseItem()
	{
		if (!IsEmpty)
		{
			BaseItem pop = items.Pop();

			UpdateTextUI();

			if (IsEmpty)
			{
				ChangeSlotItem (null, false);
			}

			pop.OnUse();

			return true;
		}

		return false;
	}

	/// <summary>
	/// Updates the text U.
	/// </summary>
	private void UpdateTextUI()
	{
		if (!IsEmpty && CurrentItem.IsWeapon && Equiped)
		{
			for (int i = 0; i < slotItemTextsUI.Length; i++) 
			{
				slotItemTextsUI[i].text = EquipedLabel;
			}
		}
		else
		{
			for (int i = 0; i < slotItemTextsUI.Length; i++)
			{
				slotItemTextsUI[i].text = SlotItemText;
			}
		}
	}

	/// <summary>
	/// Clears the slot.
	/// </summary>
	public void ClearSlot()
	{
		items.Clear ();

		ChangeSlotItem (null, false);

		for (int i = 0; i < slotItemTextsUI.Length; i++) 
		{
			slotItemTextsUI[i].text = string.Empty;
		}
	}

	#region IPointerClickHandler implementation

	/// <summary>
	/// Raises the pointer click event.
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnPointerClick (PointerEventData eventData)
	{
//		if (eventData.button == PointerEventData.InputButton.Right && GlobalVariables.InventorySystem.Active)
//		{
//			GlobalVariables.InventorySystem.ShowTootip(this);
//		}
	}

	#endregion

	#endregion
}