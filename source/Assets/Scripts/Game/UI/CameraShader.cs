using UnityEngine;
using System.Collections;
using System;

public class CameraShader : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The default shader.
	/// </summary>
	[SerializeField]
	private Shader defaultShader;
	/// <summary>
	/// The black white shader.
	/// </summary>
	[SerializeField]
	private Shader blackWhiteShader;
	/// <summary>
	/// The color of the original.
	/// </summary>
	private Color originalColor;

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		originalColor = GameContext.MainCamera.backgroundColor;
	}

	/// <summary>
	/// Sets the default.
	/// </summary>
	public void SetDefault ()
	{
		GameContext.MainCamera.backgroundColor = originalColor;

		GameContext.PlayerObject.renderer.material.shader = defaultShader;
	}

	/// <summary>
	/// Sets the black white.
	/// </summary>
	public void SetBlackWhite ()
	{
		GameContext.MainCamera.backgroundColor = Color.gray;

		GameContext.PlayerObject.renderer.material.shader = blackWhiteShader;
	}

	#endregion
}