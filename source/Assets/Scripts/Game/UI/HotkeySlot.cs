﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class HotkeySlot : BaseScript
{
	#region [ FIELDS ]

	/// <summary>
	/// The type.
	/// </summary>
	[SerializeField]
	private HotkeyType hotkeyType;
	/// <summary>
	/// The image U.
	/// </summary>
	private Image imageUI;
	/// <summary>
	/// The slot item image U.
	/// </summary>
	[SerializeField]
	private Image slotItemImageUI;
	/// <summary>
	/// The current slot.
	/// </summary>
	private InventorySlot currentSlot;
	/// <summary>
	/// The button U.
	/// </summary>
	private Button buttonUI;
	/// <summary>
	/// The hotkey status bar.
	/// </summary>
	[SerializeField]
	private HotkeyStatusBar hotkeyStatusBar;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets the image U.
	/// </summary>
	/// <value>The image U.</value>
	public Image ImageUI 
	{ 
		get 
		{ 
			if (imageUI == null)
			{
				imageUI = GetComponent<Image> ();
			}
			
			return imageUI; 
		} 
	}
	/// <summary>
	/// Gets the button U.
	/// </summary>
	/// <value>The button U.</value>
	public Button ButtonUI
	{ 
		get 
		{ 
			if (buttonUI == null)
			{
				buttonUI = GetComponent<Button> ();
			}
			
			return buttonUI; 
		}
	}
	/// <summary>
	/// Gets the current slot.
	/// </summary>
	/// <value>The current slot.</value>
	public InventorySlot CurrentSlot { get { return currentSlot; } }

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Sets the hot key.
	/// </summary>
	/// <param name="clicked">Clicked.</param>
	public void SetHotKey()
	{
		if (GameContext.InventorySystem.FromSlot != null && !GameContext.InventorySystem.FromSlot.IsEmpty)
		{
			SetItem(GameContext.InventorySystem.FromSlot);
			
			ResetClickedItem();
		}
		else if (currentSlot != null)
		{
			GameContext.InventorySystem.ShowDialogOverHotKey(this);
		}
	}

	/// <summary>
	/// Sets the item.
	/// </summary>
	/// <param name="item">Item.</param>
	public void SetItem(InventorySlot slot)
	{
		if (currentSlot != null)
		{
			currentSlot.Equiped = false;
		}

		currentSlot = slot;
		currentSlot.Equiped = true;

		if (currentSlot.CurrentItem.IsWeapon)
		{
			GameContext.PlayerWeapon.SwitchWeapon(currentSlot.CurrentItem.ItemType, hotkeyType);
		}

		ChangeSlotItem (currentSlot.CurrentItem.ItemSprite, currentSlot.SlotItemText, true);
	}

	/// <summary>
	/// Tries to use.
	/// </summary>
	public bool TryToUse()
	{
		if (currentSlot != null)
		{
			if (currentSlot.OnUseItem())
			{
				hotkeyStatusBar.ChangeItemText(currentSlot.SlotItemText);
			}

			if (currentSlot.IsEmpty)
			{
				ClearSlot();

				return true;
			}
		}

		return false;
	}

	/// <summary>
	/// Clears the slot.
	/// </summary>
	public void ClearSlot()
	{
		GameContext.PlayerWeapon.RemoveWeapon(hotkeyType);

		currentSlot.Equiped = false;
		currentSlot = null;
		
		ChangeSlotItem (null, string.Empty, false);
	}

	/// <summary>
	/// Changes the slot item.
	/// </summary>
	/// <param name="slotItem">Slot item.</param>
	/// <param name="slotText">Slot text.</param>
	/// <param name="enable">If set to <c>true</c> enable.</param>
	private void ChangeSlotItem(Sprite slotItem, string slotText, bool enable)
	{
		slotItemImageUI.sprite = slotItem;
		slotItemImageUI.enabled = enable;

		hotkeyStatusBar.ChangeItem (slotItem, slotText, enable);
	}

	/// <summary>
	/// Resets the clicked item.
	/// </summary>
	private void ResetClickedItem ()
	{
		GameContext.InventorySystem.ResetMoveItem ();

		GameContext.InventorySystem.HideDialogBox ();
	}

	#endregion
}