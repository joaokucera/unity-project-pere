﻿using UnityEngine;
using System.Collections;

public class CameraSettings : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The ortographic size on exploration.
	/// </summary>
	private const float OrtographicSizeOnExploration = 8.1f;
	/// <summary>
	/// The ortographic size on boss fighting.
	/// </summary>
	private const float OrtographicSizeOnBossFighting = OrtographicSizeOnExploration * 1.5f;
	/// <summary>
	/// The ortographic size on loading weapon.
	/// </summary>
	private const float OrtographicSizeOnLoadingWeapon = OrtographicSizeOnExploration * 3f / 4f;

	/// <summary>
	/// The cameras.
	/// </summary>
	private Camera[] cameras;
	/// <summary>
	/// The last size of the ortographic.
	/// </summary>
	private float lastOrtographicSize;
	/// <summary>
	/// The timer.
	/// </summary>
	private float timer = 0;

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	protected override void Awake ()
	{
		cameras = GetComponentsInChildren<Camera> ();

		for (int i = 0; i < cameras.Length; i++) 
		{
			cameras[i].depth = i;
			cameras[i].orthographicSize = OrtographicSizeOnExploration;
		}

		lastOrtographicSize = OrtographicSizeOnExploration;
	}

	/// <summary>
	/// Zooms the in.
	/// </summary>
	/// <param name="loadingTime">Loading time.</param>
	public void ZoomIn(float loadingTime)
	{
		timer += Time.deltaTime;

		if (timer < loadingTime || cameras[0].orthographicSize <= OrtographicSizeOnLoadingWeapon) return;

		for (int i = 0; i < cameras.Length; i++)
		{
			cameras[i].orthographicSize -= Time.deltaTime;
		}

		GameContext.CameraShake.Shake (0.0025f);
	}

	/// <summary>
	/// Returns the size of the to original.
	/// </summary>
	public void ZoomOut()
	{
		timer = 0f;

		StartCoroutine (ToZoomOut ());
	}

	/// <summary>
	/// Returnings the size of the to original.
	/// </summary>
	/// <returns>The to original size.</returns>
	private IEnumerator ToZoomOut()
	{
		var currentSize = cameras [0].orthographicSize;

		while (currentSize < lastOrtographicSize)
		{
			for (int i = 0; i < cameras.Length; i++)
			{
				cameras[i].orthographicSize += Time.deltaTime;
			}

			currentSize = cameras [0].orthographicSize;

			yield return null;
		}
	}

	#endregion
}