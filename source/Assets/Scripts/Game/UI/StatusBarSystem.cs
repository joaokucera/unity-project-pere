using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class StatusBarSystem : BaseCanvas 
{
	#region [ FIELDS ]

	/// <summary>
	/// The health bar image.
	/// </summary>
	[SerializeField]
	private Image healthBarImage;
	/// <summary>
	/// The extra health bar image.
	/// </summary>
	[SerializeField]
	private Image extraHealthBarImage;
	/// <summary>
	/// The health bar speed.
	/// </summary>
	[SerializeField]
	private float healthBarSpeed;
	/// <summary>
	/// The mana bar image.
	/// </summary>
	[SerializeField]
	private Image manaBarImage;
	/// <summary>
	/// The max mana bar fill amout.
	/// </summary>
	private float maxManaBarFillAmout;
	/// <summary>
	/// The gold text U.
	/// </summary>
	[SerializeField]
	private Text goldTextUI;

	#endregion

	#region [ PROPERTIES ]

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		extraHealthBarImage.fillAmount = 0;
		maxManaBarFillAmout = manaBarImage.fillAmount;

		CanvasGroup.blocksRaycasts = false;
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	protected override void Update ()
	{
		HandleLifeBar(healthBarImage, GameContext.PlayerHealth.CurrentHealth, GameContext.PlayerHealth.InitialHealth, true);

		HandleLifeBar(extraHealthBarImage, GameContext.PlayerHealth.ExtraHealth, GameContext.PlayerHealth.InitialHealth, false);
	}

	/// <summary>
	/// Uses the mana bar.
	/// </summary>
	/// <param name="time">Time.</param>
	/// <param name="callback">Callback.</param>
	public void UseManaBar(float time, Action callback)
	{
		StartCoroutine (HandleManaBar (time, callback));
	}

	/// <summary>
	/// Handles the mana bar.
	/// </summary>
	/// <returns>The mana bar.</returns>
	/// <param name="time">Time.</param>
	/// <param name="callback">Callback.</param>
	private IEnumerator HandleManaBar(float time, Action callback)
	{
		manaBarImage.fillAmount = 0;

		float start = manaBarImage.fillAmount;
		float rate = 1f / time;
		float progress = 0f;

		while (progress < 1f)
		{
			manaBarImage.fillAmount = Mathf.Lerp (start, maxManaBarFillAmout, progress);
			progress += rate * Time.unscaledDeltaTime;
			
			yield return null;
		}

		callback ();
	}

	/// <summary>
	/// Handles the life bar.
	/// </summary>
	/// <param name="image">Image.</param>
	/// <param name="current">Current.</param>
	/// <param name="max">Max.</param>
	/// <param name="changeColor">If set to <c>true</c> change color.</param>
	private void HandleLifeBar(Image image, float current, float max, bool changeColor)
	{
		float currentValue = Map (current, 0, max, 0, 1);

		image.fillAmount = Mathf.Lerp (image.fillAmount, currentValue, healthBarSpeed * Time.unscaledDeltaTime);

		if (changeColor)
		{
			if (current > max / 2)
			{
				image.color = new Color32((byte)Map(current, max / 2, max, 255, 0), 255, 0, 255);
			}
			else
			{
				image.color = new Color32(255, (byte)Map(current, 0, max / 2, 0, 255), 0, 255);
			}
		}
	}

	/// <summary>
	/// Map the specified x, inMin, inMax, outMin and outMax.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="inMin">In minimum.</param>
	/// <param name="inMax">In max.</param>
	/// <param name="outMin">Out minimum.</param>
	/// <param name="outMax">Out max.</param>
	private float Map(float x, float inMin, float inMax, float outMin, float outMax)
	{
		return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
	}

	/// <summary>
	/// Updates the gold text U.
	/// </summary>
	/// <param name="goldAmount">Gold amount.</param>
	public void UpdateGoldTextUI (int goldAmount)
	{
		goldTextUI.text = goldAmount.ToString();
	}

	#endregion
}