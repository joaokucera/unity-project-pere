using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TimerItemImage : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The image U.
	/// </summary>
	private Image imageUI;

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		imageUI = GetComponent<Image> ();
		imageUI.fillAmount = 0;
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	protected override void Update ()
	{
		transform.position = GameContext.PlayerObject.transform.position;
	}

	/// <summary>
	/// Starts the clock.
	/// </summary>
	public void StartClock(float time)
	{
		StartCoroutine (ReduceClockTime(time));

		imageUI.fillAmount -= Time.deltaTime;
	}

	/// <summary>
	/// Clocking the specified time.
	/// </summary>
	/// <param name="time">Time.</param>
	private IEnumerator ReduceClockTime(float time)
	{
		imageUI.fillAmount = 1;

		float reducer = time * Time.deltaTime;

		for (; time > 0; time -= Time.deltaTime)
		{
			imageUI.fillAmount -= reducer * Time.deltaTime;

			yield return null;
		}

		//imageUI.fillAmount = 0;
	}

	#endregion
}