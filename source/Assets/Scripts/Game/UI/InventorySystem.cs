using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventorySystem : BaseCanvas 
{
	#region [ FIELDS ]
	
	/// <summary>
	/// From slot.
	/// </summary>
	private InventorySlot fromSlot;
	/// <summary>
	/// To slot.
	/// </summary>
	private InventorySlot toSlot;
	/// <summary>
	/// The clicked slot.
	/// </summary>
	private InventorySlot clickedSlot;
	/// <summary>
	/// The clicked hot key.
	/// </summary>
	private HotkeySlot clickedHotKey;
	/// <summary>
	/// The event system.
	/// </summary>
	private EventSystem eventSystem;
	/// <summary>
	/// The image U.
	/// </summary>
	private Image imageUI;
	/// <summary>
	/// The inventory slots.
	/// </summary>
	private List<InventorySlot> inventorySlots = new List<InventorySlot>();
	/// <summary>
	/// The inventory background.
	/// </summary>
	[SerializeField]
	private GameObject inventoryBackground;
	/// <summary>
	/// The gold amount.
	/// </summary>
	private int goldAmount = 0;
	/// <summary>
	/// The gold text U.
	/// </summary>
	[SerializeField]
	private Text goldTextUI;
	/// <summary>
	/// The dialog box.
	/// </summary>
	[SerializeField]
	private RectTransform dialogBox;
	/// <summary>
	/// The size of the dialog box.
	/// </summary>
	private Vector2 dialogBoxSize;
	/// <summary>
	/// The dialog box buttons.
	/// </summary>
	[SerializeField]
	private Button[] dialogBoxButtons;
	/// <summary>
	/// The tooltip.
	/// </summary>
	[SerializeField]
	private RectTransform tooltip;
	/// <summary>
	/// The tooltip size text.
	/// </summary>
	[SerializeField]
	private Text tooltipSizeText;
	/// <summary>
	/// The tooltip visual text.
	/// </summary>
	[SerializeField]
	private Text tooltipVisualText;
	/// <summary>
	/// The weapons hot keys.
	/// </summary>
	[SerializeField]
	private HotkeySlot[] weaponsHotKeys = new HotkeySlot[2];
	/// <summary>
	/// The items hot keys.
	/// </summary>
	[SerializeField]
	private HotkeySlot[] itemsHotKeys = new HotkeySlot[4];
	/// <summary>
	/// The selected slot sprite.
	/// </summary>
	[SerializeField]
	private Sprite selectedSlotSprite;
	/// <summary>
	/// The original slot sprite.
	/// </summary>
	private Sprite originalSlotSprite;
	/// <summary>
	/// The tabs images.
	/// </summary>
	[SerializeField]
	private Image[] tabsImages = new Image[3];
	/// <summary>
	/// The tabs sprites.
	/// </summary>
	[SerializeField]
	private Sprite[] tabsSprites = new Sprite[6];

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets from slot.
	/// </summary>
	/// <value>From slot.</value>
	public InventorySlot FromSlot { get { return fromSlot; } set { fromSlot = value; } }
	/// <summary>
	/// Gets the empty slots.
	/// </summary>
	/// <value>The empty slots.</value>
	public int EmptySlots { get { return inventorySlots.Count(i => i.IsEmpty); } }
	/// <summary>
	/// Gets the image U.
	/// </summary>
	/// <value>The image U.</value>
	private EventSystem EventSystem
	{
		get
		{
			if (eventSystem == null)
			{
				eventSystem = GameObject.FindObjectOfType<EventSystem> ();
			}
			
			return eventSystem;
		}
	}
	/// <summary>
	/// Gets the weapons hot keys.
	/// </summary>
	/// <value>The weapons hot keys.</value>
	public HotkeySlot[] WeaponsHotKeys { get { return weaponsHotKeys; } }
	/// <summary>
	/// Gets the items hot keys.
	/// </summary>
	/// <value>The items hot keys.</value>
	public HotkeySlot[] ItemsHotKeys { get { return itemsHotKeys; } }

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		HideDialogBox ();
		HideTootip ();

		UpdateGoldTextUI ();
		dialogBoxSize = dialogBox.sizeDelta;

		CreateLayout ();
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	protected override void Update ()
	{
		if (GameContext.PauseMenu.IsPaused) return;

		if (InputControl.IsInventoryPressed)
		{
			if (Active)
			{
				Close();
			}
			else
			{
				Open();
			}
		}
		else if (InputControl.IsShotPressed && !EventSystem.current.IsPointerOverGameObject (-1))
		{
			Close();
		}
		else if (InputControl.IsHotKeyDownPressed)
		{
			OnHotKeyPressed(HotkeyType.ItemDown);
		}
		else if (InputControl.IsHotKeyRightPressed)
		{
			OnHotKeyPressed(HotkeyType.ItemRight);
		}
		else if (InputControl.IsHotKeyLeftPressed)
		{
			OnHotKeyPressed(HotkeyType.ItemLeft);
		}
		else if (InputControl.IsHotKeyUpPressed)
		{
			OnHotKeyPressed(HotkeyType.ItemUp);
		}
	}

	/// <summary>
	/// Shows all slots.
	/// </summary>
	public void ShowAllSlots()
	{
		ChangeTabSprites (InventoryTab.All);

		ResetMoveItem ();
		HideDialogBox ();

		for (int i = 0; i < inventorySlots.Count; i++) 
		{
			var slot = inventorySlots[i];
		
			if (!slot.IsEmpty && !slot.Equiped)
			{
				slot.ButtonUI.interactable = true;
			}
		}
	}

	/// <summary>
	/// Shows the weapon slots.
	/// </summary>
	public void ShowWeaponSlots()
	{
		ChangeTabSprites (InventoryTab.Weapons);

		ResetMoveItem ();
		HideDialogBox ();

		for (int i = 0; i < inventorySlots.Count; i++) 
		{
			var slot = inventorySlots[i];
			
			if (!slot.IsEmpty && !slot.Equiped)
			{
				slot.ButtonUI.interactable = slot.CurrentItem.IsWeapon;
			}
		}
	}

	/// <summary>
	/// Shows the item slots.
	/// </summary>
	public void ShowItemSlots()
	{
		ChangeTabSprites (InventoryTab.Items);

		ResetMoveItem ();
		HideDialogBox ();

		for (int i = 0; i < inventorySlots.Count; i++) 
		{
			var slot = inventorySlots[i];
			
			if (!slot.IsEmpty && !slot.Equiped)
			{
				slot.ButtonUI.interactable = !slot.CurrentItem.IsWeapon;
			}
		}
	}

	/// <summary>
	/// Raises the hot key pressed event.
	/// </summary>
	/// <param name="type">Type.</param>
	private void OnHotKeyPressed(HotkeyType type)
	{
		int index = (int)type;

		if (itemsHotKeys[index].TryToUse())
		{
			for (int i = 0; i < itemsHotKeys.Length; i++)
			{
				var slot = itemsHotKeys[i];

				if (slot.CurrentSlot != null && slot.CurrentSlot.IsEmpty)
				{
					slot.ClearSlot();
				}
			}
		}
	}

	/// <summary>
	/// Creates the layout.
	/// </summary>
	private void CreateLayout()
	{
		inventorySlots = GetComponentsInChildren<InventorySlot> ().ToList();

		originalSlotSprite = inventorySlots.First ().ImageUI.sprite;

		ShowAllSlots ();
	}

	/// <summary>
	/// Updates the gold text U.
	/// </summary>
	private void UpdateGoldTextUI()
	{
		goldTextUI.text = goldAmount.ToString();

		GameContext.StatusBarSystem.UpdateGoldTextUI (goldAmount);
	}

	/// <summary>
	/// Adds the item.
	/// </summary>
	/// <param name="item">Item.</param>
	public void AddItem(BaseItem item)
	{
		if (item.ItemType == ItemType.Gold)
		{
			goldAmount++;

			UpdateGoldTextUI();
		}
		else
		{
			InventorySlot slot = inventorySlots.FirstOrDefault(s => !s.IsEmpty && s.CurrentItem.ItemType == item.ItemType);

			if (slot != null)
			{
				slot.AddItem(item);

				return;
			}

			if (EmptySlots > 0)
			{
				OnPlaceItem(item);
			}
		}
	}

	/// <summary>
	/// Raises the place item event.
	/// </summary>
	/// <param name="item">Item.</param>
	private void OnPlaceItem(BaseItem item)
	{
		if (EmptySlots > 0)
		{
			InventorySlot slot = inventorySlots.FirstOrDefault(s => s.IsEmpty);

			if (slot != null)
			{
				slot.AddItem(item);
			}
		}
	}

	/// <summary>
	/// Moves the item.
	/// </summary>
	/// <param name="clicked">Clicked.</param>
	public void MoveItem(InventorySlot clicked)
	{
		if (!Active) return;

		if (clickedSlot == null)
		{
			if (!clicked.IsEmpty)
			{
				clickedSlot = clicked;

				if (clickedSlot.CurrentItem.IsWeapon)
				{
					OnSelect();
				}
				else
				{
					ShowDialogBox (clicked.RectTransform);
				}
			}
		}
		else if (!dialogBox.gameObject.activeInHierarchy)
		{
			clickedSlot = clicked;

			OnSelect();
		}
		else
		{
			HideDialogBox();
		}
	}

	/// <summary>
	/// Raises the use event.
	/// </summary>
	public void OnUse()
	{
		clickedSlot.OnUseItem ();

		HideDialogBox ();
	}

	/// <summary>
	/// Raises the select event.
	/// </summary>
	public void OnSelect()
	{
		if (fromSlot == null)
		{
			SelectInventorySlot (clickedSlot);

			HideDialogBox (false);
		}
		else if (toSlot == null)
		{
			toSlot = clickedSlot;

			HideDialogBox ();
		}

		if (fromSlot != null && toSlot != null)
		{
			Stack<BaseItem> tmpToSlot = new Stack<BaseItem>(toSlot.Items);

			toSlot.AddItems(fromSlot.Items);

			if (tmpToSlot.Count == 0)
			{
				fromSlot.ClearSlot();
			}
			else
			{
				fromSlot.AddItems(tmpToSlot);
			}

			ResetMoveItem ();
		}
	}

	/// <summary>
	/// Raises the remove event.
	/// </summary>
	public void OnRemove()
	{
		if (clickedHotKey != null)
		{
			clickedHotKey.ClearSlot();
			clickedHotKey = null;

			HideDialogBox();
		}
	}

	/// <summary>
	/// Selects the inventory slot.
	/// </summary>
	/// <param name="clicked">Clicked.</param>
	private void SelectInventorySlot (InventorySlot clicked)
	{
		if (!clicked.IsEmpty)
		{
			fromSlot = clicked;
			fromSlot.ImageUI.sprite = selectedSlotSprite;

			if (clicked.CurrentItem.IsWeapon)
			{
				ActiveHotKeys(true, false);
			}
			else
			{
				ActiveHotKeys(false, true);
			}
		}
	}

	/// <summary>
	/// Changes the tab sprites.
	/// </summary>
	/// <param name="index">Index.</param>
	private void ChangeTabSprites(InventoryTab tab)
	{
		for (int i = 0; i < tabsImages.Length; i++)
		{
			tabsImages[i].sprite = tabsSprites[i + 3];
		}

		int index = (int)tab;
		tabsImages[index].sprite = tabsSprites[index];
	}

	/// <summary>
	/// Actives the hot keys.
	/// </summary>
	/// <param name="activeWeapons">If set to <c>true</c> active weapons.</param>
	/// <param name="activeItems">If set to <c>true</c> active items.</param>
	private void ActiveHotKeys(bool activeWeapons, bool activeItems)
	{
		for (int i = 0; i < weaponsHotKeys.Length; i++)
		{
			weaponsHotKeys[i].ButtonUI.interactable = activeWeapons;
		}

		for (int i = 0; i < itemsHotKeys.Length; i++)
		{
			itemsHotKeys[i].ButtonUI.interactable = activeItems;
		}
	}

	/// <summary>
	/// Resets the move item.
	/// </summary>
	public void ResetMoveItem ()
	{
		if (fromSlot != null)
		{
			fromSlot.ImageUI.sprite = originalSlotSprite;
		}

		fromSlot = null;
		toSlot = null;

		ActiveHotKeys(true, true);
	}

	/// <summary>
	/// Shows the dialog over hot key.
	/// </summary>
	/// <param name="clicked">Clicked.</param>
	public void ShowDialogOverHotKey(HotkeySlot clicked)
	{
		clickedHotKey = clicked;

		ShowDialogBox(clickedHotKey.transform as RectTransform, true);
	}

	/// <summary>
	/// Shows the dialog box.
	/// </summary>
	/// <param name="parentSlot">Parent slot.</param>
	/// <param name="overHotKeySlot">If set to <c>true</c> over hot key slot.</param>
	public void ShowDialogBox(RectTransform parentSlot, bool overHotKeySlot = false)
	{
		HideTootip ();

		if (overHotKeySlot)
		{
			dialogBox.sizeDelta = new Vector2(dialogBoxSize.x, dialogBoxSize.y / 2);

			for (int i = 0; i < dialogBoxButtons.Length; i++) 
			{
				dialogBoxButtons[i].gameObject.SetActive(false);
			}
			dialogBoxButtons.Last().gameObject.SetActive(true);
		}
		else
		{
			dialogBox.sizeDelta = dialogBoxSize;

			for (int i = 0; i < dialogBoxButtons.Length; i++)
			{
				dialogBoxButtons[i].gameObject.SetActive(true);
			}
			dialogBoxButtons.Last().gameObject.SetActive(false);
		}

		dialogBox.SetParent (parentSlot);
		dialogBox.localPosition = new Vector3(parentSlot.sizeDelta.x / 2f, - parentSlot.sizeDelta.y / 2f);
		dialogBox.SetParent (transform);

		dialogBox.gameObject.SetActive (true);
	}

	/// <summary>
	/// Hides the dialog box.
	/// </summary>
	/// <param name="nullableClickedSlot">If set to <c>true</c> nullable clicked slot.</param>
	public void HideDialogBox(bool nullableClickedSlot = true)
	{
		if (nullableClickedSlot)
		{
			clickedSlot = null;
		}

		dialogBox.gameObject.SetActive (false);
	}

	/// <summary>
	/// Shows the tootip.
	/// </summary>
	/// <param name="slot">Slot.</param>
	public void ShowTootip(InventorySlot slot)
	{
		if (!slot.IsEmpty && clickedSlot == null)
		{
			ResetMoveItem();
			HideDialogBox();

			tooltipSizeText.text = slot.CurrentItem.OnTooltip ();
			tooltipVisualText.text = tooltipSizeText.text;

			tooltip.transform.SetParent (slot.transform);
			tooltip.transform.localPosition = new Vector3(slot.RectTransform.sizeDelta.x / 2f, - slot.RectTransform.sizeDelta.y / 2f);
			tooltip.transform.SetParent (transform);

			tooltip.gameObject.SetActive (true);
		}
		else
		{
			HideTootip ();
		}
	}

	/// <summary>
	/// Hides the tootip.
	/// </summary>
	public void HideTootip()
	{
		tooltipSizeText.text = string.Empty;
		tooltipVisualText.text = tooltipSizeText.text;

		tooltip.gameObject.SetActive (false);
	}

	/// <summary>
	/// Open this instance.
	/// </summary>
	protected override void Open ()
	{
		base.Open ();

		ShowAllSlots ();
	}

	/// <summary>
	/// Close this instance.
	/// </summary>
	protected override void Close ()
	{
		base.Close ();
	
		ResetMoveItem ();
		HideDialogBox ();
		HideTootip ();
	}

	/// <summary>
	/// Opens the immediately.
	/// </summary>
	public override void OpenImmediately()
	{
		ShowAllSlots ();

		base.OpenImmediately ();
	}

	/// <summary>
	/// Instants the close.
	/// </summary>
	public override void CloseImmediately()
	{
		base.CloseImmediately ();

		ResetMoveItem ();
		HideDialogBox ();
		HideTootip ();
	}

	#endregion
}