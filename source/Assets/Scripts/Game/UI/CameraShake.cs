using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class CameraShake : BaseScript
{
    #region [ FIELDS ]

    /// <summary>
    /// Original position.
    /// </summary>
    private Vector3 originPosition;
    /// <summary>
    /// Original rotation.
    /// </summary>
    private Quaternion originRotation;
    /// <summary>
    /// Shake variables.
    /// </summary>
	private float shakeIntensity = 0;
	/// <summary>
	/// The shake decay.
	/// </summary>
	[SerializeField]
    private float shakeDecay = 0.001f;
	/// <summary>
	/// The shake coef intensity.
	/// </summary>
	[SerializeField]
    private float shakeCoefIntensity = 0.01f;

    #endregion

    #region [ METHODS ]

	/// <summary>
	/// Shake the specified multiplier.
	/// </summary>
	/// <param name="multiplier">Multiplier.</param>
	public void Shake(float multiplier)
	{
		originPosition = GameContext.MainCamera.transform.position;
		originRotation = GameContext.MainCamera.transform.rotation;
		
		shakeIntensity = shakeCoefIntensity;
		
		StartCoroutine (UpdateShake (multiplier));
	}

	/// <summary>
	/// Shakes the update.
	/// </summary>
	/// <returns>The update.</returns>
	private IEnumerator UpdateShake(float multiplier)
	{
		/// When shake intensity is bigger than zero.
		while (shakeIntensity > 0)
		{
			GameContext.MainCamera.transform.position = originPosition + Random.insideUnitSphere * shakeIntensity;
			
			GameContext.MainCamera.transform.rotation = new Quaternion
			(
				originRotation.x + Random.Range(-shakeIntensity, shakeIntensity) * multiplier,
				originRotation.y + Random.Range(-shakeIntensity, shakeIntensity) * multiplier,
				originRotation.z + Random.Range(-shakeIntensity, shakeIntensity) * multiplier,
				originRotation.w + Random.Range(-shakeIntensity, shakeIntensity) * multiplier
			);
			
			shakeIntensity -= shakeDecay;

			originPosition = GameContext.MainCamera.transform.position;

			yield return null;
		}

		GameContext.MainCamera.transform.position = originPosition;
		GameContext.MainCamera.transform.rotation = originRotation;
	}

    #endregion
}