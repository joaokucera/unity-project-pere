using UnityEngine;
using System.Collections;

public class PauseMenu : BaseCanvas 
{
	#region [ FIELDS ]

	/// <summary>
	/// The is paused.
	/// </summary>
	private bool isPaused;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets a value indicating whether this instance is paused.
	/// </summary>
	/// <value><c>true</c> if this instance is paused; otherwise, <c>false</c>.</value>
	public bool IsPaused { get { return isPaused; } }

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	protected override void Update ()
	{
		if (InputControl.IsPausePressed)
		{
			if (Active)
			{
				Close();
			}
			else
			{
				Open();
			}
		}
	}

	/// <summary>
	/// Open this instance.
	/// </summary>
	protected override void Open ()
	{
		isPaused = true;

		GameContext.StatusBarSystem.CloseImmediately ();
		GameContext.InventorySystem.CloseImmediately ();

		base.Open ();
	}

	/// <summary>
	/// Close this instance.
	/// </summary>
	protected override void Close ()
	{
		isPaused = false;

		GameContext.StatusBarSystem.OpenImmediately ();

		base.Close ();
	}

	#endregion
}