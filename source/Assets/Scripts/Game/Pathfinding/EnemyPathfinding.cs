using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class EnemyPathfinding : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The nodes.
	/// </summary>
	private Node[,] nodes;
	/// <summary>
	/// The path request queue.
	/// </summary>
	private Queue<PathRequest> pathRequestQueue = new Queue<PathRequest> ();
	/// <summary>
	/// The current path request.
	/// </summary>
	private PathRequest currentPathRequest;
	/// <summary>
	/// The is processing path.
	/// </summary>
	private bool isProcessingPath;

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		nodes = new Node[GameContext.Grid.GridSizeX, GameContext.Grid.GridSizeY];

		for (int x = 0; x < GameContext.Grid.GridSizeX; x++)
		{
			for (int y = 0; y < GameContext.Grid.GridSizeY; y++) 
			{
				Node original = GameContext.Grid.Nodes[x, y];

				nodes[x, y] = new Node(original.walkable, original.worldPosition, x, y, original.movementPenalty);
			}
		}
	}

	/// <summary>
	/// Requests the path.
	/// </summary>
	/// <param name="pathStart">Path start.</param>
	/// <param name="pathEnd">Path end.</param>
	/// <param name="callback">Callback.</param>
	public void RequestPath(Vector2 pathStart, Vector2 pathEnd, Action<Vector2[], bool> callback)
	{
		PathRequest newRequest = new PathRequest (pathStart, pathEnd, callback);
		
		pathRequestQueue.Enqueue (newRequest);
		TryProcessNext ();
	}
	
	/// <summary>
	/// Tries the process next.
	/// </summary>
	private void TryProcessNext()
	{
		if (!isProcessingPath && pathRequestQueue.Count > 0)
		{
			currentPathRequest = pathRequestQueue.Dequeue();
			isProcessingPath = true;
			
			StartFindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd);
		}
	}
	
	/// <summary>
	/// Finisheds the processing path.
	/// </summary>
	/// <param name="path">Path.</param>
	/// <param name="success">If set to <c>true</c> success.</param>
	private void FinishedProcessingPath(Vector2[] path, bool success)
	{
		currentPathRequest.callback (path, success);
		isProcessingPath = false;
		TryProcessNext ();
	}

	/// <summary>
	/// Starts the find path.
	/// </summary>
	/// <param name="startPos">Start position.</param>
	/// <param name="endPos">End position.</param>
	private void StartFindPath (Vector2 startPos, Vector2 endPos)
	{
		StartCoroutine (FindPath (startPos, endPos));
	}

	/// <summary>
	/// Finds the path.
	/// </summary>
	/// <returns>The path.</returns>
	/// <param name="startPos">Start position.</param>
	/// <param name="targetPos">Target position.</param>
	private IEnumerator FindPath(Vector2 startPos, Vector2 targetPos)
	{
		Vector2[] waypoints = new Vector2[0];
		bool pathSuccess = false;

		Node startNode = GetNodeFromWorldPoint (startPos);
		Node targetNode = GetNodeFromWorldPoint (targetPos);

		if (startNode.walkable && targetNode.walkable)
		{
			Heap<Node> openSet = new Heap<Node> (GameContext.Grid.MaxSize);
			HashSet<Node> closeSet = new HashSet<Node> ();
			openSet.Add (startNode);

			while (openSet.Count > 0) 
			{
				Node currentNode = openSet.RemoveFirst();
				closeSet.Add(currentNode);

				if (currentNode == targetNode)
				{
					pathSuccess = true;
					break;
				}

				foreach (Node neighbour in GetNeighbours(currentNode))
				{
					if (!neighbour.walkable || closeSet.Contains(neighbour))
					{
						continue;
					}

					int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour) + neighbour.movementPenalty;
					if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
					{
						neighbour.gCost = newMovementCostToNeighbour;
						neighbour.hCost = GetDistance(neighbour, targetNode);
						neighbour.parent = currentNode;

						if (!openSet.Contains(neighbour))
						{
							openSet.Add(neighbour);
						}
						else
						{
							openSet.UpdateItem(neighbour);
						}
					}
				}
			}
		}
		yield return null;

		if (pathSuccess)
		{
			waypoints = RetracePath(startNode, targetNode);
		}

		FinishedProcessingPath (waypoints, pathSuccess);
	}

	/// <summary>
	/// Gets the neighbours.
	/// </summary>
	/// <returns>The neighbours.</returns>
	/// <param name="node">Node.</param>
	public List<Node> GetNeighbours(Node node)
	{
		List<Node> neighbour = new List<Node> ();
		
		for (int x = -1; x <= 1; x++) 
		{
			for (int y = -1; y <= 1; y++) 
			{
				if (x == 0 && y == 0)
				{
					continue;
				}

				int checkX = node.gridX + x;
				int checkY = node.gridY + y;
				
				if (checkX >= 0 && checkX < GameContext.Grid.GridSizeX && checkY >= 0 && checkY < GameContext.Grid.GridSizeY)
				{
					neighbour.Add(nodes[checkX, checkY]);
				}
			}
		}
		
		return neighbour;
	}
	
	/// <summary>
	/// Gets the node from world point.
	/// </summary>
	/// <returns>The node from world point.</returns>
	/// <param name="worldPosition">World position.</param>
	public Node GetNodeFromWorldPoint(Vector2 worldPosition)
	{
		float percentX = (worldPosition.x + GameContext.Grid.GridWorldSize.x / 2) / GameContext.Grid.GridWorldSize.x;
		float percentY = (worldPosition.y + GameContext.Grid.GridWorldSize.y / 2) / GameContext.Grid.GridWorldSize.y;
		percentX = Mathf.Clamp01 (percentX);
		percentY = Mathf.Clamp01 (percentY);
		
		int x = Mathf.RoundToInt((GameContext.Grid.GridSizeX - 1) * percentX);
		int y = Mathf.RoundToInt((GameContext.Grid.GridSizeY - 1) * percentY);

		return nodes[x, y];
	}

	/// <summary>
	/// Retraces the path.
	/// </summary>
	/// <param name="startNode">Start node.</param>
	/// <param name="endNode">End node.</param>
	private Vector2[] RetracePath(Node startNode, Node endNode)
	{
		List<Node> path = new List<Node> ();
		Node currentNode = endNode;

		while (currentNode != startNode) 
		{
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}

		path.Add (startNode);
		Vector2[] waypoints = SimplifyPath (path);
		Array.Reverse (waypoints);

		return waypoints;
	}

	/// <summary>
	/// Simplifies the path.
	/// </summary>
	/// <returns>The path.</returns>
	/// <param name="path">Path.</param>
	private Vector2[] SimplifyPath(List<Node> path)
	{
		List<Vector2> waypoints = new List<Vector2> ();
		Vector2 directionOld = Vector2.zero;

		for (int i = 1; i < path.Count; i++)
		{
			Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
			if (directionNew != directionOld)
			{
				waypoints.Add(path[i-1].worldPosition);
			}
			directionOld = directionNew;
		}

		return waypoints.ToArray ();
	}

	/// <summary>
	/// Gets the distance.
	/// </summary>
	/// <returns>The distance.</returns>
	/// <param name="nodeA">Node a.</param>
	/// <param name="nodeB">Node b.</param>
	private int GetDistance(Node nodeA, Node nodeB)
	{
		int dstX = Mathf.Abs (nodeA.gridX - nodeB.gridX);
		int dstY = Mathf.Abs (nodeA.gridY - nodeB.gridY);

		if (dstX > dstY)
		{
			return 14 * dstY + 10 * (dstX - dstY);
		}

		return 14 * dstX + 10 * (dstY - dstX);
	}

	#endregion
}