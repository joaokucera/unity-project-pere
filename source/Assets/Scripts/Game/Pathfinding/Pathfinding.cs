﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Pathfinding : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The grid.
	/// </summary>
	private Grid grid;
	/// <summary>
	/// The request manager.
	/// </summary>
	private PathRequestManager requestManager;

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	protected override void Awake ()
	{
		grid = GetComponent<Grid>();
		requestManager = GetComponent<PathRequestManager>();
	}

	/// <summary>
	/// Starts the find path.
	/// </summary>
	/// <param name="startPos">Start position.</param>
	/// <param name="endPos">End position.</param>
	public void StartFindPath (Vector2 startPos, Vector2 endPos)
	{
		StartCoroutine (FindPath (startPos, endPos));
	}

	/// <summary>
	/// Finds the path.
	/// </summary>
	/// <returns>The path.</returns>
	/// <param name="startPos">Start position.</param>
	/// <param name="targetPos">Target position.</param>
	private IEnumerator FindPath(Vector2 startPos, Vector2 targetPos)
	{
		Vector2[] waypoints = new Vector2[0];
		bool pathSuccess = false;

		Node startNode = grid.GetNodeFromWorldPoint (startPos);
		Node targetNode = grid.GetNodeFromWorldPoint (targetPos);

		if (startNode.walkable && targetNode.walkable)
		{
			Heap<Node> openSet = new Heap<Node> (grid.MaxSize);
			HashSet<Node> closeSet = new HashSet<Node> ();
			openSet.Add (startNode);

			while (openSet.Count > 0) 
			{
				Node currentNode = openSet.RemoveFirst();
				closeSet.Add(currentNode);

				if (currentNode == targetNode)
				{
					pathSuccess = true;
					break;
				}

				List<Node> neighbours = grid.GetNeighbours(currentNode);
				for (int i = 0; i < neighbours.Count; i++)
				{
					Node neighbour = neighbours[i];
				
					if (!neighbour.walkable || closeSet.Contains(neighbour))
					{
						continue;
					}

					int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour) + neighbour.movementPenalty;
					if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
					{
						neighbour.gCost = newMovementCostToNeighbour;
						neighbour.hCost = GetDistance(neighbour, targetNode);
						neighbour.parent = currentNode;

						if (!openSet.Contains(neighbour))
						{
							openSet.Add(neighbour);
						}
						else
						{
							openSet.UpdateItem(neighbour);
						}
					}
				}
			}
		}
		yield return null;

		if (pathSuccess)
		{
			waypoints = RetracePath(startNode, targetNode);
		}

		requestManager.FinishedProcessingPath (waypoints, pathSuccess);
	}

	/// <summary>
	/// Retraces the path.
	/// </summary>
	/// <param name="startNode">Start node.</param>
	/// <param name="endNode">End node.</param>
	private Vector2[] RetracePath(Node startNode, Node endNode)
	{
		List<Node> path = new List<Node> ();
		Node currentNode = endNode;

		while (currentNode != startNode) 
		{
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}

		path.Add (startNode);
		Vector2[] waypoints = SimplifyPath (path);
		Array.Reverse (waypoints);

		return waypoints;
	}

	/// <summary>
	/// Simplifies the path.
	/// </summary>
	/// <returns>The path.</returns>
	/// <param name="path">Path.</param>
	private Vector2[] SimplifyPath(List<Node> path)
	{
		List<Vector2> waypoints = new List<Vector2> ();
		Vector2 directionOld = Vector2.zero;

		for (int i = 1; i < path.Count; i++)
		{
			Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
			if (directionNew != directionOld)
			{
				waypoints.Add(path[i-1].worldPosition);
			}
			directionOld = directionNew;
		}

		return waypoints.ToArray ();
	}

	/// <summary>
	/// Gets the distance.
	/// </summary>
	/// <returns>The distance.</returns>
	/// <param name="nodeA">Node a.</param>
	/// <param name="nodeB">Node b.</param>
	private int GetDistance(Node nodeA, Node nodeB)
	{
		int dstX = Mathf.Abs (nodeA.gridX - nodeB.gridX);
		int dstY = Mathf.Abs (nodeA.gridY - nodeB.gridY);

		if (dstX > dstY)
		{
			return 14 * dstY + 10 * (dstX - dstY);
		}

		return 14 * dstX + 10 * (dstY - dstX);
	}

	#endregion
}