﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public struct RegionType
{
	public LayerMask mask;
	public int penalty;
}

[ExecuteInEditMode]
public class Grid : BaseScript
{
	#region [ FIELDS ]

	/// <summary>
	/// The display gizmos.
	/// </summary>
	[SerializeField]
	private bool displayGizmos;
	/// <summary>
	/// The unwalkable mask.
	/// </summary>
	[SerializeField]
	private LayerMask unwalkableMask;
	public static LayerMask UnwalkableMask;
	/// <summary>
	/// The size of the grid world.
	/// </summary>
	[SerializeField]
	private Vector2 gridWorldSize;
	/// <summary>
	/// The node radius.
	/// </summary>
	[SerializeField]
	private float nodeRadius;
	/// <summary>
	/// The regions.
	/// </summary>
	[SerializeField]
	private RegionType[] regions;

	/// <summary>
	/// The grid.
	/// </summary>
	private Node[,] grid;
	/// <summary>
	/// The node diameter.
	/// </summary>
	private float nodeDiameter;
	public static float NodeDiameter;
	/// <summary>
	/// The grid size x.
	/// </summary>
	private int gridSizeX, gridSizeY;
	/// <summary>
	/// The walkable mask.
	/// </summary>
	private LayerMask walkableMask;
	/// <summary>
	/// The walkabl regions dictionary.
	/// </summary>
	private Dictionary<int, int> walkableRegionsDictionary = new Dictionary<int, int> ();

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets the size of the max.
	/// </summary>
	/// <value>The size of the max.</value>
	public int MaxSize { get { return gridSizeX * gridSizeY; } }
	/// <summary>
	/// Gets the nodes.
	/// </summary>
	/// <value>The nodes.</value>
	public Node[,] Nodes { get { return grid; } }
	/// <summary>
	/// Gets the size of the grid world.
	/// </summary>
	/// <value>The size of the grid world.</value>
	public Vector2 GridWorldSize { get { return gridWorldSize; } }
	/// <summary>
	/// Gets the grid size x.
	/// </summary>
	/// <value>The grid size x.</value>
	public int GridSizeX { get { return gridSizeX; } }
	/// <summary>
	/// Gets the grid size y.
	/// </summary>
	/// <value>The grid size y.</value>
	public int GridSizeY { get { return gridSizeY; } }

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Awake ()
	{
		nodeDiameter = nodeRadius * 2f;
		gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
		gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);

		for (int i = 0; i < regions.Length; i++) 
		{
			RegionType region = regions[i];

			walkableMask.value |= region.mask.value;
			walkableRegionsDictionary.Add((int)Mathf.Log(region.mask.value, 2), region.penalty);
		}

		CreateGrid ();

		UnwalkableMask = unwalkableMask;
		NodeDiameter = nodeDiameter;
	}

	/// <summary>
	/// Creates the grid.
	/// </summary>
	private void CreateGrid()
	{
		grid = new Node[gridSizeX, gridSizeY];
		Vector2 worldBottomLeft = new Vector2(transform.position.x, transform.position.y) - Vector2.right * gridWorldSize.x / 2f - Vector2.up * gridWorldSize.y / 2;

		for (int x = 0; x < gridSizeX; x++) 
		{
			for (int y = 0; y < gridSizeY; y++) 
			{
				Vector2 worldPoint = worldBottomLeft + Vector2.right * (x * nodeDiameter + nodeRadius) + Vector2.up * (y * nodeDiameter + nodeRadius);
				bool walkable = Physics2D.OverlapCircle(worldPoint, nodeRadius, unwalkableMask) == null;

				int movementPenalty = 0;
				if (walkable)
				{
					Collider2D walkableCollider = Physics2D.OverlapCircle(worldPoint, nodeRadius, walkableMask);
					if (walkableCollider)
					{
						walkableRegionsDictionary.TryGetValue(walkableCollider.gameObject.layer, out movementPenalty);

					}
				}

				grid[x, y] = new Node(walkable, worldPoint, x, y, movementPenalty);
			}
		}
	}

	/// <summary>
	/// Gets the neighbours.
	/// </summary>
	/// <returns>The neighbours.</returns>
	/// <param name="node">Node.</param>
	public List<Node> GetNeighbours(Node node)
	{
		List<Node> neighbour = new List<Node> ();

		for (int x = -1; x <= 1; x++) 
		{
			for (int y = -1; y <= 1; y++) 
			{
				if (x == 0 && y == 0)
				{
					continue;
				}

				int checkX = node.gridX + x;
				int checkY = node.gridY + y;

				if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
				{
					neighbour.Add(grid[checkX, checkY]);
				}
			}
		}

		return neighbour;
	}

	/// <summary>
	/// Gets the node from world point.
	/// </summary>
	/// <returns>The node from world point.</returns>
	/// <param name="worldPosition">World position.</param>
	public Node GetNodeFromWorldPoint(Vector2 worldPosition)
	{
		float percentX = (worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x;
		float percentY = (worldPosition.y + gridWorldSize.y / 2) / gridWorldSize.y;
		percentX = Mathf.Clamp01 (percentX);
		percentY = Mathf.Clamp01 (percentY);

		int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
		int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);

		return grid[x, y];
	}

	/// <summary>
	/// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
	/// </summary>
	protected override void OnDrawGizmos ()
	{
		Gizmos.DrawWireCube (new Vector2(transform.position.x, transform.position.y), gridWorldSize);

		if (displayGizmos && grid != null)
		{
			foreach (Node n in grid)
			{
				Gizmos.color = n.walkable ? new Color(1, 1, 1, 0.1f) : new Color(1, 0, 0, 0.1f);
				Gizmos.DrawCube(n.worldPosition, Vector2.one * (nodeDiameter - 0.1f));
			}
		}
	}

	#endregion
}