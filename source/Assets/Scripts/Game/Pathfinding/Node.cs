﻿using UnityEngine;
using System;
using System.Collections;

public class Node : IHeapItem<Node>
{
	#region [ FIELDS ]

	/// <summary>
	/// The walkable.
	/// </summary>
	public bool walkable;
	/// <summary>
	/// The world position.
	/// </summary>
	public Vector2 worldPosition;
	/// <summary>
	/// The grid x.
	/// </summary>
	public int gridX;
	/// <summary>
	/// The grid y.
	/// </summary>
	public int gridY;
	/// <summary>
	/// The movement penalty.
	/// </summary>
	public int movementPenalty;
	/// <summary>
	/// The parent.
	/// </summary>
	public Node parent;
	/// <summary>
	/// The g cost.
	/// </summary>
	public int gCost;
	/// <summary>
	/// The h cost.
	/// </summary>
	public int hCost;
	/// <summary>
	/// Gets the f cost.
	/// </summary>
	/// <value>The f cost.</value>
	public int fCost { get { return gCost + hCost; } }

	/// <summary>
	/// The index of the heap.
	/// </summary>
	private int heapIndex;

	#endregion

	#region [ CONSTRUCTOR ]

	/// <summary>
	/// Initializes a new instance of the <see cref="Node"/> class.
	/// </summary>
	/// <param name="_walkable">If set to <c>true</c> _walkable.</param>
	/// <param name="_worldPos">_world position.</param>
	/// <param name="_gridX">_grid x.</param>
	/// <param name="_gridY">_grid y.</param>
	/// <param name="_penalty">_penalty.</param>
	public Node (bool _walkable, Vector2 _worldPos, int _gridX, int _gridY, int _penalty)
	{
		walkable = _walkable;
		worldPosition = _worldPos;
		gridX = _gridX;
		gridY = _gridY;
		movementPenalty = _penalty;
	}

	#endregion

	#region [ METHODS ]

	#region IComparable implementation

	/// <summary>
	/// Compares to.
	/// </summary>
	/// <returns>The to.</returns>
	/// <param name="other">Other.</param>
	public int CompareTo (Node other)
	{
		int compare = fCost.CompareTo (other.fCost);
		if (compare == 0)
		{
			compare = hCost.CompareTo(other.hCost);
		}

		return -compare;
	}

	#endregion

	#region IHeapItem implementation

	/// <summary>
	/// Gets or sets the index of the heap.
	/// </summary>
	/// <value>The index of the heap.</value>
	public int HeapIndex {
		get {
			return heapIndex;
		}
		set {
			heapIndex = value;
		}
	}

	#endregion

	#endregion
}