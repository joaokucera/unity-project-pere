﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public struct PathRequest
{
	public Vector2 pathStart;
	public Vector2 pathEnd;
	public Action<Vector2[], bool> callback;

	public PathRequest (Vector2 _start, Vector2 _end, Action<Vector2[], bool> _callback)
	{
		pathStart = _start;
		pathEnd = _end;
		callback = _callback;
	}
}

public class PathRequestManager : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The instance.
	/// </summary>
	private static PathRequestManager instance;

	/// <summary>
	/// The path request queue.
	/// </summary>
	private Queue<PathRequest> pathRequestQueue = new Queue<PathRequest> ();
	/// <summary>
	/// The current path request.
	/// </summary>
	private PathRequest currentPathRequest;
	/// <summary>
	/// The pathfinding.
	/// </summary>
	private Pathfinding pathfinding;
	/// <summary>
	/// The is processing path.
	/// </summary>
	private bool isProcessingPath;
	
	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	protected override void Awake ()
	{
		instance = this;
		pathfinding = GetComponent<Pathfinding> ();
	}

	/// <summary>
	/// Requests the path.
	/// </summary>
	/// <param name="pathStart">Path start.</param>
	/// <param name="pathEnd">Path end.</param>
	/// <param name="callback">Callback.</param>
	public static void RequestPath(Vector2 pathStart, Vector2 pathEnd, Action<Vector2[], bool> callback)
	{
		PathRequest newRequest = new PathRequest (pathStart, pathEnd, callback);

		instance.pathRequestQueue.Enqueue (newRequest);
		instance.TryProcessNext ();
	}

	/// <summary>
	/// Tries the process next.
	/// </summary>
	private void TryProcessNext()
	{
		if (!isProcessingPath && pathRequestQueue.Count > 0)
		{
			currentPathRequest = pathRequestQueue.Dequeue();
			isProcessingPath = true;

			pathfinding.StartFindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd);
		}
	}

	/// <summary>
	/// Finisheds the processing path.
	/// </summary>
	/// <param name="path">Path.</param>
	/// <param name="success">If set to <c>true</c> success.</param>
	public void FinishedProcessingPath(Vector2[] path, bool success)
	{
		currentPathRequest.callback (path, success);
		isProcessingPath = false;
		TryProcessNext ();
	}

	#endregion
}