using UnityEngine;
using System.Collections;

public class EnemyUnit : EnemyPathfinding 
{
	public Transform target;
	private float speed = 20f;
	private Vector2[] path;
	private int targetIndex;

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Update ()
	{
		RequestPath (transform.position, target.position, OnPathFound);
	}
	
	/// <summary>
	/// Raises the path found event.
	/// </summary>
	/// <param name="newPath">New path.</param>
	/// <param name="pathSuccessful">If set to <c>true</c> path successful.</param>
	public void OnPathFound(Vector2[] newPath, bool pathSuccessful)
	{
		if (pathSuccessful)
		{
			path = newPath;
			
			StopCoroutine("FollowPath");
			StartCoroutine("FollowPath");
		}
	}
	
	/// <summary>
	/// Follows the path.
	/// </summary>
	/// <returns>The path.</returns>
	private IEnumerator FollowPath()
	{
		if (path.Length == 0)
		{
			yield break;
		}
		
		Vector2 currentWaypoint = path[0];
		
		while (true) 
		{
			Vector2 pos = new Vector2(transform.position.x, transform.position.y);
			if (pos == currentWaypoint)
			{
				targetIndex++;
				if (targetIndex >= path.Length)
				{
					targetIndex = 0;
					path = new Vector2[0];
					
					yield break;
				}
				currentWaypoint = path[targetIndex];
			}
			
			transform.position = Vector2.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
			
			yield return null;
		}
	}
	
	/// <summary>
	/// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
	/// </summary>
	protected override void OnDrawGizmos ()
	{
		if (path != null)
		{
			for (int i = targetIndex; i < path.Length; i++) 
			{
				Gizmos.color = Color.white;
				Gizmos.DrawCube(path[i], Vector2.one);
				
				if (i == targetIndex)
				{
					Gizmos.DrawLine(transform.position, path[i]);
				}
				else
				{
					Gizmos.DrawLine(path[i-1], path[i]);
				}
			}
		}
	}
}
