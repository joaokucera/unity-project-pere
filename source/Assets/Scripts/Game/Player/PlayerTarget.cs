using UnityEngine;
using System.Collections;
using System;

public class PlayerTarget : BaseScript
{
	#region [ METHODS ]

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    protected override void Update ()
    {
        // Mouse position.
		Vector3 aimPosition = InputControl.GetMousePosition(GameContext.MainCamera, transform.position);

        // Set target position.
        transform.position = aimPosition;

        // Enforce target renderer inside of screen.
		transform.EnforceBounds(GameContext.MainCamera, renderer);
    }

	#endregion
}
