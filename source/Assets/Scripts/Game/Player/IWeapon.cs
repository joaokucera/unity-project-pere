﻿using UnityEngine;
using System.Collections;

public interface IWeapon
{
	void Shoot (HotkeyType type, Vector3 lastAimPosition, Quaternion angle, bool useAmmoAsDirection);

	void IncreaseDamage (int extraDamageValue, float extraDamageEffectTime);
}