﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class ManaSettings
{
	/// <summary>
	/// The dodge reload time.
	/// </summary>
	public float dodgeReloadTime = 1f;
	/// <summary>
	/// The dodge is full.
	/// </summary>
	[HideInInspector]
	public bool dodgeIsFull = true;
}