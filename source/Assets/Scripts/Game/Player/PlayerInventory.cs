using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PlayerInventory : BaseScript
{
    #region Methods

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		//transform.position = new Vector2 (transform.position.x, transform.parent.position.y + transform.parent.renderer.bounds.size.y / 2);

		collider2D.isTrigger = true;
	}
	
    /// <summary>
    /// When trigger is enabled, it detect when player is catching items.
    /// </summary>
	protected override void OnTriggerEnter2D (Collider2D collider)
    {
		if (collider.CompareTag(ShortcutWords.ItemTag) || collider.CompareTag(ShortcutWords.GoldTag))
        {
			BaseItem item = collider.GetComponent<BaseItem>();

			item.OnCollected(transform);

			GameContext.InventorySystem.AddItem(item);
        }
    }

    #endregion
}