using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerWeaponControl : BaseScript 
{
	#region [ FIELDS ]

	/// <summary>
	/// The type of the input.
	/// </summary>
	[SerializeField]
	private InputType inputType;
	/// <summary>
	/// The main weapon.
	/// </summary>
	private IWeapon weaponMain;
	/// <summary>
	/// The secondary weapon.
	/// </summary>
	private IWeapon weaponSecondary;
	/// <summary>
	/// The last aim position.
	/// </summary>
	private Vector3 lastAimPosition;
	/// <summary>
	/// The weapons.
	/// </summary>
	private Dictionary<ItemType, IWeapon> weaponsDictionary = new Dictionary<ItemType, IWeapon> ();

	#endregion

	#region [ PROPERTIES ]
	
	/// <summary>
	/// Gets or sets the type of the input.
	/// </summary>
	/// <value>The type of the input.</value>
	public InputType InputType 
	{
		get
		{
			return inputType;
		}
	}
	/// <summary>
	/// Gets the last aim position.
	/// </summary>
	/// <value>The last aim position.</value>
	public Vector3 LastAimPosition 
	{
		get
		{
			return lastAimPosition;
		}
	}
	
	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		lastAimPosition = -Vector3.up;

		CreateDictionaryWeapons ();
	}

	/// <summary>
	/// Creates the dictionary weapons.
	/// </summary>
	private void CreateDictionaryWeapons()
	{
		var slingWeapon = GetComponentInChildren<SlingWeapon> ();
		weaponsDictionary.Add (ItemType.Weapon_Sling, slingWeapon);

		var blowgunWeapon = GetComponentInChildren<BlowgunWeapon> ();
		weaponsDictionary.Add (ItemType.Weapon_Blowgun, blowgunWeapon);

		var koiereAxeWeapon = GetComponentInChildren<KoiereAxeWeapon> ();
		weaponsDictionary.Add (ItemType.Weapon_KoiereAxe, koiereAxeWeapon);

		var boleadeiraWeapon = GetComponentInChildren<BoleadeiraWeapon> ();
		weaponsDictionary.Add (ItemType.Weapon_Boleadeira, boleadeiraWeapon);
	}

	/// <summary>
	/// Switchs the weapon.
	/// </summary>
	/// <param name="itemType">Item type.</param>
	/// <param name="hotkeyType">Hotkey type.</param>
	public void SwitchWeapon(ItemType itemType, HotkeyType hotkeyType)
	{
		switch (hotkeyType) 
		{
			case HotkeyType.WeaponMain:
			{
				weaponMain = SelectWeapon(itemType);
				break;
			}
			case HotkeyType.WeaponSecondary:
			{
				weaponSecondary = SelectWeapon(itemType);
				break;
			}
			default:
			{
				break;
			}
		}
	}

	/// <summary>
	/// Switchs the weapon.
	/// </summary>
	/// <param name="itemType">Item type.</param>
	/// <param name="hotkeyType">Hotkey type.</param>
	public void RemoveWeapon(HotkeyType hotkeyType)
	{
		switch (hotkeyType) 
		{
			case HotkeyType.WeaponMain:
			{
				weaponMain = null;
				break;
			}
			case HotkeyType.WeaponSecondary:
			{
				weaponSecondary = null;
				break;
			}
			default:
			{
				break;
			}
		}
	}

	/// <summary>
	/// Selects the weapon.
	/// </summary>
	/// <returns>The weapon.</returns>
	/// <param name="itemType">Item type.</param>
	public IWeapon SelectWeapon(ItemType itemType)
	{ 
		IWeapon newWeapon;
		if (weaponsDictionary.TryGetValue(itemType, out newWeapon))
		{
			return newWeapon;
		}

		return null;
	}

	/// <summary>
	/// Increases the damage.
	/// </summary>
	/// <param name="extraDamageValue">Extra damage value.</param>
	/// <param name="extraDamageEffectTime">Extra damage effect time.</param>
	public void IncreaseDamage (int extraDamageValue, float extraDamageEffectTime)
	{
		if (weaponMain != null)
		{
			weaponMain.IncreaseDamage(extraDamageValue, extraDamageEffectTime);
		}
		
		if (weaponSecondary != null)
		{
			weaponSecondary.IncreaseDamage(extraDamageValue, extraDamageEffectTime);
		}
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	protected override void Update ()
	{
		if (inputType == InputType.Keyboard)
		{
			UpdateKeyboard();
		}
		else
		{
			UpdateJoystick();
		}
	}

	/// <summary>
	/// Updates the keyboard.
	/// </summary>
	private void UpdateKeyboard()
	{
		// Aim position.
		Vector3 aimPosition = InputControl.GetMousePosition(GameContext.MainCamera, transform.position);
		
		// Figure out angle.
		Quaternion angle = Quaternion.FromToRotation(Vector3.up, lastAimPosition - transform.position);

		// Main shoot.
		WeaponShoot (HotkeyType.WeaponMain, weaponMain, aimPosition, angle, true);
		// Secondary shoot.
		WeaponShoot (HotkeyType.WeaponSecondary, weaponSecondary, aimPosition, angle, true);
	}

	/// <summary>
	/// Updates the joystick.
	/// </summary>
	private void UpdateJoystick()
	{
		// Aim position.
		Vector3 aimPosition = InputControl.GetAimPosition(GameContext.MainCamera, transform.position);

		// Main shoot.
		WeaponShoot (HotkeyType.WeaponMain, weaponMain, aimPosition, Quaternion.identity, false);
		// Secondary shoot.
		WeaponShoot (HotkeyType.WeaponSecondary, weaponSecondary, aimPosition, Quaternion.identity, false);
	}

	/// <summary>
	/// Keyboards the shoot.
	/// </summary>
	/// <param name="weapon">Weapon.</param>
	/// <param name="angle">Angle.</param>
	private void WeaponShoot (HotkeyType type, IWeapon weapon, Vector3 aimPosition, Quaternion angle, bool useAmmoAsDirection)
	{
		// Hold last movement. 
		if (aimPosition.x != 0f || aimPosition.y != 0f)
		{
			lastAimPosition = aimPosition;
		}

		if (weapon != null)
		{
			weapon.Shoot (type, lastAimPosition, angle, useAmmoAsDirection);
		}
	}

	#endregion
}