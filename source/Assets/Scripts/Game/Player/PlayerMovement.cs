using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : BaseScript
{
    #region [ FIELDS ]

	/// <summary>
	/// The name of the stop extra speed method.
	/// </summary>
	private const string StopExtraSpeedMethodName = "StopExtraSpeed";

	/// <summary>
	/// The stamina.
	/// </summary>
	[SerializeField]
	private ManaSettings manaSettings;
    /// <summary>
    /// Player movement.
    /// </summary>
    private Vector3 movement;
	/// <summary>
	/// Player last movement.
	/// </summary>
	private Vector3 lastMovement;
	/// <summary>
	/// The initial walk speed.
	/// </summary>
	[SerializeField]
	private float initialWalkSpeed;
    /// <summary>
    /// Current walk speed.
    /// </summary>
	private float currentWalkSpeed;
	/// <summary>
	/// The extra walk speed.
	/// </summary>
	private float extraWalkSpeed = 1f;
	/// <summary>
    /// Current dodge speed.
    /// </summary>
	[SerializeField]
	private float currentDodgeSpeed;
	/// <summary>
	/// The current dodge distance.
	/// </summary>
	[SerializeField]
	private float currentDodgeDistance;
	/// <summary>
	/// The enforce player inside screen.
	/// </summary>
	[SerializeField]
	private bool enforceBounds = true;
	/// <summary>
	/// The z camera offset.
	/// </summary>
	private float zCameraOffset = -500;
	/// <summary>
	/// The trail dodge.
	/// </summary>
	private TrailRenderer trailDodge;
	/// <summary>
	/// The current checkpoint.
	/// </summary>
	private Vector2 currentCheckpoint;

    #endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets the pivot.
	/// </summary>
	/// <value>The pivot.</value>
	public Vector2 Pivot 
	{
		get 
		{ 
			return new Vector2 (transform.position.x, transform.position.y + renderer.bounds.size.y / 2);
		}
	}
	/// <summary>
	/// Gets or sets the initial walk speed.
	/// </summary>
	/// <value>The initial walk speed.</value>
	public float InitialWalkSpeed 
	{
		get
		{
			return initialWalkSpeed;
		}
	}
	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="PlayerMovement"/> enforce bounds.
	/// </summary>
	/// <value><c>true</c> if enforce bounds; otherwise, <c>false</c>.</value>
	public bool EnforceBounds 
	{ 
		get { return enforceBounds; } 
		set { enforceBounds = value; }
	}
	/// <summary>
	/// Sets the last movement.
	/// </summary>
	/// <value>The last movement.</value>
	public Vector2 LastMovement { get { return lastMovement; } }
	/// <summary>
	/// Gets the mask.
	/// </summary>
	/// <value>The mask.</value>
	public LayerMask Mask { get { return gameObject.layer; } }

	#endregion

    #region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		GameContext.MainCamera.transform.position = new Vector3(Pivot.x, Pivot.y, transform.localPosition.z + zCameraOffset);

//		BoxCollider2D boxCollider = ((BoxCollider2D)collider2D);
//		boxCollider.size = new Vector2 (boxCollider.size.x / 3, boxCollider.size.y / 3);
//		boxCollider.center = new Vector2 (boxCollider.center.x, boxCollider.size.y);

		rigidbody2D.fixedAngle = true;
		lastMovement = -Vector2.up;
		currentWalkSpeed = initialWalkSpeed;

		trailDodge = GetComponentInChildren<TrailRenderer> ();
		StopDodge ();

		DefineCheckpoint (transform.position);
	}

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
	protected override void Update ()
    {
		// Walk movement.
		movement = InputControl.GetMovement();

		// Hold last movement. 
		if (movement.x != 0f || movement.y != 0f) 
		{
			lastMovement = movement;
		}

        // Enforce player inside the screen.
		if (enforceBounds)
		{
        	transform.EnforceBounds(GameContext.MainCamera, renderer);
		}

		// Set Z (deepness) position.
		transform.SetFakeDepth ();
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
	protected override void FixedUpdate ()
    {
		// If dodge is enabled.
		if (InputControl.IsDodgePressed && manaSettings.dodgeIsFull)
		{
			StartCoroutine(Dodge());
		}
		else
		{
			// Movement through rigidbody.
			rigidbody2D.MovePosition(transform.position + movement * currentWalkSpeed * extraWalkSpeed * Time.fixedDeltaTime);
		}
    }

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this object (2D physics only).
	/// </summary>
	/// <param name="collider">Collider.</param>
	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		if (collider.CompareTag(ShortcutWords.CheckpointTag))
		{
			DefineCheckpoint(collider.transform.position);
		}
	}

	/// <summary>
	/// Dodge this instance.
	/// </summary>
	private IEnumerator Dodge()
	{
		PlayDodge ();

		for (float i = 0; i < currentDodgeDistance; i += Time.deltaTime)
		{
			rigidbody2D.AddForce(lastMovement * currentDodgeSpeed);

			yield return null;
		}

		StopDodge ();
	}

	/// <summary>
	/// Increases the speed.
	/// </summary>
	/// <param name="extraSpeedValue">Extra speed value.</param>
	public void IncreaseSpeed (float extraSpeedValue, float extraSpeedEffectTime)
	{
		extraWalkSpeed = extraSpeedValue;

		Invoke (StopExtraSpeedMethodName, extraSpeedEffectTime);
	}

	/// <summary>
	/// Stops the extra speed.
	/// </summary>
	private void StopExtraSpeed()
	{
		extraWalkSpeed = 1f;
	}

	/// <summary>
	/// Shows the dodge trail.
	/// </summary>
	private void PlayDodge ()
	{
		trailDodge.enabled = true;

		manaSettings.dodgeIsFull = false;

		GameContext.StatusBarSystem.UseManaBar (manaSettings.dodgeReloadTime, RecoverDodge);
	}
	
	/// <summary>
	/// Hides the dodge trail.
	/// </summary>
	private void StopDodge ()
	{
		trailDodge.enabled = false;
	}

	/// <summary>
	/// Recovers the dodge.
	/// </summary>
	public void RecoverDodge()
	{
		manaSettings.dodgeIsFull = true;
	}

	/// <summary>
	/// Defines the checkpoint.
	/// </summary>
	/// <param name="newCheckpointPosition">New checkpoint position.</param>
	private void DefineCheckpoint(Vector2 newCheckpointPosition)
	{
		currentCheckpoint = newCheckpointPosition;
	}

	/// <summary>
	/// Reborn this instance.
	/// </summary>
	public void Reborn ()
	{
		transform.position = currentCheckpoint;
	}

	/// <summary>
	/// Raises the GU event.
	/// </summary>
	protected override void OnGUI ()
	{
//		GUI.color = Color.yellow;
//		GUI.Label(new Rect(Screen.width - 80, 100, 200, 40), "SPEED");
//		GUI.Label(new Rect(Screen.width - 80, 120, 200, 40), "Normal: " + currentWalkSpeed);
//		GUI.Label(new Rect(Screen.width - 80, 140, 200, 40), "Extra: x" + extraWalkSpeed);
	}

    #endregion
}