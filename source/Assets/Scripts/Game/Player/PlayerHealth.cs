using UnityEngine;
using System.Collections;

public class PlayerHealth : BaseHealth
{
    #region [ FIELDS ]

	/// <summary>
	/// The damage hit against enemies.
	/// </summary>
	[SerializeField]
	private int defaultDamageHitAgainstEnemies = 1;
	/// <summary>
	/// The time to reborn.
	/// </summary>
	[SerializeField]
	private float timeToReborn = 2f;

    #endregion

    #region [ METHODS ]

	/// <summary>
	/// Raises the death event.
	/// </summary>
	protected override void OnDeath()
	{
		renderer.enabled = true;

		StartCoroutine (WaitToReborn ());
	}

	/// <summary>
	/// Waits to reborn.
	/// </summary>
	/// <returns>The to reborn.</returns>
	private IEnumerator WaitToReborn()
	{
		GameContext.GameOver.OpenImmediately ();
		GameContext.MainCamera.enabled = false;

		yield return new WaitForSeconds (timeToReborn);

		GameContext.MainCamera.enabled = true;
		GameContext.GameOver.CloseImmediately ();

		GameContext.PlayerMovement.Reborn ();
		currentHeath = initialHealth;
	}

	/// <summary>
	/// Raises the blink event.
	/// </summary>
	protected override void OnBlink()
	{
		renderer.enabled = !renderer.enabled;
	}

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		hitAgainstOwnerAttackTag = OwnerAttackTag.EnemyAttack;
	}

	/// <summary>
	/// Sent each frame where a collider on another object is touching this object's collider (2D physics only).
	/// </summary>
	/// <param name="collision">Collision.</param>
    protected override void OnCollisionStay2D (Collision2D collision)
    {
		if (!wasHit && collision.gameObject.CompareTag(ShortcutWords.EnemyTag))
		{
			BaseEnemy enemy = collision.gameObject.GetComponent<BaseEnemy> ();
			if (enemy)
			{
				Damage (enemy.DamageByTouchingThePlayer);
			}
			else 
			{
				Damage (defaultDamageHitAgainstEnemies);
			}
		}
    }

	/// <summary>
	/// Sent each frame where another object is within a trigger collider attached to this object (2D physics only).
	/// </summary>
	/// <param name="collision">Collision.</param>
	protected override void OnTriggerStay2D (Collider2D collider)
	{
		if (!wasHit && collider.CompareTag(ShortcutWords.EnemyTag)) 
		{
			BaseEnemy enemy = collider.GetComponent<BaseEnemy> ();
			if (enemy) 
			{
				Damage (enemy.DamageByTouchingThePlayer);
			}
			else 
			{
				Damage (defaultDamageHitAgainstEnemies);
			}
		}
	}

	protected override void OnTriggerExit2D (Collider2D collider)
	{
		base.OnTriggerExit2D (collider);

		if (collider.CompareTag (hitAgainstOwnerAttackTag.ToString ())) 
		{
			print("hit by: " + collider.name);
		}
	}

    /// <summary>
    /// Raises the GU event.
    /// </summary>
	protected override void OnGUI ()
    {
//		GUI.color = Color.yellow;
//      GUI.Label(new Rect(Screen.width - 80, 20, 200, 40), "HEALTH");
//		GUI.Label(new Rect(Screen.width - 80, 40, 200, 40), "Normal: " + currentHeath);
//		GUI.Label(new Rect(Screen.width - 80, 60, 200, 40), "Extra: +" + extraHealth);
    }

    /// <summary>
    /// Raises the recover event.
    /// </summary>
	protected override void OnRecover()
    {
		base.OnRecover();

        renderer.enabled = true;
    }

	/// <summary>
	/// Adds the extra health.
	/// </summary>
	/// <param name="extraHealthValue">Extra health value.</param>
	public void AddExtraHealth (int extraHealthValue)
	{
		if (extraHealth + extraHealthValue <= initialHealth)
		{
			extraHealth += extraHealthValue;
		}
	}

    #endregion
}