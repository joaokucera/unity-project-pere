using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Animator))]
public class PlayerAnimation : BaseScript
{
    #region [ FIELDS ]

    /// <summary>
    /// Facing side parameter
    /// </summary>
    private int FacingSideParameter = Animator.StringToHash("FacingSide");
    /// <summary>
    /// Animator component.
    /// </summary>
    private Animator animator;
    /// <summary>
    /// Control the current sprite side.
    /// </summary>
    private FacingSide currentFacingSide;
    /// <summary>
    /// The original scale in x.
    /// </summary>
    private float originalScaleX;

    #endregion
	
	#region [ PROPERTIES ]
	
	/// <summary>
	/// Gets the pivot.
	/// </summary>
	/// <value>The pivot.</value>
	private Vector2 Pivot 
	{
		get 
		{ 
			return new Vector2 (transform.position.x, transform.position.y + renderer.bounds.size.y / 2);
		}
	}
	
	#endregion

    #region [ METHODS ]

    /// <summary>
    /// Called when script start.
    /// </summary>
	protected override void Start ()
    {
        animator = GetComponent<Animator>();

        currentFacingSide = FacingSide.Down;
        originalScaleX = transform.localScale.x;
    }

    /// <summary>
    /// Update game logic.
    /// </summary>
	protected override void Update ()
    {
		if (GameContext.Paused) return;

        // Aim position.
		Vector3 aimPosition = GameContext.PlayerWeapon.LastAimPosition;

        // To evaluate which animation.
		if (GameContext.PlayerWeapon.InputType == InputType.Keyboard)
		{
			EvaluateAnimationFromKeyboard(aimPosition);
		}
		else
		{
			EvaluateAnimationFromJoystick(aimPosition);
		}

        // To update current animation.
        UpdateAnimation();
    }

    /// <summary>
    /// Method to evaluate which animation will be setted.
    /// </summary>
	private void EvaluateAnimationFromKeyboard(Vector2 aimPosition)
    {
		float xDistance = Pivot.x - aimPosition.x;
		float yDistance = Pivot.y - aimPosition.y;

		if (Mathf.Abs(xDistance) > Mathf.Abs(yDistance))
        {
			if (aimPosition.x >= Pivot.x)
            {
                currentFacingSide = FacingSide.Right;
            }
			else
            {
                currentFacingSide = FacingSide.Left;
            }
        }
        else
        {
			if (aimPosition.y >= Pivot.y)
            {
                currentFacingSide = FacingSide.Up;
            }
			else
            {
                currentFacingSide = FacingSide.Down;
            }
        }
    }

	/// <summary>
	/// Method to evaluate which animation will be setted.
	/// </summary>
	private void EvaluateAnimationFromJoystick(Vector2 aimPosition)
	{
		bool lookAtSide = Mathf.Abs(aimPosition.x) > Mathf.Abs(aimPosition.y);

		if (lookAtSide)
		{
			if (aimPosition.x >= 0)
			{
				currentFacingSide = FacingSide.Right;
			}
			else
			{
				currentFacingSide = FacingSide.Left;
			}
		}
		else
		{
			if (aimPosition.y >= 0)
			{
				currentFacingSide = FacingSide.Up;
			}
			else
			{
				currentFacingSide = FacingSide.Down;
			}
		}
	}

    /// <summary>
    /// Method to control animation.
    /// </summary>
    private void UpdateAnimation()
    {
        int parameter = Mathf.Abs((int)currentFacingSide);

        animator.SetInteger(FacingSideParameter, parameter);

        switch (currentFacingSide)
        {
            case FacingSide.Left:
                Flip(1);
                break;
            default:
                Flip(-1);
                break;
        }
    }

    /// <summary>
    /// Method to flip sprite.
    /// </summary>
    private void Flip(int xScale)
    {
        Vector2 scale = transform.localScale;
        scale.x = xScale * originalScaleX;
        transform.localScale = scale;
    }

    #endregion
}