using UnityEngine;
using System.Collections;

public class BlowgunWeapon : BaseWeapon, IWeapon
{
    #region [ FIELDS ]

    #endregion

    #region [ METHODS ]

	/// <summary>
	/// Shoot the specified type, lastAimPosition, angle and useAmmoAsDirection.
	/// </summary>
	/// <param name="type">Type.</param>
	/// <param name="lastAimPosition">Last aim position.</param>
	/// <param name="angle">Angle.</param>
	/// <param name="useAmmoAsDirection">If set to <c>true</c> use ammo as direction.</param>
	public void Shoot (HotkeyType type, Vector3 lastAimPosition, Quaternion angle, bool useAmmoAsDirection)
	{
		if (weaponSettings.CooldownSettings.Value > 0) return;

		// If shot button is pressed.
		if ((type == HotkeyType.WeaponMain && InputControl.IsShotPressed) || 
		    (type == HotkeyType.WeaponSecondary && InputControl.IsMeleePressed))
		{
			ExecuteRangedAttack(lastAimPosition, angle, useAmmoAsDirection);
		}
	}

	#endregion
}