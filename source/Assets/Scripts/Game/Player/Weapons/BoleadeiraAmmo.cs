using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CircleCollider2D))]
public class BoleadeiraAmmo : BaseAmmo
{
    #region [ FIELDS ]

	/// <summary>
	/// The back speed.
	/// </summary>
	[SerializeField]
	private float backSpeed;
	/// <summary>
	/// The rotation speed.
	/// </summary>
	[SerializeField]
	private float rotationSpeed;
	/// <summary>
	/// The improve distance.
	/// </summary>
	[SerializeField]
	private float improveDistance;
	/// <summary>
	/// The back to player.
	/// </summary>
	private bool backToPlayer = false;

    #endregion

	#region [ PROPERTIES ]
	
	/// <summary>
	/// Set if game object is active.
	/// </summary>
	public override bool Active
	{
		get 
		{
			return gameObject.activeInHierarchy; 
		}
		set 
		{ 
			gameObject.SetActive(value);
			
			backToPlayer = false;
		}
	}
	
	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		gameObject.tag = ShortcutWords.PlayerAttackTag;
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	protected override void Update ()
	{
		transform.SetFakeDepth ();
	}

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this object (2D physics only).
	/// </summary>
	/// <param name="collider">Collider.</param>
	protected override void OnTriggerEnter2D (Collider2D collider)
	{
		if (collider.CompareTag(ShortcutWords.ObstacleTag))
		{
			GameContext.ParticlesRepository.SpawnParticle (ParticleType.GreySplash, Pivot, transform.rotation);
			
			backToPlayer = true;
		}
		else if (backToPlayer && collider.CompareTag(ShortcutWords.PlayerTag))
		{
			backToPlayer = false;
		}
	}

	/// <summary>
	/// Dos the active.
	/// </summary>
	/// <param name="active">If set to <c>true</c> active.</param>
	public override void DoActive (bool active)
	{
		// TODO: Nothing.
	}

	#region implemented abstract members of BaseAmmo

	/// <summary>
	/// Raises the move event.
	/// </summary>
	/// <param name="settings">Settings.</param>
	protected override IEnumerator OnMove (WeaponSettings settings)
	{
		if (!backToPlayer)
		{
			ammoSettings = settings.AmmoSettings;
			transform.position += settings.AmmoSettings.Direction * improveDistance;

			for (float i = 0; i < settings.AmmoSettings.DamageSettings.Time; i += Time.deltaTime)
			{
				var distance = GameContext.PlayerMovement.Pivot - Pivot;
				transform.RotateAround(GameContext.PlayerMovement.Pivot + distance, Vector3.forward, settings.AmmoSettings.DamageSettings.Force * Time.deltaTime);
				
				transform.Rotate(-Vector3.forward * settings.AmmoSettings.DamageSettings.Force * rotationSpeed * Time.deltaTime);
				
				GameContext.ParticlesRepository.SpawnParticle (ParticleType.ExplosionEffect, Pivot, transform.rotation);

				if (backToPlayer)
				{
					break;
				}
				else
				{
					yield return null;
				}
			}

			transform.SetRotationZ(0);
			backToPlayer = true;

			while (backToPlayer)
			{
				transform.MoveTowardsTo(GameContext.PlayerMovement.Pivot, backSpeed * Time.deltaTime);
				
				yield return null;
			}
			
			Active = false;
		}
	}

	#endregion

	#endregion
}