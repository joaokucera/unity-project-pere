using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CircleCollider2D), typeof(Rigidbody2D))]
public class SlingAmmo : BaseAmmo
{
    #region [ FIELDS ]

    #endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		gameObject.tag = ShortcutWords.PlayerAttackTag;
	}

	#region implemented abstract members of BaseAmmo

	/// <summary>
	/// Raises the move event.
	/// </summary>
	/// <param name="settings">Settings.</param>
	protected override IEnumerator OnMove (WeaponSettings settings)	
	{
		ammoSettings = settings.AmmoSettings;
		rigidbody2D.AddForce(ammoSettings.Direction * settings.AmmoSettings.DamageSettings.Force);

		bool isFull = settings.LoadingSettings.Current == settings.LoadingSettings.Maximum;
		for (float i = 0; i < settings.AmmoSettings.DamageSettings.Time; i += Time.deltaTime)
		{
			if (isFull)
			{
				GameContext.ParticlesRepository.SpawnParticle (ParticleType.ExplosionEffect, Pivot, transform.rotation);
			}

			yield return null;
		}

		GameContext.ParticlesRepository.SpawnParticle (ParticleType.GreySplash, Pivot, transform.rotation);
		Active = false;
	}

	#endregion

	#endregion
}