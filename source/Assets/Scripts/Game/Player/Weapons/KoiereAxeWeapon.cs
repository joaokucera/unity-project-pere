using UnityEngine;
using System.Collections;

public class KoiereAxeWeapon : BaseWeapon, IWeapon
{
    #region [ FIELDS ]

	/// <summary>
	/// The melee weapon.
	/// </summary>
	private MeleeAttack meleeWeapon;

    #endregion

    #region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		meleeWeapon = GetComponentInChildren<MeleeAttack> ();
	}

	/// <summary>
	/// Shoot the specified type, lastAimPosition, angle and useAmmoAsDirection.
	/// </summary>
	/// <param name="type">Type.</param>
	/// <param name="lastAimPosition">Last aim position.</param>
	/// <param name="angle">Angle.</param>
	/// <param name="useAmmoAsDirection">If set to <c>true</c> use ammo as direction.</param>
	public void Shoot (HotkeyType type, Vector3 lastAimPosition, Quaternion angle, bool useAmmoAsDirection)
	{
		if (weaponSettings.CooldownSettings.Value > 0) return;
		
		// Holding shot button.
		if ((type == HotkeyType.WeaponMain && InputControl.IsShotLoading) || 
		    (type == HotkeyType.WeaponSecondary && InputControl.IsMeleeLoading))
		{
			GameContext.CameraSettings.ZoomIn(weaponSettings.LoadingSettings.Maximum);

			weaponSettings.LoadingSettings.Current += Time.deltaTime;
			weaponSettings.LoadingSettings.Current = Mathf.Clamp (weaponSettings.LoadingSettings.Current, weaponSettings.LoadingSettings.Minimum, weaponSettings.LoadingSettings.Maximum);
		}
		else if ((type == HotkeyType.WeaponMain && InputControl.IsShotReleased) || 
		         (type == HotkeyType.WeaponSecondary && InputControl.IsMeleeReleased))
		{
			if (weaponSettings.LoadingSettings.IsFull)
			{
				ExecuteRangedAttack(lastAimPosition, angle, useAmmoAsDirection);
			}
			else
			{
				ExecuteMeleeAttack(meleeWeapon);
			}

			// Back to min range distance.
			weaponSettings.LoadingSettings.Current = weaponSettings.LoadingSettings.Minimum;

			GameContext.CameraSettings.ZoomOut();
		}
	}

	#endregion
}