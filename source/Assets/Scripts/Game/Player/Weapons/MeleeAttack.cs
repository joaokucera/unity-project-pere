﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CircleCollider2D))]
public class MeleeAttack : BaseAmmo 
{
	#region [ FIELDS ]

	/// <summary>
	/// The melee show time.
	/// </summary>
	[SerializeField]
	private float meleeShowTime = 0.5f;
	/// <summary>
	/// Control the current sprite side.
	/// </summary>
	private FacingSide currentFacingSide;
	/// <summary>
	/// The original scale.
	/// </summary>
	private Vector2 originalScale;
	
	#endregion

	#region [ METHODS ]

	#region implemented abstract members of BaseAmmo
	
	/// <summary>
	/// Dos the shoot.
	/// </summary>
	/// <param name="settings">Settings.</param>
	public override void Execute (WeaponSettings settings)
	{
		ammoSettings = settings.AmmoSettings;

		gameObject.SetActive (true);

		StartCoroutine (OnMove (settings));
	}
	
	/// <summary>
	/// Raises the move event.
	/// </summary>
	/// <param name="settings">Settings.</param>
	protected override IEnumerator OnMove (WeaponSettings settings)
	{
		yield return new WaitForSeconds (meleeShowTime);
		
		gameObject.SetActive (false);
	}
	
	#endregion

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	protected override void Start ()
	{
		base.Start ();

		gameObject.tag = ShortcutWords.PlayerAttackTag;

//		currentFacingSide = FacingSide.Right;
//		originalScale = transform.localScale;
		
		transform.localPosition = -transform.parent.localPosition;
		gameObject.SetActive (false);
	}

//	/// <summary>
//	/// Update is called every frame, if the MonoBehaviour is enabled.
//	/// </summary>
//	protected override void Update ()
//	{
//		// Aim position.
//		Vector3 aimPosition = GlobalVariables.PlayerWeapon.LastAimPosition;
//		
//		// To evaluate which animation.
//		if (GlobalVariables.PlayerWeapon.InputType == InputType.Keyboard)
//		{
//			EvaluateAnimationFromKeyboard(aimPosition);
//		}
//		else
//		{
//			EvaluateAnimationFromJoystick(aimPosition);
//		}
//		
//		UpdateFacingSide();
//	}
//
//	/// <summary>
//	/// Method to evaluate which animation will be setted.
//	/// </summary>
//	private void EvaluateAnimationFromKeyboard(Vector2 aimPosition)
//	{
//		float xDistance = transform.localPosition.x - aimPosition.x;
//		float yDistance = transform.localPosition.y - aimPosition.y;
//		
//		if (Mathf.Abs(xDistance) > Mathf.Abs(yDistance))
//		{
//			if (aimPosition.x >= transform.localPosition.x)
//			{
//				currentFacingSide = FacingSide.Right;
//			}
//			else
//			{
//				currentFacingSide = FacingSide.Left;
//			}
//		}
//		else
//		{
//			if (aimPosition.y >= transform.localPosition.y)
//			{
//				currentFacingSide = FacingSide.Up;
//			}
//			else
//			{
//				currentFacingSide = FacingSide.Down;
//			}
//		}
//	}
//	
//	/// <summary>
//	/// Method to evaluate which animation will be setted.
//	/// </summary>
//	private void EvaluateAnimationFromJoystick(Vector2 aimPosition)
//	{
//		bool lookAtSide = Mathf.Abs(aimPosition.x) > Mathf.Abs(aimPosition.y);
//		
//		if (lookAtSide)
//		{
//			if (aimPosition.x >= 0)
//			{
//				currentFacingSide = FacingSide.Right;
//			}
//			else
//			{
//				currentFacingSide = FacingSide.Left;
//			}
//		}
//		else
//		{
//			if (aimPosition.y >= 0)
//			{
//				currentFacingSide = FacingSide.Up;
//			}
//			else
//			{
//				currentFacingSide = FacingSide.Down;
//			}
//		}
//	}
//	
//	/// <summary>
//	/// Method to control animation.
//	/// </summary>
//	private void UpdateFacingSide()
//	{
//		switch (currentFacingSide)
//		{
//			case FacingSide.Right:
//			case FacingSide.Up:
//				Flip(new Vector2(-1, 1));
//				break;
//			case FacingSide.Left:
//				Flip(new Vector2(1, 1));
//				break;
//			case FacingSide.Down:
//				Flip(new Vector2(1, -1));
//				break;
//		}
//	}
//	
//	/// <summary>
//	/// Method to flip sprite.
//	/// </summary>
//	private void Flip(Vector2 newScale)
//	{
//		Vector2 scale = transform.localScale;
//		scale.x = newScale.x * originalScale.x;
//		scale.y = newScale.y * originalScale.y;
//		
//		transform.localScale = scale;
//	}

	#endregion
}