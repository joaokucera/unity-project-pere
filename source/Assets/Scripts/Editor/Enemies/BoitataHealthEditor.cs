﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BoitataHealth))]
public class BoitataHealthEditor : EnemyHealthEditor 
{
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
	}
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}
