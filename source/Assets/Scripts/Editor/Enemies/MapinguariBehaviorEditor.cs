using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapinguariBehavior))]
public class MapinguariBehaviorEditor : BaseEnemyEditor 
{
	#region [ FIELDS ]

	private SerializedProperty meleeDistance;
	private SerializedProperty waitTimeWhenBlocking;
	private SerializedProperty spriteWhenBlocking;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		meleeDistance = instance.FindProperty ("meleeDistance");
		waitTimeWhenBlocking = instance.FindProperty ("waitTimeWhenBlocking");
		spriteWhenBlocking = instance.FindProperty ("spriteWhenBlocking");
	}
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();

		GUILayout.Space (5f);
		EditorGUILayout.Slider(meleeDistance, 1f, 5f, "Melee Distance");
		EditorGUILayout.LabelField("(distance to stop front the player and attack him)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(waitTimeWhenBlocking, 0.5f, 5f, "Wait Time When Blocking");
		EditorGUILayout.LabelField("(time to wait when the state is blocked)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(spriteWhenBlocking);
		EditorGUILayout.LabelField("(the sprite used when state is blocking)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}