﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BoitataBehavior))]
public class BoitataBehaviorEditor : BaseEnemyEditor 
{
	#region [ FIELDS ]

	private SerializedProperty probabilityPercentageToShoot;
	private SerializedProperty sprintMultiplier;
	private SerializedProperty bulletWeaponIndex;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		probabilityPercentageToShoot = instance.FindProperty ("probabilityPercentageToShoot");
		sprintMultiplier = instance.FindProperty ("sprintMultiplier");
		bulletWeaponIndex = instance.FindProperty ("bulletWeaponIndex");
	}
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();

		GUILayout.Space (5f);
		EditorGUILayout.IntSlider(probabilityPercentageToShoot, 0, 100, "Probability Percentage");
		EditorGUILayout.LabelField("(probability to shoot when is available)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(sprintMultiplier, 1f, 50f, "Sprint Multiplier");
		EditorGUILayout.LabelField("(the sprint multiplier)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.IntSlider(bulletWeaponIndex, 2, 5, "Bullet Weapon Index");
		EditorGUILayout.LabelField("(the index to use bullet weapon)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}