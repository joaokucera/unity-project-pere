﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CapeloboBehavior))]
public class CapeloboBehaviorEditor : BaseEnemyEditor 
{
	#region [ FIELDS ]
	
	private SerializedProperty attackDistance;
	private SerializedProperty sprintForce;
	private SerializedProperty tongueSpeed;
	private SerializedProperty probabilityPercentageToSprint;
	private SerializedProperty probabilityPercentageToLick;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
		
		attackDistance = instance.FindProperty ("attackDistance");
		sprintForce = instance.FindProperty ("sprintForce");
		tongueSpeed = instance.FindProperty ("tongueSpeed");
		probabilityPercentageToSprint = instance.FindProperty ("probabilityPercentageToSprint");
		probabilityPercentageToLick = instance.FindProperty ("probabilityPercentageToLick");
	}
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();
		
		GUILayout.Space (5f);
		EditorGUILayout.Slider(attackDistance, 5f, 15f, "Attack Distance");
		EditorGUILayout.LabelField("(distance to stop away from the player and randomize which the attack will be made)", EditorStyles.miniLabel);
		
		GUILayout.Space (5f);
		EditorGUILayout.Slider(sprintForce, 250f, 750f, "Sprint Force");
		EditorGUILayout.LabelField("(how strong will be the impulse against the player)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(tongueSpeed, 5f, 15f, "Tongue Speed");
		EditorGUILayout.LabelField("(speed to hit the player)", EditorStyles.miniLabel);

		GUILayout.Space (10f);
		EditorGUILayout.LabelField ("Attacks Probability Percentage", EditorStyles.boldLabel);
		EditorGUILayout.LabelField("(probabilities to sprint and lick between 0% and 100%)", EditorStyles.miniLabel);
		GUILayout.Space (5f);
		probabilityPercentageToSprint.intValue = 100 - probabilityPercentageToLick.intValue;
		EditorGUILayout.IntSlider(probabilityPercentageToSprint, 0, 100, "To Sprint");
		probabilityPercentageToLick.intValue = 100 - probabilityPercentageToSprint.intValue;
		EditorGUILayout.IntSlider(probabilityPercentageToLick, 0, 100, "To Lick");
		
		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}