using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ArrancaLinguasBehavior))]
public class ArrancaLinguasBehaviorEditor : BaseEnemyEditor 
{
	#region [ FIELDS ]

	private SerializedProperty meleeDistance;
	private SerializedProperty returnGuardSpeed;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		meleeDistance = instance.FindProperty ("meleeDistance");
		returnGuardSpeed = instance.FindProperty ("returnGuardSpeed");
	}
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();

		GUILayout.Space (5f);
		EditorGUILayout.Slider(meleeDistance, 1f, 2f, "Melee Distance");
		EditorGUILayout.LabelField("(distance to stop front the player and attack him)",EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(returnGuardSpeed, 0.5f, 2.5f, "Return Guard Speed");
		EditorGUILayout.LabelField("(speed to return to start position)",EditorStyles.miniLabel);
		
		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}