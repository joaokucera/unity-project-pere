﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(IpupiaraBehavior))]
public class IpupiaraBehaviorEditor : BaseEnemyEditor 
{
	#region [ FIELDS ]

	private SerializedProperty probabilityPercentageToBurrower;
	private SerializedProperty burrowerPlaces;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		probabilityPercentageToBurrower = instance.FindProperty ("probabilityPercentageToBurrower");
		burrowerPlaces = instance.FindProperty ("burrowerPlaces");
	}
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();

		GUILayout.Space (10f);
		EditorGUILayout.LabelField("Burrower Settings", EditorStyles.boldLabel);
		GUILayout.Space (5f);
		EditorGUILayout.IntSlider(probabilityPercentageToBurrower, 0, 100, "Probability Percentage");
		EditorGUILayout.LabelField("(probability to burrower when is available)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField (burrowerPlaces, true);
		EditorGUILayout.LabelField("(places used to randomize burrower destination)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}