using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BoitataLaserAmmo))]
public class BoitataLaserAmmoEditor : BaseAmmoEditor {

	#region [ FIELDS ]

	#endregion

	#region [ METHODS ]

	protected override void OnEnable ()
	{
		base.OnEnable ();
	}

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		instance.Update ();

		instance.ApplyModifiedProperties ();
	}

	#endregion
}