using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnemyWeapon))]
public class EnemyWeaponEditor : GenericPoolingEditor 
{
	#region [ FIELDS ]
	
	private SerializedProperty ammoSpeed;
	private SerializedProperty ammoDamage;
	private SerializedProperty enabledTime;
	private SerializedProperty attackTime;
	private SerializedProperty cooldownRate;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
		
		SerializedProperty weaponSettings = instance.FindProperty ("weaponSettings");
		
		ammoSpeed = weaponSettings.FindPropertyRelative ("AmmoSettings.Speed");
		ammoDamage = weaponSettings.FindPropertyRelative ("AmmoSettings.DamageSettings.Current");
		enabledTime = weaponSettings.FindPropertyRelative ("TimeSettings.Enabled");
		attackTime = weaponSettings.FindPropertyRelative ("TimeSettings.Value");
		cooldownRate = weaponSettings.FindPropertyRelative ("CooldownSettings.Rate");
	}
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();
		
		GUILayout.Space(5f);
		GUILayout.Label ("Weapon Settings", EditorStyles.boldLabel);
		
		EditorGUILayout.Slider (ammoSpeed, 1f, 5000f, "Ammo Speed");
		EditorGUILayout.IntSlider (ammoDamage, 1, 100, "Ammo Damage");
		
		GUILayout.Space(5f);
		enabledTime.boolValue = EditorGUILayout.ToggleLeft ("Enable Attack Time", enabledTime.boolValue);
		if (enabledTime.boolValue)
		{
			EditorGUILayout.Slider(attackTime, 0.1f, 5f, "Attack Time");
		}
		
		GUILayout.Space(5f);
		GUILayout.Label ("Coowdown Rate", EditorStyles.boldLabel); 
		EditorGUILayout.Slider (cooldownRate, 0.1f, 10f, "Coowdown Rate");
		
		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}