using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BoitataLaserWeapon))]
public class BoitataLaserWeaponEditor : EnemyWeaponEditor 
{
	#region [ FIELDS ]

	private SerializedProperty scaleDivider;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		scaleDivider = instance.FindProperty ("scaleDivider");
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();
		
		GUILayout.Space(10f);
		EditorGUILayout.Slider (scaleDivider, 1f, 1000f, "Scale Divider");
		
		instance.ApplyModifiedProperties ();

		base.OnInspectorGUI ();
	}
	
	#endregion
}