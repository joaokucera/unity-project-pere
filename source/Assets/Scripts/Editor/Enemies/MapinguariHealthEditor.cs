﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapinguariHealth))]
public class MapinguariHealthEditor : EnemyHealthEditor 
{
	#region [ FIELDS ]
	
	//private SerializedProperty boitataParts;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		//boitataParts = instance.FindProperty ("boitataParts");
	}
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();

//		GUILayout.Space (5f);
//		EditorGUILayout.PropertyField (boitataParts, true);
//		EditorGUILayout.LabelField("(the boitata parts)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}
