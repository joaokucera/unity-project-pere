﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BoitataEyeBehavior))]
public class BoitataEyeBehaviorEditor : BaseEnemyEditor 
{
	#region [ FIELDS ]

	private SerializedProperty meleeDistance;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		meleeDistance = instance.FindProperty ("meleeDistance");
	}
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		instance.Update ();

		GUILayout.Space (5f);
		EditorGUILayout.Slider(meleeDistance, 1f, 5f, "Melee Distance");
		EditorGUILayout.LabelField("(distance to stop front the player and attack him)",EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}