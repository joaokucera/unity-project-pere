using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CorpoSecoMasterBehavior))]
public class CorpoSecoMasterBehaviorEditor : BaseEnemyEditor 
{
	#region [ FIELDS ]

	private SerializedProperty meleeDistance;
	private SerializedProperty flockSize;
	private SerializedProperty timeToSpawn;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		meleeDistance = instance.FindProperty ("meleeDistance");
		flockSize = instance.FindProperty ("flockSize");
		timeToSpawn = instance.FindProperty ("timeToSpawn");
	}
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();

		GUILayout.Space (5f);
		EditorGUILayout.Slider(meleeDistance, 1f, 5f, "Melee Distance");
		EditorGUILayout.LabelField("(distance to stop front the player and attack him)",EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.IntSlider(flockSize, 1, 20, "Flock Size");
		EditorGUILayout.LabelField("(how many corpo secos will be created)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(timeToSpawn, 1f, 10f, "Time To Spawn");
		EditorGUILayout.LabelField("(time to spawn slaves)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}