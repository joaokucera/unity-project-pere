using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(ItemsRepository))]
public class ItemsRepositoryEditor : BaseEditor 
{
	#region [ FIELDS ]

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}