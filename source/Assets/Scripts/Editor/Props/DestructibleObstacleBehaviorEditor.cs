using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(DestructibleObstacleBehavior))]
public class DestructibleObstacleBehaviorEditor : BaseEditor
{
	#region [ FIELDS ]

	private SerializedProperty healthStepsSprites;
	private SerializedProperty spawnItemProbability;
	private SerializedProperty spawnItems;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		spawnItemProbability = instance.FindProperty ("spawnItemProbability");
		healthStepsSprites = instance.FindProperty ("healthStepsSprites");
		spawnItems = instance.FindProperty ("spawnItems");
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();

		GUILayout.Space (15f);
		GUILayout.BeginHorizontal();
			GUILayout.Label ("Health Steps", EditorStyles.boldLabel, GUILayout.Width(75));
			EditorGUILayout.LabelField("(the sprites to show health steps)", EditorStyles.miniLabel);
		GUILayout.EndHorizontal();
		EditorGUILayout.PropertyField(healthStepsSprites, GUIContent.none, true);

		GUILayout.Space (10f);
		GUILayout.Label ("Spawn Items", EditorStyles.boldLabel);
		
		GUILayout.Space (5f);
		GUILayout.BeginHorizontal();
		GUILayout.Label ("Probability between 0% and 100%", GUILayout.Width(180));
		EditorGUILayout.LabelField("(the probability to spawn an item)", EditorStyles.miniLabel);
		GUILayout.EndHorizontal();
		EditorGUILayout.IntSlider (spawnItemProbability, 0, 100, GUIContent.none);
		
		EditorGUI.BeginDisabledGroup (spawnItemProbability.intValue == 0);
		GUILayout.Space (5f);
		GUILayout.BeginHorizontal();
			GUILayout.Label ("Items", GUILayout.Width(35));
			EditorGUILayout.LabelField("(the items to spawn)", EditorStyles.miniLabel);
		GUILayout.EndHorizontal();
		EditorGUILayout.PropertyField (spawnItems, GUIContent.none, true);
		EditorGUI.EndDisabledGroup ();

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}