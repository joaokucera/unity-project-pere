using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(ParticlesPooling))]
public class ParticlesPoolingEditor : GenericPoolingEditor 
{
	#region [ FIELDS ]

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}