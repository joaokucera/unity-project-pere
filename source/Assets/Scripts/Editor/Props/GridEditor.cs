using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Grid))]
public class GridEditor : BaseEditor
{
	#region [ FIELDS ]

	private SerializedProperty displayGizmos;
	private SerializedProperty unwalkableMask;
	private SerializedProperty gridWorldSize;
	private SerializedProperty nodeRadius;

	private ReorderableList regions;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		displayGizmos = instance.FindProperty ("displayGizmos");
		unwalkableMask = instance.FindProperty ("unwalkableMask");
		gridWorldSize = instance.FindProperty ("gridWorldSize");
		nodeRadius = instance.FindProperty ("nodeRadius");

		regions = new ReorderableList(instance, instance.FindProperty("regions"), true, true, true, true);
		regions.drawHeaderCallback = (Rect rect) => 
		{ 
			EditorGUI.LabelField(rect, "Regions");
		};
		regions.drawElementCallback =  (Rect rect, int index, bool isActive, bool isFocused) => 
		{
			var element = regions.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;

			EditorGUI.PropertyField
			(
				new Rect(rect.x, rect.y, 120, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("mask"), GUIContent.none
			);
			EditorGUI.PropertyField
			(
				new Rect(rect.x + 140, rect.y, 40, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("penalty"), GUIContent.none
			);
		};
		regions.onRemoveCallback = (ReorderableList l) => 
		{  
			if (EditorUtility.DisplayDialog("Warning!", "Are you sure you want to delete the region?", "Yes", "No")) 
			{
				ReorderableList.defaultBehaviours.DoRemoveButton(l);
			}
		};
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();

		GUILayout.Space (10f);
		displayGizmos.boolValue = EditorGUILayout.ToggleLeft(" Display Gizmos", displayGizmos.boolValue);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(unwalkableMask);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(gridWorldSize);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(nodeRadius, 0.1f, 2f, "Node Radius");

		GUILayout.Space (5f);
		regions.DoLayoutList();


		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}