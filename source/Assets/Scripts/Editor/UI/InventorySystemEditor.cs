﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(InventorySystem))]
public class InventorySystemEditor : BaseEditor 
{
	#region [ FIELDS ]
	
	private SerializedProperty inventoryBackground;
	private SerializedProperty goldTextUI;
	private SerializedProperty dialogBox;
	private SerializedProperty dialogBoxButtons;
	private SerializedProperty tooltip;
	private SerializedProperty tooltipSizeText;
	private SerializedProperty tooltipVisualText;
	private SerializedProperty weaponsHotKeys;
	private SerializedProperty itemsHotKeys;
	private SerializedProperty selectedSlotSprite;
	private SerializedProperty tabsImages;
	private SerializedProperty tabsSprites;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		inventoryBackground = instance.FindProperty ("inventoryBackground");
		goldTextUI = instance.FindProperty ("goldTextUI");
		dialogBox = instance.FindProperty ("dialogBox");
		dialogBoxButtons = instance.FindProperty ("dialogBoxButtons");
		tooltip = instance.FindProperty ("tooltip");
		tooltipSizeText = instance.FindProperty ("tooltipSizeText");
		tooltipVisualText = instance.FindProperty ("tooltipVisualText");
		weaponsHotKeys = instance.FindProperty ("weaponsHotKeys");
		itemsHotKeys = instance.FindProperty ("itemsHotKeys");
		selectedSlotSprite = instance.FindProperty ("selectedSlotSprite");
		tabsImages = instance.FindProperty ("tabsImages");
		tabsSprites = instance.FindProperty ("tabsSprites");
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();

		GUILayout.Space (10f);
		EditorGUILayout.PropertyField(inventoryBackground);
		EditorGUILayout.LabelField("(the inventory image)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(goldTextUI);
		EditorGUILayout.LabelField("(the gold amount text)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(dialogBox);
		EditorGUILayout.LabelField("(the dialog box to choose between to use or to select an item)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(dialogBoxButtons, true);
		EditorGUILayout.LabelField("(the dialog box buttons)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(tooltip);
		EditorGUILayout.LabelField("(the tooltip object)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(tooltipSizeText);
		EditorGUILayout.LabelField("(the text to risize tooltip background)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(tooltipVisualText);
		EditorGUILayout.LabelField("(the text to show item's details)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(weaponsHotKeys, true);
		EditorGUILayout.LabelField("(the hotkeys to hold weapons)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(itemsHotKeys, true);
		EditorGUILayout.LabelField("(the hotkeys to hold items)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(selectedSlotSprite, true);
		EditorGUILayout.LabelField("(the sprite when a slot was selected)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(tabsImages, true);
		EditorGUILayout.LabelField("(the tabs images UI components)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(tabsSprites, true);
		EditorGUILayout.LabelField("(the tabs sprites)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}