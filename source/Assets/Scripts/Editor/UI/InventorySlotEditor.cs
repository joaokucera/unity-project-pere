﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(InventorySlot))]
public class InventorySlotEditor : BaseEditor 
{
	#region [ FIELDS ]

	private SerializedProperty slotItemImageUI;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		slotItemImageUI = instance.FindProperty ("slotItemImageUI");
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();

		GUILayout.Space (10f);
		EditorGUILayout.PropertyField(slotItemImageUI);
		EditorGUILayout.LabelField("(the image component to hold the item's sprite)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}