﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CameraSmoothFollow))]
public class CameraSmoothFollowEditor : BaseEditor {

	#region [ FIELDS ]
	
	private SerializedProperty interpolation;
	private SerializedProperty lerpSpeed;
	private SerializedProperty offset;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
		
		interpolation = instance.FindProperty ("interpolation");
		lerpSpeed = instance.FindProperty ("lerpSpeed");
		offset = instance.FindProperty ("offset");
	}
	
	public override void OnInspectorGUI ()
	{
		instance.Update ();
		
		GUILayout.Space (10f);
		EditorGUILayout.Slider(interpolation, 1f, 20f, "Interpolation");
		EditorGUILayout.LabelField("(how far the player can move from camera center)",EditorStyles.miniLabel);
		
		GUILayout.Space (5f);
		EditorGUILayout.Slider(lerpSpeed, 0.1f, 5f, "Lerp Speed");
		EditorGUILayout.LabelField("(how fast the camera follows the player)",EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(offset);
		EditorGUILayout.LabelField("(the camera compensation)",EditorStyles.miniLabel);
		
		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}