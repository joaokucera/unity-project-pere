﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CameraShader))]
public class CameraShaderEditor : BaseEditor {

	#region [ FIELDS ]
	
	private SerializedProperty defaultShader;
	private SerializedProperty blackWhiteShader;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
		
		defaultShader = instance.FindProperty ("defaultShader");
		blackWhiteShader = instance.FindProperty ("blackWhiteShader");
	}
	
	public override void OnInspectorGUI ()
	{
		instance.Update ();
		
		GUILayout.Space (10f);
		EditorGUILayout.PropertyField(defaultShader);
		EditorGUILayout.LabelField("(default shader)",EditorStyles.miniLabel);
		
		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(blackWhiteShader);
		EditorGUILayout.LabelField("(black & white shader)",EditorStyles.miniLabel);
		
		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}