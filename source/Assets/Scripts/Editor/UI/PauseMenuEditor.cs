﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PauseMenu))]
public class PauseMenuEditor : BaseEditor 
{
	#region [ FIELDS ]

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}