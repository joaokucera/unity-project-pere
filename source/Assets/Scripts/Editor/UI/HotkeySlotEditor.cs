﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(HotkeySlot))]
public class HotkeySlotEditor : BaseEditor 
{
	#region [ FIELDS ]
	
	private SerializedProperty hotkeyType;
	private SerializedProperty slotItemImageUI;
	private SerializedProperty hotkeyStatusBar;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		hotkeyType = instance.FindProperty ("hotkeyType");
		slotItemImageUI = instance.FindProperty ("slotItemImageUI");
		hotkeyStatusBar = instance.FindProperty ("hotkeyStatusBar");
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();

		GUILayout.Space (10f);
		EditorGUILayout.PropertyField(hotkeyType);
		EditorGUILayout.LabelField("(the hotkey type to load the correct weapon)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(slotItemImageUI);
		EditorGUILayout.LabelField("(the image component to hold the item's sprite)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(hotkeyStatusBar);
		EditorGUILayout.LabelField("(the relative image component on status bar)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}