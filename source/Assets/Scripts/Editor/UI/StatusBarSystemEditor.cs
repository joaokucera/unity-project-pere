﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StatusBarSystem))]
public class StatusBarSystemEditor : BaseEditor 
{
	#region [ FIELDS ]

	private SerializedProperty healthBarImage;
	private SerializedProperty extraHealthBarImage;
	private SerializedProperty healthBarSpeed;
	private SerializedProperty manaBarImage;
	private SerializedProperty goldTextUI;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		healthBarImage = instance.FindProperty ("healthBarImage");
		extraHealthBarImage = instance.FindProperty ("extraHealthBarImage");
		healthBarSpeed = instance.FindProperty ("healthBarSpeed");
		manaBarImage = instance.FindProperty ("manaBarImage");
		goldTextUI = instance.FindProperty ("goldTextUI");
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();

		GUILayout.Space (10f);
		EditorGUILayout.PropertyField(healthBarImage);
		EditorGUILayout.LabelField("(the health bar image component)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(extraHealthBarImage);
		EditorGUILayout.LabelField("(the extra health bar image component)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(healthBarSpeed, 1f, 10f, "Health Bar Speed");
		EditorGUILayout.LabelField("(how fast health bar moves)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(manaBarImage);
		EditorGUILayout.LabelField("(the mana bar image component)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(goldTextUI);
		EditorGUILayout.LabelField("(the gold amount text)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}