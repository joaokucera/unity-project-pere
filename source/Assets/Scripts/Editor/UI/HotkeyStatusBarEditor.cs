﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(HotkeyStatusBar))]
public class HotkeyStatusBarEditor : BaseEditor 
{
	#region [ FIELDS ]

	private SerializedProperty itemImageUI;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		itemImageUI = instance.FindProperty ("itemImageUI");
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();

		GUILayout.Space (10f);
		EditorGUILayout.PropertyField(itemImageUI);
		EditorGUILayout.LabelField("(the image component to hold the item's sprite)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}