﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CassavaItem))]
public class CassavaItemEditor : BaseItemEditor 
{
	#region [ FIELDS ]
	
	private SerializedProperty extraDamageValue;
	private SerializedProperty extraDamageEffectTime;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
		
		extraDamageValue = instance.FindProperty ("extraDamageValue");
		extraDamageEffectTime = instance.FindProperty ("extraDamageEffectTime");
	}
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();
		
		GUILayout.Space (10f);
		EditorGUILayout.IntSlider(extraDamageValue, 0, 10, "Extra Damage Value");
		EditorGUILayout.LabelField("(the extra damage value)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(extraDamageEffectTime, 0, 10f, "Extra Damage Effect Time");
		EditorGUILayout.LabelField("(the duration of extra damage)",EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}