﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CoffeeItem))]
public class CoffeeItemEditor : BaseItemEditor 
{
	#region [ FIELDS ]
	
	private SerializedProperty extraSpeedPercentage;
	private SerializedProperty extraSpeedEffectTime;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
		
		extraSpeedPercentage = instance.FindProperty ("extraSpeedPercentage");
		extraSpeedEffectTime = instance.FindProperty ("extraSpeedEffectTime");
	}
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();
		
		GUILayout.Space (10f);
		EditorGUILayout.IntSlider(extraSpeedPercentage, 0, 10, "Extra Speed Percentage (0% to 100%)");
		EditorGUILayout.LabelField("(the percentage to increment current walk speed)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(extraSpeedEffectTime, 0f, 10f, "Extra Speed Effect Time");
		EditorGUILayout.LabelField("(the duration of extra speed)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}