﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GuaranaItem))]
public class GuaranaItemEditor : BaseItemEditor 
{
	#region [ FIELDS ]

	private SerializedProperty extraHealthPercentage;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		extraHealthPercentage = instance.FindProperty ("extraHealthPercentage");
	}
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();

		instance.Update ();

		GUILayout.Space (10f);
		EditorGUILayout.IntSlider(extraHealthPercentage, 1, 100, "Extra Health Percentage (0% to 100%)");
		EditorGUILayout.LabelField("(the percentage to add extra health)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}