﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AcaiBerryItem))]
public class AcaiBerryEditor : BaseItemEditor 
{
	#region [ FIELDS ]

	private SerializedProperty healPercentage;

	#endregion
	
	#region [ METHODS ]

	protected override void OnEnable ()
	{
		base.OnEnable ();

		healPercentage = instance.FindProperty ("healPercentage");
	}
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();

		instance.Update ();

		GUILayout.Space (10f);
		EditorGUILayout.IntSlider(healPercentage, 0, 100, "Heal Percentage (0% to 100%)");
		EditorGUILayout.LabelField("(the percentage of healing)",EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}

	#endregion
}