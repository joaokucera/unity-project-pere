﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MuiraquitaItem))]
public class MuiraquitaItemEditor : BaseItemEditor 
{
	#region [ FIELDS ]

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
	}
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();

		instance.Update ();
		
		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}