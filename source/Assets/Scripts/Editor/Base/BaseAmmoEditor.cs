﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BaseAmmo))]
public class BaseAmmoEditor : BaseEditor 
{	
	#region [ FIELDS ]
	
	private SerializedProperty shadow;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		shadow = instance.FindProperty ("shadow");
	}
	
	public override void OnInspectorGUI ()
	{
		instance.Update ();

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(shadow);
		EditorGUILayout.LabelField("(the ammo shadow)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}
