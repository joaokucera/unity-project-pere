﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BaseEnemy))]
public abstract class BaseEnemyEditor : BaseEditor {

	#region [ FIELDS ]

	private SerializedProperty debug;
	private SerializedProperty waitTimeToChangeStatus;
	private SerializedProperty rangeDistance;
	private SerializedProperty chaseDistance;
	private SerializedProperty chaseSpeed;
	private SerializedProperty damageByTouchingThePlayer;
	private SerializedProperty rangedDistanceIncreaser;
	private SerializedProperty rangedDistanceIncreaserTime;
	private SerializedProperty timeToAttack;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		debug = instance.FindProperty ("debug");
		waitTimeToChangeStatus = instance.FindProperty ("waitTimeToChangeStatus");
		rangeDistance = instance.FindProperty ("rangeDistance");
		chaseDistance = instance.FindProperty ("chaseDistance");
		chaseSpeed = instance.FindProperty ("chaseSpeed");
		damageByTouchingThePlayer = instance.FindProperty ("damageByTouchingThePlayer");
		rangedDistanceIncreaser = instance.FindProperty ("rangedDistanceIncreaser");
		rangedDistanceIncreaserTime = instance.FindProperty ("rangedDistanceIncreaserTime");
		timeToAttack = instance.FindProperty ("timeToAttack");
	}
	
	public override void OnInspectorGUI ()
	{
		instance.Update ();

		GUILayout.Space (10f);
		debug.boolValue = EditorGUILayout.Toggle("Debug Enable", debug.boolValue);
		EditorGUILayout.LabelField("(enable to debug)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(waitTimeToChangeStatus, 0f, 2f, "Wait Time To Change Status");
		EditorGUILayout.LabelField("(time to wait before changing among status)",EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(rangeDistance, 5f, 50f, "Range Distance");
		EditorGUILayout.LabelField("(max distance to follow the player)",EditorStyles.miniLabel);
		
		GUILayout.Space (5f);
		EditorGUILayout.Slider(chaseDistance, 5f, 20f, "Chase Distance");
		EditorGUILayout.LabelField("(distance to start following the player)",EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(chaseSpeed, 0f, 50f, "Chase Speed");
		EditorGUILayout.LabelField("(speed to follow the player)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.IntSlider(damageByTouchingThePlayer, 1, 10, "Damage By Touching The Player");
		EditorGUILayout.LabelField("(the damage when touching the player)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(rangedDistanceIncreaser, 1f, 10f, "Ranged Distance Increaser");
		EditorGUILayout.LabelField("(the range distance increment)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(rangedDistanceIncreaserTime, 1f, 10f, "Ranged Distance Increaser Time");
		EditorGUILayout.LabelField("(the range distance increment timer)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(timeToAttack, 0.1f, 5f, "Time To Attack");
		EditorGUILayout.LabelField("(time to attack the player)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}
