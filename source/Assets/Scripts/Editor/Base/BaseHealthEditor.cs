﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BaseHealth))]
public abstract class BaseHealthEditor : BaseEditor 
{	
	#region [ FIELDS ]
	
	private SerializedProperty initialHealth;
	private SerializedProperty initialBlinkEffectTime;
	private SerializedProperty recoveryTime;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
		
		initialHealth = instance.FindProperty ("initialHealth");
		initialBlinkEffectTime = instance.FindProperty ("initialBlinkEffectTime");
		recoveryTime = instance.FindProperty ("recoveryTime");
	}
	
	public override void OnInspectorGUI ()
	{
		instance.Update ();
		
		GUILayout.Space (10f);
		EditorGUILayout.IntSlider(initialHealth, 1, 100, "Health");
		EditorGUILayout.LabelField("(health initial value)",EditorStyles.miniLabel);
		
		GUILayout.Space (5f);
		EditorGUILayout.Slider(initialBlinkEffectTime, 0.1f, 1f, "Blink Effect Time");
		EditorGUILayout.LabelField("(how long is the effect)",EditorStyles.miniLabel);
		
		GUILayout.Space (5f);
		EditorGUILayout.Slider(recoveryTime, 0f, 2.5f, "Recovery Time");
		EditorGUILayout.LabelField("(how long to recover and be hit again)",EditorStyles.miniLabel);
		
		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}