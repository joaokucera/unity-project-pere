﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BaseItem))]
public abstract class BaseItemEditor : BaseEditor 
{
	#region [ FIELDS ]

	private SerializedProperty color;
	private SerializedProperty description;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		color = instance.FindProperty ("color");
		description = instance.FindProperty ("description");
	}
	
	public override void OnInspectorGUI()
	{
		instance.Update ();
		
		GUILayout.Space (10f);
		EditorGUILayout.PropertyField(color);
		EditorGUILayout.LabelField("(the item's color)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.PropertyField(description);
		EditorGUILayout.LabelField("(the item's description)", EditorStyles.miniLabel);
		
		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}