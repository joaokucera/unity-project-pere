﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BaseWeapon))]
public abstract class BaseWeaponEditor : GenericPoolingEditor 
{
	#region [ FIELDS ]
	
	private SerializedProperty ammoSpeed;
	private SerializedProperty ammoDamage;
	private SerializedProperty currentLoading;
	private SerializedProperty enabledLoading;
	private SerializedProperty minLoading;
	private SerializedProperty maxLoading;
	private SerializedProperty enabledTime;
	private SerializedProperty attackTime;
	private SerializedProperty kickbackShotEnabled;
	private SerializedProperty kickbackShotForce;
	private SerializedProperty cooldownRate;
	
	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

		SerializedProperty weaponSettings = instance.FindProperty ("weaponSettings");

		ammoSpeed = weaponSettings.FindPropertyRelative ("AmmoSettings.Speed");
		ammoDamage = weaponSettings.FindPropertyRelative ("AmmoSettings.DamageSettings.Current");
		currentLoading = weaponSettings.FindPropertyRelative ("LoadingSettings.Current");
		enabledLoading = weaponSettings.FindPropertyRelative ("LoadingSettings.Enabled");
		minLoading = weaponSettings.FindPropertyRelative ("LoadingSettings.Minimum");
		maxLoading = weaponSettings.FindPropertyRelative ("LoadingSettings.Maximum");
		enabledTime = weaponSettings.FindPropertyRelative ("TimeSettings.Enabled");
		attackTime = weaponSettings.FindPropertyRelative ("TimeSettings.Value");
		kickbackShotEnabled = weaponSettings.FindPropertyRelative ("KickbackSettings.Enabled");
		kickbackShotForce = weaponSettings.FindPropertyRelative ("KickbackSettings.Force");
		cooldownRate = weaponSettings.FindPropertyRelative ("CooldownSettings.Rate");
	}
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();
		
		instance.Update ();

		GUILayout.Space(5f);
		GUILayout.Label ("Weapon Settings", EditorStyles.boldLabel);

		EditorGUILayout.Slider (ammoSpeed, 1f, 5000f, "Ammo Speed");
		EditorGUILayout.IntSlider (ammoDamage, 1, 100, "Ammo Damage");

		GUILayout.Space(5f);
		enabledLoading.boolValue = EditorGUILayout.ToggleLeft ("Enable Loading", enabledLoading.boolValue);
		if (enabledLoading.boolValue)
		{
			EditorGUILayout.Slider(minLoading, 0.1f, 5f, "Min Loading");
			minLoading.floatValue = Mathf.Clamp (minLoading.floatValue, 0.1f, maxLoading.floatValue);

			EditorGUILayout.Slider(maxLoading, 0.1f, 5f, "Max Loading");
			maxLoading.floatValue = Mathf.Clamp (maxLoading.floatValue, minLoading.floatValue, 5f);
		}
		else
		{
			EditorGUILayout.Slider(currentLoading, 0.1f, 5f, "Current Loading");
		}

		GUILayout.Space(5f);
		enabledTime.boolValue = EditorGUILayout.ToggleLeft ("Enable Attack Time", enabledTime.boolValue);
		if (enabledTime.boolValue)
		{
			EditorGUILayout.Slider(attackTime, 0.1f, 5f, "Attack Time");
		}

		GUILayout.Space(5f);
		GUILayout.Label ("Kickback", EditorStyles.boldLabel); 
		EditorGUILayout.BeginHorizontal();
		kickbackShotEnabled.boolValue = EditorGUILayout.Toggle ("Kickback Enabled", kickbackShotEnabled.boolValue);
			EditorGUILayout.LabelField("(you should keep it on, its cool)",EditorStyles.miniLabel);
		EditorGUILayout.EndHorizontal();
		EditorGUI.BeginDisabledGroup (!kickbackShotEnabled.boolValue);
			EditorGUILayout.Slider (kickbackShotForce, 1f, 5000f, "Kickback Force");
		EditorGUI.EndDisabledGroup();
		
		GUILayout.Space(5f);
		GUILayout.Label ("Coowdown Rate", EditorStyles.boldLabel); 
		EditorGUILayout.Slider (cooldownRate, 0.1f, 1.0f, "Coowdown Rate");
		
		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}