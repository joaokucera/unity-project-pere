﻿using UnityEngine;
using UnityEditor;

public abstract class BaseEditor : Editor 
{
	#region [ FIELDS ]
	
	protected SerializedObject instance;

	#endregion
	
	#region [ METHODS ]
	
	protected virtual void OnEnable()
	{
		if(instance == null) instance = new SerializedObject(target);
	}
	
	#endregion
}