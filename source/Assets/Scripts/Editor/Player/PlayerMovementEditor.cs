﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerMovement))]
public class PlayerMovementEditor : BaseEditor {

	#region [ FIELDS ]
	
	private SerializedProperty initialWalkSpeed;
	private SerializedProperty currentDodgeSpeed;
	private SerializedProperty currentDodgeDistance;
	private SerializedProperty enforceBounds;

	// manaSettings
	private SerializedProperty manaSettings_dodgeReloadTime;

	#endregion

	#region [ METHODS ]

	protected override void OnEnable ()
	{
		base.OnEnable ();

		initialWalkSpeed = instance.FindProperty ("initialWalkSpeed");
		currentDodgeSpeed = instance.FindProperty ("currentDodgeSpeed");
		currentDodgeDistance = instance.FindProperty ("currentDodgeDistance");
		enforceBounds = instance.FindProperty ("enforceBounds");

		var manaSettings = instance.FindProperty ("manaSettings");
		manaSettings_dodgeReloadTime = manaSettings.FindPropertyRelative ("dodgeReloadTime");
	}

	public override void OnInspectorGUI()
	{
		instance.Update ();

		GUILayout.Space (10f);
		EditorGUILayout.Slider(initialWalkSpeed, 1f, 50f, "Walk Speed");
		EditorGUILayout.LabelField("(how fast to walk)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(currentDodgeSpeed, 1f, 5000f, "Dodge Speed");
		EditorGUILayout.LabelField("(how fast to dodge)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(currentDodgeDistance, 0.1f, 10f, "Dodge Distance");
		EditorGUILayout.LabelField("(how far is the dodge)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		enforceBounds.boolValue = EditorGUILayout.Toggle ("Enforce Bounds", enforceBounds.boolValue);
		EditorGUILayout.LabelField("(enforce player inside the screen)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.LabelField ("Mana Settings", EditorStyles.boldLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(manaSettings_dodgeReloadTime, 0.1f, 10f, "Dodge Reload Time");
		EditorGUILayout.LabelField("(values to reload dodge)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}

	#endregion
}