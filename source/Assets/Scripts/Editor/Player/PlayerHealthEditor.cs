﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerHealth))]
public class PlayerHealthEditor : BaseHealthEditor {

	#region [ FIELDS ]
	
	private SerializedProperty defaultDamageHitAgainstEnemies;
	private SerializedProperty timeToReborn;

	#endregion

	#region [ METHODS ]

	protected override void OnEnable ()
	{
		base.OnEnable ();

		defaultDamageHitAgainstEnemies = instance.FindProperty ("defaultDamageHitAgainstEnemies");
		timeToReborn = instance.FindProperty ("timeToReborn");
	}

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		instance.Update ();

		GUILayout.Space (5f);
		EditorGUILayout.IntSlider(defaultDamageHitAgainstEnemies, 1, 10, "Damage Against Enemies");
		EditorGUILayout.LabelField("(the damage when was hit by an enemy)", EditorStyles.miniLabel);

		GUILayout.Space (5f);
		EditorGUILayout.Slider(timeToReborn, 1f, 10f, "Time To Reborn");
		EditorGUILayout.LabelField("(the time to reborn the player)", EditorStyles.miniLabel);

		instance.ApplyModifiedProperties ();
	}

	#endregion
}