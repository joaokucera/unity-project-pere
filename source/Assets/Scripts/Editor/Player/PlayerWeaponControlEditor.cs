﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerWeaponControl))]
public class PlayerWeaponControlEditor : BaseEditor {
	
	#region [ FIELDS ]

	private SerializedProperty inputType;

	#endregion
	
	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
		
		inputType = instance.FindProperty ("inputType");
	}
	
	public override void OnInspectorGUI ()
	{
		instance.Update ();
		
		GUILayout.Space (10f);
		GUILayout.Label ("Input Type Settings", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField (inputType);

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}