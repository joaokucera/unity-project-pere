using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(KoiereAxeWeapon))]
public class KoiereAxeWeaponEditor : BaseWeaponEditor
{
	#region [ FIELDS ]

//	private SerializedProperty meleeWeapon;
	//private SerializedProperty meleeShowTime;

	#endregion

	#region [ METHODS ]
	
	protected override void OnEnable ()
	{
		base.OnEnable ();

//		meleeWeapon = instance.FindProperty ("meleeWeapon");
		//meleeShowTime = instance.FindProperty ("meleeShowTime");
	}
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();

		instance.Update ();

//		GUILayout.Space(10f);
//		GUILayout.Label ("Melee Settings", EditorStyles.boldLabel); 
//		EditorGUILayout.PropertyField(meleeWeapon);
		//EditorGUILayout.Slider(meleeShowTime, 0.1f, 5f, "Melee Show Time");

		instance.ApplyModifiedProperties ();
	}
	
	#endregion
}