using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(KoiereAxeAmmo))]
public class KoiereAxeAmmoEditor : BaseAmmoEditor 
{
	#region [ FIELDS ]

	private SerializedProperty backSpeed;
	private SerializedProperty aditionalSpeed;
	private SerializedProperty rotationSpeed;

	#endregion

	#region [ METHODS ]

	protected override void OnEnable ()
	{
		base.OnEnable ();

		backSpeed = instance.FindProperty ("backSpeed");
		aditionalSpeed = instance.FindProperty ("aditionalSpeed");
		rotationSpeed = instance.FindProperty ("rotationSpeed");
	}

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		instance.Update ();

		GUILayout.Space(5f);
		EditorGUILayout.Slider(backSpeed, 1f, 50f, "Back Speed");

		GUILayout.Space(5f);
		EditorGUILayout.Slider(aditionalSpeed, 1f, 50f, "Aditional Speed");

		GUILayout.Space(5f);
		EditorGUILayout.Slider(rotationSpeed, 1f, 50f, "Rotation Speed");

		instance.ApplyModifiedProperties ();
	}

	#endregion
}