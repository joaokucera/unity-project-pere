using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BlowgunAmmo))]
public class BlowgunAmmoEditor : BaseAmmoEditor {

	#region [ FIELDS ]

	#endregion

	#region [ METHODS ]

	protected override void OnEnable ()
	{
		base.OnEnable ();
	}

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		instance.Update ();

		instance.ApplyModifiedProperties ();
	}

	#endregion
}