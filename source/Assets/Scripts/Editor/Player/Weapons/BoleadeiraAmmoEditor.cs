using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BoleadeiraAmmo))]
public class BoleadeiraAmmoEditor : BaseAmmoEditor {

	#region [ FIELDS ]

	private SerializedProperty backSpeed;
	private SerializedProperty rotationSpeed;
	private SerializedProperty improveDistance;

	#endregion

	#region [ METHODS ]

	protected override void OnEnable ()
	{
		base.OnEnable ();

		backSpeed = instance.FindProperty ("backSpeed");
		rotationSpeed = instance.FindProperty ("rotationSpeed");
		improveDistance = instance.FindProperty ("improveDistance");
	}

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		instance.Update ();

		GUILayout.Space(5f);
		EditorGUILayout.Slider(backSpeed, 1f, 50f, "Back Speed");

		GUILayout.Space(5f);
		EditorGUILayout.Slider(rotationSpeed, 1f, 50f, "Rotation Speed");

		GUILayout.Space(5f);
		EditorGUILayout.Slider(improveDistance, 1f, 5f, "Improve Distance");

		instance.ApplyModifiedProperties ();
	}

	#endregion
}