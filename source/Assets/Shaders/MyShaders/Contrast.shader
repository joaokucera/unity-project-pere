﻿Shader "MyShaders/Contrast" {
    Properties {
        _Amount ("Amount", Range(0,1) ) = 0
    }
    SubShader {
        Tags { "RenderType"="Opaque" }
 
        CGPROGRAM
        #pragma surface surf Lambert
 
        float _Amount;
 
        struct Input {
            float2 uv_MainTex;
            INTERNAL_DATA
        };
 
        void surf (Input IN, inout SurfaceOutput o) { 
            float4 color = float4(0.05 + _Amount / 1.25, 0.05 + _Amount / 1.25, 0.05 + _Amount / 1.25, 1);
 
            o.Albedo = color.rgb;
            o.Alpha = 1;
        }
        ENDCG
    } 
    FallBack "Diffuse"
}