﻿Shader "MyShaders/Color" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		Cull Off
		LOD 200
 
		CGPROGRAM
		#pragma surface surf Lambert
 
		fixed4 _Color;
 
		struct Input {
		  float2 uv_MainTex;
		};
 
		void surf (Input IN, inout SurfaceOutput o) {
		  o.Albedo = _Color.rgb;
		  o.Alpha = 1;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}